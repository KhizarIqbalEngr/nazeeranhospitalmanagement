﻿using NazeeranHospitalManagement.Models;
using NazeeranHospitalManagement.Models.Helpers;
using NazeeranHospitalManagement.ViewModels.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using static NazeeranHospitalManagement.Models.Helpers.NazeeranHospitalManagementConstants;
using MvcSelectListItem = System.Web.Mvc.SelectListItem;

namespace NazeeranHospitalManagement.ViewModels
{
    public class ViewAllViewModel
    {
        public UserRoles Role { set; get; }
        public IEnumerable<NazeeranHospitalUser> Users { set; get; }
    }

    #region DoctorsViewModels..!!

    public class DoctorBaseViewModel : IViewErrors
    {        
        [Required]
        [Display(Name = FullNameConst.DISPLAY_NAME)]
        [RegularExpression(FullNameConst.REGULAR_EXPRESSION, ErrorMessage = FullNameConst.ERROR_MESSAGE)]
        public string FullName { set; get; }

        [Required]
        [Display(Name = UserNameConst.DISPLAY_NAME)]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = CnicConst.DISPLAY_NAME)]
        [StringLength(CnicConst.MIN_LENGTH, ErrorMessage = CnicConst.ERROR_MESSAGE, MinimumLength = CnicConst.MIN_LENGTH)]
        [RegularExpression(CnicConst.REGULAR_EXPRESSION, ErrorMessage = CnicConst.ERROR_MESSAGE)]
        public string Cnic { set; get; }

        [Required]
        [Display(Name = "Gender")]
        public Gender Gender { set; get; }

        [Required]
        public int SelectedDepartment { set; get; }

        public IList<MvcSelectListItem> Departments { set; get; }

        [ScaffoldColumn(false)]
        [ReadOnly(true)]
        public List<string> Errors { set; get; }
    }

    public class AddPersonViewModel : DoctorBaseViewModel
    {
        [Required]
        public UserRoles Role { set; get; }
    }

    public class EditPersonViewModel : IViewErrors
    {
        [Required]
        public string Id { set; get; }

        [Required]
        [Display(Name = "Full Name")]
        public string Name { set; get; }

        [Required]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        public string Email { set; get; }

        [Required]
        [Display(Name = CnicConst.DISPLAY_NAME)]
        [RegularExpression(CnicConst.REGULAR_EXPRESSION, ErrorMessage = CnicConst.ERROR_MESSAGE)]
        [StringLength(CnicConst.MAX_LENGTH, ErrorMessage = CnicConst.ERROR_MESSAGE, MinimumLength = CnicConst.MIN_LENGTH)]
        public string Cnic { set; get; }

        [Required]
        public Gender Gender { set; get; }

        public bool IsEdited { set; get; }

        public List<string> Errors { set; get; }
    }

    public class ViewDoctorViewModel : DoctorBaseViewModel
    {
        public string Key { set; get;}

        [Display(Name = "Department")]
        public string DepartmentName { set; get; }
    }

    public class EditDoctorViewModel : ViewDoctorViewModel
    {
        public bool IsEditSuccessfully { set; get; }
    }

    public class ViewDoctorScheduleAdminViewModel
    {
        public int ScheduleId { get; set; }
        public string DoctorId { get; set; }
        public WeekDay Day { get; set; }
        public TimeSpan ArrivalTime { get; set; }
        public TimeSpan LeaveTime { get; set; }
        public string DoctorName { set; get; }
        public int AverageTime { set; get; }
    }


    public class DeleteDoctorScheduleAdminViewModel
    {
        [Required]
        public string DoctorId { get; set; }
        [Required]
        public WeekDay? Day { get; set; }
    }

    public class AddEditDoctorScheduleAdminViewModel
    {
        [Required]
        public string DoctorId { get; set; }

        public WeekDay? Day { get; set; }
        [Required]
        public TimeSpan ArrivalTime { get; set; }
        [Required]
        public TimeSpan LeaveTime { get; set; }
        [Required]
        public int AverageTime { set; get; }
    }

    #endregion DoctorsViewModels..!!

    #region DepartmentsModels..!!

    public class EditDepartmentViewModel : IViewErrors
    {
        [Required]
        public int Id { set; get; }

        [Required]
        public string Name { set; get; }

        public string Details { set; get; }

        public bool IsEditSuccessfully { set; get; }

        public List<string> Errors { set; get; }
    }

    #endregion DepartmentsModels..!!
}