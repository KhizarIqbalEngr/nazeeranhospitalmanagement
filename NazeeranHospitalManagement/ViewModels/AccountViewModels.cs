﻿using NazeeranHospitalManagement.Models.Helpers;
using NazeeranHospitalManagement.ViewModels.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using static NazeeranHospitalManagement.Models.Helpers.NazeeranHospitalManagementConstants;

namespace NazeeranHospitalManagement.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginRegisterViewModel
    {
        public LoginViewModel LoginModel { set; get; }
        public RegisterViewModel RegisterModel { set; get; }
    }

    public class LoginViewModel : IViewErrors
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }

        public List<string> Errors { set; get; }
    }

    public class RegisterViewModel : IViewErrors
    {
        [Required]
        [Display(Name = "Full Name")]
        public string FullName { set; get; }

        [Required]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(AllowEmptyStrings = CnicConst.ALLOW_EMPTY_STRINGS, ErrorMessage = CnicConst.ERROR_MESSAGE)]
        [Display(Name = CnicConst.DISPLAY_NAME)]
        //[RegularExpression(CnicConst.CNIC_REGULAR_EXPRESSION)]
        //[StringLength(CnicConst.MAX_LENGTH, MinimumLength = CnicConst.MIN_LENGTH, ErrorMessage = CnicConst.ERROR_MESSAGE)]
        public string Cnic { set; get; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Gender")]
        public Gender PatientGender { set; get; }

        public List<string> Errors { set; get; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class AddQueryMessageViewModel : IViewErrors
    {
        [Required(AllowEmptyStrings =false)]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = false)]
        [EmailAddress]
        public string Email { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        [Required(AllowEmptyStrings = false)]
        [DataType(DataType.MultilineText)]
        public string Question { get; set; }

        public List<string> Errors { set; get; }
    }

    public class ViewQueryMessageViewModel : AddQueryMessageViewModel
    {
        public int Id { get; set; }

        [DataType(DataType.MultilineText)]
        public string Answer { get; set; }

        public DateTime PostedOn { get; set; }
    }

    public class AnswerQueryMessageViewModel
    {
        [Required]
        public int Id { get; set; }

        [Required(AllowEmptyStrings =false)]
        [DataType(DataType.MultilineText)]
        [MinLength(1)]
        public string Answer { get; set; }
    }
}
