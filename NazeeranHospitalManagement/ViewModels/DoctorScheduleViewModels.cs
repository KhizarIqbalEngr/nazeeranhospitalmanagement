﻿using NazeeranHospitalManagement.Models;
using NazeeranHospitalManagement.Models.Helpers;
using NazeeranHospitalManagement.ViewModels.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NazeeranHospitalManagement.ViewModels
{
    public class ViewScheduleViewModel
    {
        public int ScheduleId { set; get; }

        [Display(Name = "Doctor Id")]
        public string DoctorId { set; get; }

        [Display(Name = "Docotor Name")]
        public string DoctorName { set; get; }

        [Display(Name = "Arrival Time")]
        public TimeSpan ArrivalTime { set; get; }

        [Display(Name = "Leave Time")]
        public TimeSpan LeaveTime { set; get; }

        public WeekDay Day { set; get; }
    }

    public class CreateScheduleViewModel
    {
        [Required]
        [Display(Name = "Arrival Time")]
        public TimeSpan ArrivalTime { set; get; }

        [Required]
        [Display(Name = "Leave Time")]
        [DataType(DataType.DateTime)]
        public TimeSpan LeaveTime { set; get; }

        [Required]
        public WeekDay Day { set; get; }
    }

    public class EditScheduleViewModel : CreateScheduleViewModel, IViewErrors
    {
        public List<string> Errors { set; get; }

        // Id of the DoctorSchedule in the database....!!!!
        public int ScheduleId { set; get; }
    }

    public class AppointmentDetailViewModel : IViewErrors
    {
        public Appointment Appointment { set; get; }
        public List<string> Errors { set; get; }
    }
}