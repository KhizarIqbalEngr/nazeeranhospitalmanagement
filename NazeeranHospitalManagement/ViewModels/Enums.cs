﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;

namespace NazeeranHospitalManagement.ViewModels.Enums
{
    public enum Gender
    {
        [Display(Name = "Male")]
        MALE,
        [Display(Name = "Female")]
        FEMALE
    }

    public enum WeekDay
    {
        [Display(Name = "Monday")]
        MON = 1,

        [Display(Name = "Tuesday")]
        TUE = 2,

        [Display(Name = "Wednesday")]
        WED = 3,

        [Display(Name = "Thursday")]
        THU = 4,

        [Display(Name = "Friday")]
        FRI = 5,

        [Display(Name = "Saturday")]
        SAT = 6,

        [Display(Name = "Sunday")]
        SUN = 0
    }

    public enum UserRoles
    {
        Admin = 1,
        Doctor,
        Pharmacist,
        LabAttendent,
        HelpDesk,
        Patient
    }

    public enum SearchBy
    {
        [Display(Name = "Select Option To Search")]
        SelectAnOption = 1,

        [Display(Name = "By Name")]
        Name = 2,

        [Display(Name = "By Dates")]
        Dates = 3,

        [Display(Name = "By Department")]
        Department = 4,
    }

    public enum AppointmentStatus
    {
        Scheduled = 1,

        [Display(Name = "Over Date")]
        OverDate = 2,

        Pending= 3,

        Cancelled = 4
    }

    public enum MedicalTestStatus
    {
        Active = 1,

        SampleCollected = 2,

        Conducted = 3,

        Completed = 4
    }

    public enum TestTypes
    {
        CrossMatch = 1,

        TFT = 2,

        BloodCP = 3,

        Calcuim = 4
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum AjaxResquestStatusCode
    {
        [Display(Name = "User Not Registered")]
        UserNotRegistered = 1,

        [Display(Name = "Exception Throws")]
        ExceptionThrows = 2,

        [Display(Name = "Invalid Model")]
        InValidModel = 3,

        [Display(Name = "Not An Ajax Request")]
        NotAnAjaxRequest = 4,

        [Display(Name = "Not An Ajax Request")]
        AppointmentAlreadyHave = 4
    }
}