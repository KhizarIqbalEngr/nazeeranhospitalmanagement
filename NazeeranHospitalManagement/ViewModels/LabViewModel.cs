﻿using NazeeranHospitalManagement.ViewModels.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NazeeranHospitalManagement.ViewModels
{
    public class ViewLabTestsViewModel
    {
        public int PatientId { set; get; }
        public string PatientName { set; get; }
        public DateTime DateOfBirth { set; get; }

        public string DoctorName { set; get; }
        public string DoctorDepartment { set; get; }

        public List<PrescribedTestsViewmodel> Tests { set; get; }
    }

    public class PrescribedTestsViewmodel
    {
        [Display(Name = "Test Name")]
        public TestTypes TestType { set; get; }

        public string Name { get; set; }

        [Display(Name = "Patient Id")]
        public int PatientId{ get; set; }

        [Display(Name = "Patient Id")]
        public int MedicalTestTypeId { get; set; }

        public int MedicalTestId { set; get; }

        public int PrescriptionId { set; get; }

        public MedicalTestStatus Status { set; get; }
    }

    public class GetTestViewModel
    {
        public int PatientId { set; get; }
        public TestTypes TestType { set; get; }
        public int MedicalTestId { set; get; }
        public int PrescriptionId { set; get; }
    }
}


