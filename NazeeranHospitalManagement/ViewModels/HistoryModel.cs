﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NazeeranHospitalManagement.ViewModels
{
    public class HistoryModel
    {
        public IEnumerable<HistoryViewmodel> PrescribedMedicinesByDoctor { set; get; }
    }

    public class Histry
    {
        public int Id { set; get; }
        public List<HistoryViewmodel> PatientHistory { set; get; }

        public string MedicineName { get; set; }
    }

    public class HistoryViewmodel
    {
        public int PatientId { get; set; }
        public string PatientName { get; set; }
        public int DoctorId { get; set; }
        public string DoctorName { get; set; }

        public int pat_Id { get; set; }
        public int MedicineId { get; set; }
        public int PrescriptionId { get; set; }
        public int Quantity { get; set; }
        public string Time { get; set; }
        public int NoOfDays { get; set; }

        public string MedicineName { get; set; }
        public string MedicineFormula { get; set; }
        public int prescription_id { get; set; }
        public int medicine_id { get; set; }

        [Display(Name = "Name")]
        public string medicine_name { get; set; }
        [Display(Name = "Quantity")]
        public int medicine_quantity { get; set; }
        [Display(Name = "Time of day")]
        public string medicine_time { get; set; }
        [Display(Name = "Number of day")]
        public int medicine_noOfday { get; set; }
        [Display(Name = "Price Per Unit")]
        public double medicine_price { get; set; }
        [Display(Name = "Price")]
        public double price { get; set; }
        public double amount { get; set; }
    }
}