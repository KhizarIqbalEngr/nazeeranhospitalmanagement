﻿using NazeeranHospitalManagement.Models.Helpers;
using NazeeranHospitalManagement.ViewModels.Enums;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using static NazeeranHospitalManagement.Models.Helpers.NazeeranHospitalManagementConstants;

namespace NazeeranHospitalManagement.ViewModels
{
    /// <summary>
    /// This class is used as the base class for most of the Get DoctorSchedule View Models!
    /// </summary>
    public abstract class AppointmentBaseViewModel
    {
        [Display(Name = "Average Time")]
        public int AverageTime { set; get; }

        [Display(Name = "Date")]
        [DisplayFormat(DataFormatString = "{d/M/yy}")]
        [DataType(DataType.Date)]
        public DateTime? Date { set; get; }

        //[Display(Name = "Leave Time")]
        //[DataType(DataType.Time)]
        //public TimeSpan LeaveTime { set; get; }

        [Display(Name = "Doctor Name")]
        public string DoctorName { set; get; }

        [Display(Name = "Department Id")]
        public int DepartmentId { set; get; }

        [Display(Name = "Department Name")]
        public string DepartmentName { set; get; }
    }

    /// <summary>
    /// This View Model Is Used For Searching The Doctor (By Name, Date, Department) And View It's Schedule..!!
    /// </summary>
    public class SearchScheduleViewModel : AppointmentBaseViewModel, IValidatableObject
    {
        [Display(Name = "Select Department")]
        public IEnumerable<SelectListItem> DepartmentList { set; get; }

        [Required]
        [Display(Name = "Search By")]
        public SearchBy SearchOption { set; get; }

        public SearchBy FilterOption { set; get; }

        public bool IsFiltered { set; get; }

        [ReadOnly(true)]
        public IEnumerable<ViewDoctorsViewModel> DoctorSchedule { set; get; }

        public bool WantCancelling { set; get; }
        public int OldAppointmentId { set; get; }
        public ViewAppointmentViewModel OldAppointment { set; get; }

        public SearchAppointmentViewModel SearchAppointmentViewModel { set; get; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            switch(SearchOption)
            {
                case SearchBy.Name:
                    {
                        if (string.IsNullOrEmpty(DoctorName))
                            yield return new ValidationResult("Please, enter a doctor name to search");
                    }
                    break;

                case SearchBy.Department:
                    if (DepartmentId <= 0)
                        yield return new ValidationResult("Invalid Department Id..!!");
                    break;

                case SearchBy.Dates:
                    if(!Date.HasValue)
                        yield return new ValidationResult("Please Enter A Valid Date..!!");

                    if (Date.HasValue && Date.Value.Date < DateTime.Now.Date)
                        yield return new ValidationResult("Date Can't Be In The Past..!!");
                    break;

                case SearchBy.SelectAnOption:
                    yield return new ValidationResult("Not a valid search option...!!");
                    break;
            }

            yield return ValidationResult.Success;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ViewDoctorsViewModel : AppointmentBaseViewModel
    {
        [Required]
        [Display(Name = "Doctor Id")]
        public string DoctorId { set; get; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ViewAppointmentViewModel 
    {
        public int PatientId { get; set; }
        public string PatientCnic { get; set; }
        public Gender PatientGender { set; get; }
        public string PatientEmail { get; set; }
        public string PatientContactNo { set; get; }

        [Display(Name = "Appointment Id")]
        public int AppointmentId { set; get; }

        [Display(Name = "Doctor Name")]
        public string DoctorName { set; get; }

        [Display(Name = "Department Name")]
        public string DepartmentName { set; get; }

        [Display(Name = "Availabe Day")]
        public WeekDay Day { set; get; }

        [Display(Name = "Appointment Time")]
        public DateTime AppointmentTime { set; get; }

        [Display(Name = "Patient Name")]
        public string PatientName { set; get; }

        [Display(Name = "Appointment Status")]
        public AppointmentStatus Status { set; get; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class SearchAppointmentViewModel : IViewErrors
    {
        // Used to view or cancel the appointment by the user
        [Required]
        [Display(Name = "Appointment Id")]
        public int AppointmentId { set; get; }

        [Required]
        [Display(Name = CnicConst.DISPLAY_NAME)]
        [RegularExpression(CnicConst.REGULAR_EXPRESSION, ErrorMessage = CnicConst.ERROR_MESSAGE)]
        [StringLength(CnicConst.MAX_LENGTH, ErrorMessage = CnicConst.ERROR_MESSAGE, MinimumLength = CnicConst.MIN_LENGTH)]
        public string Cnic { set; get; }

        [Display(Name = "Appointment No")]
        public int AppointmentNo { get; set; }

        [Display(Name = "Doctor")]
        public string DoctorName { get; set; }

        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        public TimeSpan Time { get; set; }

        [Display(Name = "Appointment Status")]
        public AppointmentStatus State { get; set; }

        public bool IsFound { set; get; }

        public bool WantCancelling { set; get; }

        public List<string> Errors { set; get; }
    }

    /// <summary>
    /// This Class Is Used To Get Scheculed For the doctor.!
    /// </summary>
    public class GetScheduleViewModel
    {
        [Required]
        public string DoctorId { set; get; }

        // The Start Date From Where Doctor Schedule Will Be Seached..!!
        //[DataType(DataType.Date)]
        [Display(Name = "Start Date")]
        [DisplayFormat(DataFormatString = "{0:dd/M/yyy}", ApplyFormatInEditMode = true)]
        public DateTime StartDate { set; get; }

        // The End Date From Where Doctor Schedule Will Be Ended..!!
        //[DataType(DataType.Date)]
        [Display(Name = "End Date")]
        [DisplayFormat(DataFormatString = "{0:dd/M/yyy}", ApplyFormatInEditMode = true)]
        public DateTime EndDate { set; get; }

        public bool WantCancelling { set; get; }
        public int OldAppointmentId { set; get; }
        public ViewAppointmentViewModel OldAppointment { set; get; }
    }

    /// <summary>
    /// This View Model Is Used To Hold The Details of the Doctor Appointments And Details about All of The Appointment Slots..!!
    /// </summary>
    public class DoctorAppointmentsViewModel
    {
        public WeekDay Day { set; get; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date { set; get; }

        [ReadOnly(true)]
        public IEnumerable<AppointmentSlotViewModel> AppointmentSlots { set; get; }
    }

    /// <summary>
    /// This View Model Is Used To View The Detials Of Doctor Appointment Schedules Details...!!!
    /// </summary>
    public class DoctorScheduleViewModel : IViewErrors
    {
        [Display(Name = "Doctor Name")]
        public string DoctorName { set; get; }

        public GetScheduleViewModel GetScheduleModel { set; get; }

        [ReadOnly(true)]
        public IEnumerable<DoctorAppointmentsViewModel> Appointments { set; get; }

        public List<string> Errors { set; get; }

        public bool WantCancelling { set; get; }
        public int OldAppointmentId { set; get; }
        public ViewAppointmentViewModel OldAppointment { set; get; }
        public BookAppointmentViewModel BookAppointmentViewModel { set; get; }
    }

    /// <summary>
    /// This View Model Is Used For An Appointment Slost!
    /// </summary>
    public class AppointmentSlotViewModel
    {
        public WeekDay Day { set; get; }

        public DateTime Time { set; get; }

        public bool IsBooked { set; get; }

        public int SlotNo { set; get; }
    }
    
    /// <summary>
    /// This View Model Is Used While Booking The Appointment!
    /// </summary>
    public class BookAppointmentViewModel : IViewErrors
    {
        [Display(Name = "Appointment No")]
        [ReadOnly(true)]
        public int AppointmentNo { set; get; }

        [Display(Name = "Apppointment Id")]
        [ReadOnly(true)]
        public int AppointmentId { set; get; }

        [Display(Name = "Doctor Name")]
        public string DoctorName { set; get; }

        [Required]
        [Display(Name = "Doctor Id")]
        public string DoctorId { set; get; }
        
        [Required]
        public WeekDay Day { set; get; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime Date { set; get; }

        [Display(Name = "Patient Id")]
        public int PatientId { set; get; }

        [Required]
        [Display(Name = "Patient Name")]
        public string PatientName { set; get; }

        [Required]
        [Display(Name = CnicConst.DISPLAY_NAME)]
        [RegularExpression(CnicConst.REGULAR_EXPRESSION, ErrorMessage = CnicConst.ERROR_MESSAGE)]
        [StringLength(CnicConst.MAX_LENGTH, ErrorMessage = CnicConst.ERROR_MESSAGE, MinimumLength = CnicConst.MIN_LENGTH)]
        public string Cnic { set; get; }

        [Required]
        public Gender Gender { set; get; }

        [EmailAddress]
        public string Email { set; get; }

        [Display(Name = "Contact No")]
        public string ContactNo { set; get; }

        [Required]
        public int SlotNo { set; get; }

        public List<string> Errors { set; get; }

        public bool IsAppointmentBook { set; get; }

        public AppointmentStatus Status { set; get; }

        public bool WantCancelling { set; get; }
        public int OldAppointmentId { set; get; }
    }

    public class AddPetientViewModel : IViewErrors
    {
        [Display(Name = "Patient Id")]
        public int PatientId { set; get; }

        [Required]
        [Display(Name = "Patient Name")]
        public string PatientName { set; get; }

        [Required]
        [Display(Name = CnicConst.DISPLAY_NAME)]
        [RegularExpression(CnicConst.REGULAR_EXPRESSION, ErrorMessage = CnicConst.ERROR_MESSAGE)]
        [StringLength(CnicConst.MAX_LENGTH, ErrorMessage = CnicConst.ERROR_MESSAGE, MinimumLength = CnicConst.MIN_LENGTH)]
        public string Cnic { set; get; }

        [Required]
        public Gender Gender { set; get; }

        [EmailAddress]
        [Required]
        public string Email { set; get; }

        [Required]
        [Display(Name = "Contact No")]
        public string PhoneNumber { set; get; }

        public bool IsAdded { set; get; }
        
        public List<string> Errors { set; get; }
    }

    public class ResheduleAppointmentViewModel
    {
        [Required]
        public int AppointmentId { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string PatientId { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string DoctorId { get; set; }

        [Required(AllowEmptyStrings = false)]
        public TimeSpan Time { get; set; }

        [Required(AllowEmptyStrings = false)]
        public DateTime Date { get; set; }

        [Required(AllowEmptyStrings = false)]
        [RegularExpression(CnicConst.REGULAR_EXPRESSION, ErrorMessage = CnicConst.ERROR_MESSAGE)]
        [StringLength(CnicConst.MAX_LENGTH, ErrorMessage = CnicConst.ERROR_MESSAGE, MinimumLength = CnicConst.MIN_LENGTH)]
        public string Cnic { set; get; }

        [Required]
        public Gender Gender { set; get; }

        [Required(AllowEmptyStrings = false)]
        [EmailAddress]
        public string Email { set; get; }

        [Required(AllowEmptyStrings = false)]
        public string ContactNo { set; get; }

        public int SlotNo { set; get; }
    }
}


