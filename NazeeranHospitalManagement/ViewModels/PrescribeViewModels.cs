﻿using NazeeranHospitalManagement.Models;
using NazeeranHospitalManagement.Models.Helpers;
using NazeeranHospitalManagement.ViewModels.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NazeeranHospitalManagement.ViewModels
{
    public abstract class MedicinesBaseViewModel
    {
        public int Quantity { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Time { get; set; }

        [Required]
        [MinValue(minValue: 1)]
        public int NoOfDays { get; set; }

        [Required]
        public int MedicineId { set; get; }
    }

    public class AddedMedinesViewModel : MedicinesBaseViewModel
    {
        public int PatientId { set; get; }
        public int DoctorId { set; get; }
        public int PrescriptionId { set; get; }
    }

    public class PrescribedMedicinesViewmodel : MedicinesBaseViewModel
    {
        public int ItemId { set; get; }
        public string MedicineName { set; get; }
        public string MedicineFormula { set; get; }
        public DateTime When { set; get; }
    }

    public class AddPrescriptionViewModel
    {
        public int PatientId { set; get; }
        public int DoctorId { set; get; }
        public int PrescriptionId { set; get; }

        public List<AddedMedinesViewModel> PrescribedMedicines { set; get; }
    }

    public class PrescribeSendModel
    {
        [Required]
        public int PatientId { get; set; }
        public string PatientName { get; set; }
        public Gender Gender { set; get; }
        public DateTime DateOfBirth { set; get; }

        [Required]
        public int DoctorId { get; set; }
        public string DoctorName { get; set; }
        public string DoctorDepartment { set; get; }

        public int PrescriptionId { get; set; }

        public IEnumerable<PrescribedMedicinesViewmodel> PrescribedMedicinesByDoctor { set; get; }
        public IEnumerable<PrescribedMeicalTestViewModel> PrescribedTestsByDoctor { set; get; }

        public PrescribiedTestResultViewModel AllTestsWithResults { set; get; }

        public IEnumerable<MedicalTest> AllTests { set; get; }
        public GeneralTestsViewModel GenralTests { set; get; }
    }

    public class PrescribiedTestResultViewModel
    {
        public IEnumerable<MedicalTest> AllTests { set; get; }

        public IEnumerable<BloodCP> BloodCpTests { set; get; }
        public IEnumerable<TFT> TftTests { set; get; }
        public IEnumerable<Calcium> CalciumTests { set; get; }
        public IEnumerable<CrossMatch> CrossMatchTests { set; get; }
    }

    public class A
    {
        public int idd { set; get; }
        public List<PrescribeMedicinesViewmodel> m1 { set; get; }

        public string MedicineName { get; set; }
    }

    public class AddMedicalTestsiewModel
    {
        [Required]
        public int PatientId { set; get; }

        [Required]
        public int TestId { set; get; }

        public int PrescriptionId { set; get; }
    }

    public class PrescribeMedicinesViewmodel
    {
        public int PatientId { get; set; }
        public int MedicineId { get; set; }
        public int PrescriptionId { get; set; }
        public int Quantity { get; set; }
        public string Time { get; set; }
        public int NoOfDays { get; set; }

        public string MedicineName { get; set; }
        public string MedicineFormula { get; set; }
        public int prescription_id { get; set; }
        public int medicine_id { get; set; }

        [Display(Name = "Name")]
        public string medicine_name { get; set; }
        public string TypeM { get; set; }
        public string PotencyM { get; set; }
        [Display(Name = "Quantity")]
        public int medicine_quantity { get; set; }
        [Display(Name = "Time of day")]
        public string medicine_time { get; set; }
        [Display(Name = "Number of day")]
        public int medicine_noOfday { get; set; }
        [Display(Name = "Price Per Unit")]
        public double medicine_price { get; set; }
        [Display(Name = "Price")]
        public double price { get; set; }
        public double amount { get; set; }
    }

    public class PrescribedMeicalTestViewModel
    {
        public int ItemId { set; get; }

        public string TestName { set; get; }

        public MedicalTestStatus Status { set; get; }

        public DateTime Date { set; get; }
    }

    public class BillMedicinesViewmodel
    {
        [Display(Name = "Name")]
        public string medicine_name1 { get; set; }
        [Display(Name = "Quantity")]
        public int medicine_quantity1 { get; set; }
        [Display(Name = "Time of day")]
        public string medicine_time1 { get; set; }
        [Display(Name = "Number of day")]
        public int medicine_noOfday1 { get; set; }
        [Display(Name = "Price Per Unit")]
        public double medicine_price1 { get; set; }
        [Display(Name = "Price")]
        public double price1 { get; set; }
        public double amount { get; set; }

        public bool ab1 { get; set; }
    }

    public class GeneralTestsViewModel
    {
        public BloodPressueViewModel BloodPressue { set; get; }
        public SugarTestViewModel SugarTest { set; get; }
        public HeartRateViewModel HeartRate { set; get; }
        public HeightWeightViewModel HeightWeight { set; get; }
    }

    public class BloodPressueViewModel
    {
        public bool HasBloodLow { set; get; }
        public int BloodLowValue { set; get; }

        public bool HasBloodHigh { set; get; }
        public int BloodHighValue { set; get; }
    }

    public class SugarTestViewModel
    {
        public bool HasBeforeMeal { set; get; }
        public int BeforeMealValue { set; get; }

        public bool HasFasting { set; get; }
        public int FastingValue { set; get; }
    }

    public class HeartRateViewModel
    {
        public bool HasValue { set; get; }
        public int HeartRate { set; get; }
    }

    public class HeightWeightViewModel
    {
        public bool HasHeightValue { set; get; }
        public double HeightValue { set; get; }

        public bool HasWeightValue { set; get; }
        public double WeightValue { set; get; }
    }
}