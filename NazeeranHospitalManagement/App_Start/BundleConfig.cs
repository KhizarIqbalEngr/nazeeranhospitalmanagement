﻿using System.Web.Optimization;

namespace NazeeranHospitalManagement
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/theme_css").Include(
                //"~/Content/bootstrap.css",
                "~/Content/chocolat.css",
                "~/Content/style.css"
                ));

            bundles.Add(new StyleBundle("~/Content/bootstrap_css").Include(
                "~/Content/font-awesome.min.css",
                "~/Content/bootstrap-theme.css",
                "~/Content/bootstrap.css"
                ));

            bundles.Add(new StyleBundle("~/Content/animation_css").Include(
               "~/Content/animate.css",
               "~/Content/SweetAlert/sweetalert2.css"
                ));

            bundles.Add(new StyleBundle("~/Content/signin_signup_css").Include(
               "~/Content/normalize.css",
               "~/Content/signin_signup.css"
                ));

            bundles.Add(new StyleBundle("~/Content/jquery_ui_css").Include(
               //"~/Content/jquery-ui.css",
               "~/Content/jquery.datetimepicker.css"
                ));

            bundles.Add(new StyleBundle("~/Content/custom_css").Include(
                "~/Content/pdsa-sidebar.css",
                "~/Content/pdsa-summary-block.css",
                "~/Content/pdsa-readme-box.css",
                "~/Content/site.css"
                //"~/Content/Tooltipster/tooltipster.css"
                ));

            bundles.Add(new ScriptBundle("~/Content/modernizr").Include(
                "~/Scripts/modernizr.custom.theme.js"
                ));

            bundles.Add(new StyleBundle("~/Content/theme_js").Include(
                "~/Scripts/easing.js",
                "~/Scripts/jquery.chocolat.js",
                "~/Scripts/move-top.js",
                "~/Scripts/responsiveslides.min.js"
                ));

            bundles.Add(new ScriptBundle("~/Content/custom_js").Include(
                "~/Scripts/jquery.maskedinput.js",
                "~/Scripts/Tooltipster/jquery.tooltipster.min.js",
                "~/Scripts/Script.js"
                ));

            bundles.Add(new ScriptBundle("~/Content/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/Content/bootstrap_js").Include(
                "~/Scripts/jquery-2.2.1.js",
                "~/Scripts/bootstrap.js"
                ));

            bundles.Add(new ScriptBundle("~/Content/animation_js").Include(
               "~/Scripts/SweetAlert/sweetalert2.all.js"
                ));

            bundles.Add(new ScriptBundle("~/Content/jquery_ui_js").Include(
               "~/Scripts/jquery.datetimepicker.full.js"
                ));

            //BundleTable.EnableOptimizations = true;
        }
    }
}

