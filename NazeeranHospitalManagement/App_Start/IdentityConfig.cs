﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using NazeeranHospitalManagement.Models;
using System;
using System.Configuration;
using System.Security.Claims;
using System.Threading.Tasks;

namespace NazeeranHospitalManagement
{
    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your email service here to send an email.
            return Task.FromResult(0);
        }
    }

    public class SmsService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            var AccountSid = ConfigurationManager.AppSettings["SMSAccountIdentification"];
            var AuthToken = ConfigurationManager.AppSettings["SMSAccountPassword"];
            //var Twilio = new TwilioRestClient(AccountSid, AuthToken);

            //var result = Twilio.SendMessage(
            //  System.Configuration.ConfigurationManager.AppSettings["SMSAccountFrom"],
            //  message.Destination, message.Body
            //);

            //Status is one of Queued, Sending, Sent, Failed or null if the number is not valid
            //Debug.Write(result.Status);
            //Trace.TraceInformation(result.Status);
            //Twilio doesn't currently have an async API, so return success.

            return Task.FromResult(0);
        }
    }

    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
    public class NazeeranHospitalUserManager : UserManager<NazeeranHospitalUser>
    {
        public NazeeranHospitalUserManager(IUserStore<NazeeranHospitalUser> store)
            : base(store)
        {
        }

        public static NazeeranHospitalUserManager Create(IdentityFactoryOptions<NazeeranHospitalUserManager> options, IOwinContext context) 
        {
            var manager = new NazeeranHospitalUserManager(new UserStore<NazeeranHospitalUser>(context.Get<NazeeranHospitalDbContext>()));
            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<NazeeranHospitalUser>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false,
            };

            // Configure user lockout defaults
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 5;

            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug it in here.
            manager.RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<NazeeranHospitalUser>
            {
                MessageFormat = "Your security code is {0}"
            });
            manager.RegisterTwoFactorProvider("Email Code", new EmailTokenProvider<NazeeranHospitalUser>
            {
                Subject = "Security Code",
                BodyFormat = "Your security code is {0}"
            });
            manager.EmailService = new EmailService();
            manager.SmsService = new SmsService();
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider = 
                    new DataProtectorTokenProvider<NazeeranHospitalUser>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }
    }

    // Configure the application sign-in manager which is used in this application.
    public class NazeeranHospitalSignInManager : SignInManager<NazeeranHospitalUser, string>
    {
        public NazeeranHospitalSignInManager(NazeeranHospitalUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(NazeeranHospitalUser user)
        {
            return user.GenerateUserIdentityAsync((NazeeranHospitalUserManager)UserManager);
        }

        public static NazeeranHospitalSignInManager Create(IdentityFactoryOptions<NazeeranHospitalSignInManager> options, IOwinContext context)
        {
            return new NazeeranHospitalSignInManager(context.GetUserManager<NazeeranHospitalUserManager>(), context.Authentication);
        }
    }

    public class NazeeranHospitalRoleManager : RoleManager<IdentityRole>
    {
        public NazeeranHospitalRoleManager(IRoleStore<IdentityRole, string> store) : base(store)
        {
        }

        public static NazeeranHospitalRoleManager Create(IdentityFactoryOptions<NazeeranHospitalRoleManager> options, IOwinContext context)
        {
            var roleStore = new RoleStore<IdentityRole>(context.Get<NazeeranHospitalDbContext>());
            return new NazeeranHospitalRoleManager(roleStore);
        }
    }
}
