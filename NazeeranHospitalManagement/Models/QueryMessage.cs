﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace NazeeranHospitalManagement.Models
{
    public class QueryMessage
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Question { get; set; }
        public DateTime PostedOn { get; set; }
        public string Answer { get; set; }
        public DateTime AnswerOn { get; set; }

        [ForeignKey("AnswerBy")]
        public string UserId { get; set; }
        public NazeeranHospitalUser AnswerBy { get; set; }
    }
}