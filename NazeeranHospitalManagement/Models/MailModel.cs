﻿using System.ComponentModel.DataAnnotations;

namespace NazeeranHospitalManagement.Models
{
    public class MailModel
    {
        public string From { get; set; }
        [Required]
        [EmailAddress]
        public string To { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}