﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace NazeeranHospitalManagement.Models
{

    public class NazeeranHospitalDbContext : IdentityDbContext<NazeeranHospitalUser>
    {
        public NazeeranHospitalDbContext()
            : base("NazeeranHospitalDbConnection", throwIfV1Schema: false)
        {
        }

        public static NazeeranHospitalDbContext Create()
        {
            return new NazeeranHospitalDbContext();
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<NazeeranHospitalUser>().ToTable("User");
            modelBuilder.Entity<IdentityUserRole>().ToTable("UserRole");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("UserLogin");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("UserClaim");
            modelBuilder.Entity<IdentityRole>().ToTable("Role");
        }

        public virtual DbSet<Appointment> Appointments { get; set; }
        public virtual DbSet<BloodCP> BloodCPs { get; set; }
        public virtual DbSet<BloodPressueTest> BloodPressueTests { get; set; }
        public virtual DbSet<Calcium> Calciums { get; set; }
        public virtual DbSet<CrossMatch> CrossMatches { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<Doctor> Doctors { get; set; }
        public virtual DbSet<DoctorSchedule> DoctorSchedules { get; set; }
        public virtual DbSet<HeartRateTest> HeartRateTests { get; set; }
        public virtual DbSet<HeightWeightTest> HeightWeightTests { get; set; }
        public virtual DbSet<MedicalTest> MedicalTests { get; set; }
        public virtual DbSet<MedicalTestType> MedicalTestTypes { get; set; }
        public virtual DbSet<MedicineBill> MedicineBills { get; set; }
        public virtual DbSet<Medicine> Medicines { get; set; }
        public virtual DbSet<Patient> Patients { get; set; }
        public virtual DbSet<PrescribedMedicine> PrescribedMedicines { get; set; }
        public virtual DbSet<Prescription> Prescriptions { get; set; }
        public virtual DbSet<SugarTest> SugarTests { get; set; }
        public virtual DbSet<TFT> TFTs { get; set; }
        public virtual DbSet<ChatMessage> ChatMessages { set; get; }
        public virtual DbSet<QueryMessage> QueryMessages { get; set; }
    }
}