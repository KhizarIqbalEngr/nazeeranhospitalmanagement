using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace NazeeranHospitalManagement.Models
{
    public partial class MedicineBill
    {
        public int Id { get; set; }
        public double Amount { get; set; }
        public DateTime BillTime { get; set; }

        [ForeignKey("Patient")]
        public int PatientId { get; set; }
        public virtual Patient Patient { get; set; }

        [ForeignKey("Pharmacist")]
        public string PharmacistId { get; set; }
        public virtual NazeeranHospitalUser Pharmacist { get; set; }
    }
}
