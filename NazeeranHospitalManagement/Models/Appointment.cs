using NazeeranHospitalManagement.ViewModels.Enums;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace NazeeranHospitalManagement.Models
{
    public partial class Appointment
    {
        public int Id { get; set; }
        public int AppointmentNo { get; set; }
        public DateTime Date { get; set; }
        public TimeSpan Time { get; set; }
        public AppointmentStatus Status { get; set; }

        [ForeignKey("Doctor")]
        public int DoctorId { get; set; }
        public virtual Doctor Doctor { get; set; }

        [ForeignKey("Patient")]
        public int PatientId { get; set; }
        public virtual Patient Patient { get; set; }
    }
}
