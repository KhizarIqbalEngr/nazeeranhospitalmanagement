using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace NazeeranHospitalManagement.Models
{
    public partial class BloodPressueTest
    {
        public int Id { get; set; }
        public Nullable<int> LowValue { get; set; }
        public Nullable<int> HighValue { get; set; }

        [ForeignKey("Patient")]
        public int PatientId { get; set; }
        public virtual Patient Patient { get; set; }
    }
}
