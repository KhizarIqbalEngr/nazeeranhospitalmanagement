using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace NazeeranHospitalManagement.Models
{
    public partial class PrescribedMedicine
    {
        public int Id { get; set; }
        public int Quantity { get; set; }
        public string Time { get; set; }
        public int NoOfDays { get; set; }
        public DateTime PrescribedOn { get; set; }

        [ForeignKey("Medicine")]
        public int MedicineId { get; set; }
        public virtual Medicine Medicine { get; set; }

        [ForeignKey("Prescription")]
        public int PrescriptionId { get; set; }
        public virtual Prescription Prescription { get; set; }
    }
}
