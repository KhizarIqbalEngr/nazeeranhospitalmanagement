﻿/**
* Calculate Age in C#
* https://gist.github.com/faisalman
*
* Copyright 2012-2013, Faisalman <fyzlman@gmail.com>
* Licensed under The MIT License
* http://www.opensource.org/licenses/mit-license
* https://gist.github.com/faisalman/1724253
*/

using System;

namespace NazeeranHospitalManagement.Models.Helpers
{
    public class Age
    {
        public int Years;
        public int Months;
        public int Days;

        public Age()
        {
        }

        public static Age GetAge(DateTime Bday, DateTime Cday)
        {
            Age age = new Age();

            if ((Cday.Year - Bday.Year) > 0 ||
                (((Cday.Year - Bday.Year) == 0) && ((Bday.Month < Cday.Month) ||
                  ((Bday.Month == Cday.Month) && (Bday.Day <= Cday.Day)))))
            {
                int DaysInBdayMonth = DateTime.DaysInMonth(Bday.Year, Bday.Month);
                int DaysRemain = Cday.Day + (DaysInBdayMonth - Bday.Day);

                if (Cday.Month > Bday.Month)
                {
                    age.Years = Cday.Year - Bday.Year;
                    age.Months = Cday.Month - (Bday.Month + 1) + Math.Abs(DaysRemain / DaysInBdayMonth);
                    age.Days = (DaysRemain % DaysInBdayMonth + DaysInBdayMonth) % DaysInBdayMonth;
                }
                else if (Cday.Month == Bday.Month)
                {
                    if (Cday.Day >= Bday.Day)
                    {
                        age.Years = Cday.Year - Bday.Year;
                        age.Months = 0;
                        age.Days = Cday.Day - Bday.Day;
                    }
                    else
                    {
                        age.Years = (Cday.Year - 1) - Bday.Year;
                        age.Months = 11;
                        age.Days = DateTime.DaysInMonth(Bday.Year, Bday.Month) - (Bday.Day - Cday.Day);
                    }
                }
                else
                {
                    age.Years = (Cday.Year - 1) - Bday.Year;
                    age.Months = Cday.Month + (11 - Bday.Month) + Math.Abs(DaysRemain / DaysInBdayMonth);
                    age.Days = (DaysRemain % DaysInBdayMonth + DaysInBdayMonth) % DaysInBdayMonth;
                }
            }
            else
            {
                throw new ArgumentException("Birthday date must be earlier than current date");
            }

            return age;
        }
    }
}