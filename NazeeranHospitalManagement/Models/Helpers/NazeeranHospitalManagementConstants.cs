﻿namespace NazeeranHospitalManagement.Models.Helpers
{
    public static class NazeeranHospitalManagementConstants
    {
        public static class CnicConst
        {
            public const string REGULAR_EXPRESSION = @"\d{5}-\d{7}-\d{1}";
            public const string ERROR_MESSAGE = @"Please Enter A Valid Cnic of length 15 with dashes!!";
            public const string DISPLAY_NAME = "CNIC";
            public const int MAX_LENGTH = 15;
            public const int MIN_LENGTH = 15;
            public const bool ALLOW_EMPTY_STRINGS = false;
        }

        public static class FullNameConst
        {
            public const string REGULAR_EXPRESSION = @"[a-zA-Z ]*";
            public const string ERROR_MESSAGE = "Only Alphabets And Spaces Are Allowed..!!";
            public const string DISPLAY_NAME = "Full Name";
            public const int MAX_LENGTH = int.MaxValue;
            public const int MIN_LENGTH = 3;
        }

        public static class UserNameConst
        {
            public const string REGULAT_EXPRESSION = @"[a-zA-z0-9 _]*";
            public const string DISPLAY_NAME = "User Name";
            public const string ERROR_MESSAGE = "Only Alphabets, Digitis, Spaces And Underscores Are Allowed..!!!";
        }

        public static class LayoutNameConst
        {
            public const string DefaultLayout = "~/Views/Shared/_Layout.cshtml";
            public const string LayoutPatientPartial = "~/Views/Shared/_DashboardPatient.cshtml";
            public const string LayoutDashboardAdminPartial = "~/Views/Shared/_DashboardAdminLayout.cshtml";
            public const string LayoutDashboardDoctorPartial = "~/Views/Shared/_DashboardDoctorLayout.cshtml";
            public const string LDAdmin = "~/Views/Shared/_DashboardAdminLayoutAdmin.cshtml";
        }
    }
}