﻿using System.Collections.Generic;

namespace NazeeranHospitalManagement.Models.Helpers
{
    public interface IViewErrors
    {
        List<string> Errors { set; get; }
    }
}
