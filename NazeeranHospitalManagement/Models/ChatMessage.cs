﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace NazeeranHospitalManagement.Models
{
    public class ChatMessage
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public bool IsSeen { get; set; }
        public DateTime Time { get; set; }

        [ForeignKey("FromUser")]
        public string FromUserId { get; set; }
        public NazeeranHospitalUser FromUser { get; set; }

        [ForeignKey("ToUser")]
        public string ToUserId { get; set; }
        public NazeeranHospitalUser ToUser { get; set; }
    }
}