using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace NazeeranHospitalManagement.Models
{
    public partial class Prescription
    {
        public Prescription()
        {
            this.BloodCPs = new HashSet<BloodCP>();
            this.Calciums = new HashSet<Calcium>();
            this.CrossMatches = new HashSet<CrossMatch>();
            this.MedicalTests = new HashSet<MedicalTest>();
            this.PrescribedMedicines = new HashSet<PrescribedMedicine>();
            this.TFTs = new HashSet<TFT>();
        }
    
        public int Id { get; set; }
        public DateTime Date { get; set; }

        [ForeignKey("Doctor")]
        public int DoctorId { get; set; }

        public virtual Doctor Doctor { get; set; }

        [ForeignKey("Patient")]
        public int PatientId { get; set; }
        public virtual Patient Patient { get; set; }

        public virtual ICollection<BloodCP> BloodCPs { get; set; }
        public virtual ICollection<Calcium> Calciums { get; set; }
        public virtual ICollection<CrossMatch> CrossMatches { get; set; }
        public virtual ICollection<MedicalTest> MedicalTests { get; set; }
        public virtual ICollection<TFT> TFTs { get; set; }
        public virtual ICollection<PrescribedMedicine> PrescribedMedicines { get; set; }
    }
}
