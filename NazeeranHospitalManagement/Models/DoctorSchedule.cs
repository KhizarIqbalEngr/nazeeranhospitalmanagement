using NazeeranHospitalManagement.ViewModels.Enums;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace NazeeranHospitalManagement.Models
{
    public partial class DoctorSchedule
    {
        public int Id { get; set; }
        public TimeSpan ArrivalTime { get; set; }
        public TimeSpan LeaveTime { get; set; }
        public WeekDay Day { get; set; }

        [ForeignKey("User")]
        public string UserId { get; set; }
        public virtual NazeeranHospitalUser User { get; set; }

        [ForeignKey("Doctor")]
        public int DoctorId { get; set; }
        public Doctor Doctor { get; set; }
    }
}
