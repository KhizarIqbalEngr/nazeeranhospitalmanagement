﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using NazeeranHospitalManagement.Models.Helpers;
using NazeeranHospitalManagement.ViewModels.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;

using System.Threading.Tasks;

namespace NazeeranHospitalManagement.Models
{
    public class NazeeranHospitalUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<NazeeranHospitalUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            
            // Add custom user claims here
            return userIdentity;
        }

        [Required]
        [Display(Name = "Full Name")]
        public string Name { set; get; } // Represent Full Name in the view model.

        [Required]
        public Gender Gender { set; get; }

        [Index(IsUnique = true)]
        [Required]
        [StringLength(NazeeranHospitalManagementConstants.CnicConst.MAX_LENGTH, MinimumLength = NazeeranHospitalManagementConstants.CnicConst.MIN_LENGTH, ErrorMessage = NazeeranHospitalManagementConstants.CnicConst.ERROR_MESSAGE)]
        public string Cnic { set; get; }
    }
}