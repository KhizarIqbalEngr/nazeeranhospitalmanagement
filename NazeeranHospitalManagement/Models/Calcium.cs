using System.ComponentModel.DataAnnotations.Schema;

namespace NazeeranHospitalManagement.Models
{
    public partial class Calcium
    {
        public int Id { get; set; }
        public double calcium1 { get; set; }
        public string calciumUnit { get; set; }


        [ForeignKey("Prescription")]
        public int PrescriptionId { get; set; }
        public virtual Prescription Prescription { get; set; }
    }
}
