using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NazeeranHospitalManagement.Models
{
    public partial class CrossMatch
    {
        public int Id { get; set; }

        [Display(Name = "MRN Of Donor")]
        public int MRNOfDonor { get; set; }

        [Display(Name = "Name Of Donor")]
        public string NameOfDonor { get; set; }

        [Display(Name = "Age Of Donor")]
        public int AgeOfDonor { get; set; }

        [Display(Name = "Relation Of Donor With Patient")]
        public string RelationOfDonorWithPatient { get; set; }

        [Display(Name = "BloodB ag NO")]
        public string BloodBagNO { get; set; }

        [Display(Name = "Patient Blood Group")]
        public string PatientBloodGroup { get; set; }

        [Display(Name = "Cross Match Value ")]
        public string CrossMatchValue { get; set; }

        [ForeignKey("Prescription")]
        public int PrescriptionId { get; set; }

        public virtual Prescription Prescription { get; set; }
    }
}
