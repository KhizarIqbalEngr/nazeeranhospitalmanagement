using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace NazeeranHospitalManagement.Models
{
    public partial class SugarTest
    {
        public int Id { get; set; }
        public Nullable<int> BeforeMealValue { get; set; }
        public Nullable<int> FastingValue { get; set; }


        [ForeignKey("Patient")]
        public int PatientId { get; set; }
        public virtual Patient Patient { get; set; }
    }
}
