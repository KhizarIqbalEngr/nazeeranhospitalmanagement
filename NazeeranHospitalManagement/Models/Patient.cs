using NazeeranHospitalManagement.ViewModels.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace NazeeranHospitalManagement.Models
{
    public partial class Patient
    {
        public Patient()
        {
            this.Appointments = new HashSet<Appointment>();
            this.BloodCPs = new HashSet<BloodCP>();
            this.BloodPressueTests = new HashSet<BloodPressueTest>();
            this.Calciums = new HashSet<Calcium>();
            this.CrossMatches = new HashSet<CrossMatch>();
            this.HeartRateTests = new HashSet<HeartRateTest>();
            this.HeightWeightTests = new HashSet<HeightWeightTest>();
            this.MedicineBills = new HashSet<MedicineBill>();
            this.TFTs = new HashSet<TFT>();
            this.Prescriptions = new HashSet<Prescription>();
            this.SugarTests = new HashSet<SugarTest>();
        }
    
        public int Id { get; set; }

        [ForeignKey("User")]
        public string UserId { get; set; }
        public virtual NazeeranHospitalUser User { get; set; }

        public virtual ICollection<Appointment> Appointments { get; set; }

        public virtual ICollection<BloodCP> BloodCPs { get; set; }

        public virtual ICollection<BloodPressueTest> BloodPressueTests { get; set; }

        public virtual ICollection<Calcium> Calciums { get; set; }

        public virtual ICollection<CrossMatch> CrossMatches { get; set; }

        public virtual ICollection<HeartRateTest> HeartRateTests { get; set; }

        public virtual ICollection<HeightWeightTest> HeightWeightTests { get; set; }

        public virtual ICollection<MedicineBill> MedicineBills { get; set; }

        public virtual ICollection<TFT> TFTs { get; set; }

        public virtual ICollection<Prescription> Prescriptions { get; set; }

        public virtual ICollection<SugarTest> SugarTests { get; set; }
    }
}
