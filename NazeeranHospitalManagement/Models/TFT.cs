using System.ComponentModel.DataAnnotations.Schema;

namespace NazeeranHospitalManagement.Models
{
    public partial class TFT
    {
        public int Id { get; set; }
        public double TotalT3 { get; set; }
        public string TotalT3Unit { get; set; }
        public double TotalT4 { get; set; }
        public string TotalT4Unit { get; set; }
        public double TSH { get; set; }
        public string TSHUnit { get; set; }


        [ForeignKey("Prescription")]
        public int PrescriptionId { get; set; }

        public virtual Prescription Prescription { get; set; }
    }
}
