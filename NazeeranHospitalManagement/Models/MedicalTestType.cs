namespace NazeeranHospitalManagement.Models
{
    using System.Collections.Generic;

    public partial class MedicalTestType
    {
        public MedicalTestType()
        {
            this.MedicalTests = new HashSet<MedicalTest>();
        }
    
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    
        public virtual ICollection<MedicalTest> MedicalTests { get; set; }
    }
}
