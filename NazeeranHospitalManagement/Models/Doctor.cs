using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace NazeeranHospitalManagement.Models
{
    public partial class Doctor
    {
        public Doctor()
        {
            this.Appointments = new HashSet<Appointment>();
            this.Prescriptions = new HashSet<Prescription>();
        }
    
        public int Id { get; set; }
        public int AverageTime { get; set; }
    
        [ForeignKey("Department")]
        public int DepartmentId { get; set; }
        public virtual Department Department { get; set; }

        [ForeignKey("User")]
        public string UserId { get; set; }
        public virtual NazeeranHospitalUser User { get; set; }

        public virtual ICollection<Prescription> Prescriptions { get; set; }
        public virtual ICollection<Appointment> Appointments { get; set; }
        public virtual ICollection<DoctorSchedule> DoctorSchedule { set; get; }
    }
}
