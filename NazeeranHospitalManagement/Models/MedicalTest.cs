using NazeeranHospitalManagement.ViewModels.Enums;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace NazeeranHospitalManagement.Models
{
    public partial class MedicalTest
    {
        public int Id { get; set; }
        public MedicalTestStatus Status { get; set; }

        public DateTime PrescribedOn { get; set; }

        [ForeignKey("MedicalTestType")]
        public int MedicalTestTypeId { get; set; }
        public virtual MedicalTestType MedicalTestType { get; set; }

        [ForeignKey("Prescription")]
        public int PrescriptionId { get; set; }
        public virtual Prescription Prescription { get; set; }
    }
}
