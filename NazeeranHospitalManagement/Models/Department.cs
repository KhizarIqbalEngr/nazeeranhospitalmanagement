using System.Collections.Generic;

namespace NazeeranHospitalManagement.Models
{
    public partial class Department
    {
        public Department()
        {
            this.Doctors = new HashSet<Doctor>();
        }
    
        public int Id { get; set; }
        public string Name { get; set; }
        public string Details { get; set; }
        public virtual ICollection<Doctor> Doctors { get; set; }
    }
}
