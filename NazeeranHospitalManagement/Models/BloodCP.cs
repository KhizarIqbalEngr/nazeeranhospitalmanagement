using NazeeranHospitalManagement.ViewModels.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace NazeeranHospitalManagement.Models
{
    public partial class BloodCP
    {
        public int Id { get; set; }
        public double Glucose2HRSABF { get; set; }
        public MedicalTestStatus Status { get; set; }

        [ForeignKey("Prescription")]
        public int PrescriptionId { get; set; }
        public virtual Prescription Prescription { get; set; }
    }
}
