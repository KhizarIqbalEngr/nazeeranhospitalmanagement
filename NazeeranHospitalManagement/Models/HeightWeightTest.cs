using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace NazeeranHospitalManagement.Models
{
    public partial class HeightWeightTest
    {
        public int Id { get; set; }
        public Nullable<double> HeightValue { get; set; }
        public Nullable<double> WeightValue { get; set; }


        [ForeignKey("Patient")]
        public int PatientId { get; set; }
        public virtual Patient Patient { get; set; }
    }
}
