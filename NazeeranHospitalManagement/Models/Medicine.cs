using System;
using System.Collections.Generic;

namespace NazeeranHospitalManagement.Models
{
    public partial class Medicine
    {
        public Medicine()
        {
            this.PrescribedMedicines = new HashSet<PrescribedMedicine>();
        }
    
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Potency { get; set; }
        public string ManufacturerName { get; set; }
        public DateTime ManufactuerDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public double PricePerUnit { get; set; }
        public int Quantity { get; set; }
        public string ChemicalComposition { get; set; }
        public string BatchNo { get; set; }
        public Nullable<int> QuantityThreshold { get; set; }
        public Nullable<DateTime> ExpiryThreshold { get; set; }
    
        public virtual ICollection<PrescribedMedicine> PrescribedMedicines { get; set; }
    }
}
