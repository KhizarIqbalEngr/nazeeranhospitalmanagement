namespace NazeeranHospitalManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedPatientsDetailsAndMovedItToUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Patients", "UserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Patients", "UserId");
            AddForeignKey("dbo.Patients", "UserId", "dbo.User", "Id");
            DropColumn("dbo.Patients", "Name");
            DropColumn("dbo.Patients", "Cnic");
            DropColumn("dbo.Patients", "Gender");
            DropColumn("dbo.Patients", "Address");
            DropColumn("dbo.Patients", "Email");
            DropColumn("dbo.Patients", "ContactNo");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Patients", "ContactNo", c => c.String());
            AddColumn("dbo.Patients", "Email", c => c.String());
            AddColumn("dbo.Patients", "Address", c => c.String());
            AddColumn("dbo.Patients", "Gender", c => c.Int(nullable: false));
            AddColumn("dbo.Patients", "Cnic", c => c.String());
            AddColumn("dbo.Patients", "Name", c => c.String());
            DropForeignKey("dbo.Patients", "UserId", "dbo.User");
            DropIndex("dbo.Patients", new[] { "UserId" });
            DropColumn("dbo.Patients", "UserId");
        }
    }
}
