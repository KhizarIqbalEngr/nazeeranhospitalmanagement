namespace NazeeranHospitalManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedUserIdAndDoctorInDoctorScheduleAndDoctorScheduleToDoctor : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.DoctorSchedules", "DoctorId", "dbo.User");
            DropIndex("dbo.DoctorSchedules", new[] { "DoctorId" });
            AddColumn("dbo.DoctorSchedules", "UserId", c => c.String(maxLength: 128));
            AlterColumn("dbo.DoctorSchedules", "DoctorId", c => c.Int(nullable: false));
            CreateIndex("dbo.DoctorSchedules", "UserId");
            CreateIndex("dbo.DoctorSchedules", "DoctorId");
            AddForeignKey("dbo.DoctorSchedules", "DoctorId", "dbo.Doctors", "Id", cascadeDelete: true);
            AddForeignKey("dbo.DoctorSchedules", "UserId", "dbo.User", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DoctorSchedules", "UserId", "dbo.User");
            DropForeignKey("dbo.DoctorSchedules", "DoctorId", "dbo.Doctors");
            DropIndex("dbo.DoctorSchedules", new[] { "DoctorId" });
            DropIndex("dbo.DoctorSchedules", new[] { "UserId" });
            AlterColumn("dbo.DoctorSchedules", "DoctorId", c => c.String(maxLength: 128));
            DropColumn("dbo.DoctorSchedules", "UserId");
            CreateIndex("dbo.DoctorSchedules", "DoctorId");
            AddForeignKey("dbo.DoctorSchedules", "DoctorId", "dbo.User", "Id");
        }
    }
}
