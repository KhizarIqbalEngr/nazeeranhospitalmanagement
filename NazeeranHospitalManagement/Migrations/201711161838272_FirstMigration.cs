namespace NazeeranHospitalManagement.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class FirstMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Appointments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AppointmentNo = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Time = c.Time(nullable: false, precision: 7),
                        State = c.String(),
                        DoctorId = c.Int(nullable: false),
                        PatientId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Doctors", t => t.DoctorId, cascadeDelete: true)
                .ForeignKey("dbo.Patients", t => t.PatientId, cascadeDelete: true)
                .Index(t => t.DoctorId)
                .Index(t => t.PatientId);
            
            CreateTable(
                "dbo.Doctors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AverageTime = c.Int(nullable: false),
                        DepartmentId = c.Int(nullable: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Departments", t => t.DepartmentId, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.UserId)
                .Index(t => t.DepartmentId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Details = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Prescriptions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        DoctorId = c.Int(nullable: false),
                        PatientId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Doctors", t => t.DoctorId, cascadeDelete: true)
                .ForeignKey("dbo.Patients", t => t.PatientId, cascadeDelete: true)
                .Index(t => t.DoctorId)
                .Index(t => t.PatientId);
            
            CreateTable(
                "dbo.BloodCPs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Glucose2HRSABF = c.Double(nullable: false),
                        Status = c.String(),
                        PrescriptionId = c.Int(nullable: false),
                        Patient_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Prescriptions", t => t.PrescriptionId, cascadeDelete: true)
                .ForeignKey("dbo.Patients", t => t.Patient_Id)
                .Index(t => t.PrescriptionId)
                .Index(t => t.Patient_Id);
            
            CreateTable(
                "dbo.Calciums",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        calcium1 = c.Double(nullable: false),
                        calciumUnit = c.String(),
                        PrescriptionId = c.Int(nullable: false),
                        Patient_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Prescriptions", t => t.PrescriptionId, cascadeDelete: true)
                .ForeignKey("dbo.Patients", t => t.Patient_Id)
                .Index(t => t.PrescriptionId)
                .Index(t => t.Patient_Id);
            
            CreateTable(
                "dbo.CrossMatches",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MRNOfDonor = c.Int(nullable: false),
                        NameOfDonor = c.String(),
                        AgeOfDonor = c.Int(nullable: false),
                        RelationOfDonorWithPatient = c.String(),
                        BloodBagNO = c.String(),
                        PatientName = c.String(),
                        PatientBloodGroup = c.String(),
                        CrossMatchValue = c.String(),
                        Prescription_Id = c.Int(),
                        Patient_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Prescriptions", t => t.Prescription_Id)
                .ForeignKey("dbo.Patients", t => t.Patient_Id)
                .Index(t => t.Prescription_Id)
                .Index(t => t.Patient_Id);
            
            CreateTable(
                "dbo.MedicalTests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Status = c.String(),
                        PrescribedOn = c.DateTime(nullable: false),
                        MedicalTestTypeId = c.Int(nullable: false),
                        PrescriptionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MedicalTestTypes", t => t.MedicalTestTypeId, cascadeDelete: true)
                .ForeignKey("dbo.Prescriptions", t => t.PrescriptionId, cascadeDelete: true)
                .Index(t => t.MedicalTestTypeId)
                .Index(t => t.PrescriptionId);
            
            CreateTable(
                "dbo.MedicalTestTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Patients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Cnic = c.String(),
                        Gender = c.Int(nullable: false),
                        Address = c.String(),
                        Email = c.String(),
                        ContactNo = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.BloodPressueTests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LowValue = c.Int(),
                        HighValue = c.Int(),
                        PrescriptionId = c.Int(nullable: false),
                        Patient_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Prescriptions", t => t.PrescriptionId, cascadeDelete: true)
                .ForeignKey("dbo.Patients", t => t.Patient_Id)
                .Index(t => t.PrescriptionId)
                .Index(t => t.Patient_Id);
            
            CreateTable(
                "dbo.HeartRateTests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        HeartRateValue = c.Int(nullable: false),
                        PrescriptionId = c.Int(nullable: false),
                        Patient_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Prescriptions", t => t.PrescriptionId, cascadeDelete: true)
                .ForeignKey("dbo.Patients", t => t.Patient_Id)
                .Index(t => t.PrescriptionId)
                .Index(t => t.Patient_Id);
            
            CreateTable(
                "dbo.HeightWeightTests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        HeightValue = c.Double(),
                        WeightValue = c.Double(),
                        PrescriptionId = c.Int(nullable: false),
                        Patient_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Prescriptions", t => t.PrescriptionId, cascadeDelete: true)
                .ForeignKey("dbo.Patients", t => t.Patient_Id)
                .Index(t => t.PrescriptionId)
                .Index(t => t.Patient_Id);
            
            CreateTable(
                "dbo.MedicineBills",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Amount = c.Double(nullable: false),
                        BillTime = c.DateTime(nullable: false),
                        PatientId = c.Int(nullable: false),
                        PharmacistId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Patients", t => t.PatientId, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.PharmacistId)
                .Index(t => t.PatientId)
                .Index(t => t.PharmacistId);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false),
                        Gender = c.Int(nullable: false),
                        Cnic = c.String(nullable: false, maxLength: 15),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.UserClaim",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserLogin",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserRole",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.Role", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.SugarTests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BeforeMealValue = c.Int(),
                        FastingValue = c.Int(),
                        PatientId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Patients", t => t.PatientId, cascadeDelete: true)
                .Index(t => t.PatientId);
            
            CreateTable(
                "dbo.TFTs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TotalT3 = c.Double(nullable: false),
                        TotalT3Unit = c.String(),
                        TotalT4 = c.Double(nullable: false),
                        TotalT4Unit = c.String(),
                        TSH = c.Double(nullable: false),
                        TSHUnit = c.String(),
                        PrescriptionId = c.Int(nullable: false),
                        Patient_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Prescriptions", t => t.PrescriptionId, cascadeDelete: true)
                .ForeignKey("dbo.Patients", t => t.Patient_Id)
                .Index(t => t.PrescriptionId)
                .Index(t => t.Patient_Id);
            
            CreateTable(
                "dbo.PrescribedMedicines",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Quantity = c.Int(nullable: false),
                        Time = c.String(),
                        NoOfDays = c.Int(nullable: false),
                        PrescribedOn = c.DateTime(nullable: false),
                        MedicineId = c.Int(nullable: false),
                        PrescriptionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Medicines", t => t.MedicineId, cascadeDelete: true)
                .ForeignKey("dbo.Prescriptions", t => t.PrescriptionId, cascadeDelete: true)
                .Index(t => t.MedicineId)
                .Index(t => t.PrescriptionId);
            
            CreateTable(
                "dbo.Medicines",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Type = c.String(),
                        Potency = c.String(),
                        ManufacturerName = c.String(),
                        ManufactuerDate = c.DateTime(nullable: false),
                        ExpiryDate = c.DateTime(nullable: false),
                        PricePerUnit = c.Double(nullable: false),
                        Quantity = c.Int(nullable: false),
                        ChemicalComposition = c.String(),
                        BatchNo = c.String(),
                        QuantityThreshold = c.Int(),
                        ExpiryThreshold = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DoctorSchedules",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ArrivalTime = c.Time(nullable: false, precision: 7),
                        LeaveTime = c.Time(nullable: false, precision: 7),
                        Day = c.Int(nullable: false),
                        DoctorId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.DoctorId)
                .Index(t => t.DoctorId);
            
            CreateTable(
                "dbo.Role",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserRole", "RoleId", "dbo.Role");
            DropForeignKey("dbo.DoctorSchedules", "DoctorId", "dbo.User");
            DropForeignKey("dbo.Appointments", "PatientId", "dbo.Patients");
            DropForeignKey("dbo.Appointments", "DoctorId", "dbo.Doctors");
            DropForeignKey("dbo.Doctors", "UserId", "dbo.User");
            DropForeignKey("dbo.PrescribedMedicines", "PrescriptionId", "dbo.Prescriptions");
            DropForeignKey("dbo.PrescribedMedicines", "MedicineId", "dbo.Medicines");
            DropForeignKey("dbo.Prescriptions", "PatientId", "dbo.Patients");
            DropForeignKey("dbo.TFTs", "Patient_Id", "dbo.Patients");
            DropForeignKey("dbo.TFTs", "PrescriptionId", "dbo.Prescriptions");
            DropForeignKey("dbo.SugarTests", "PatientId", "dbo.Patients");
            DropForeignKey("dbo.MedicineBills", "PharmacistId", "dbo.User");
            DropForeignKey("dbo.UserRole", "UserId", "dbo.User");
            DropForeignKey("dbo.UserLogin", "UserId", "dbo.User");
            DropForeignKey("dbo.UserClaim", "UserId", "dbo.User");
            DropForeignKey("dbo.MedicineBills", "PatientId", "dbo.Patients");
            DropForeignKey("dbo.HeightWeightTests", "Patient_Id", "dbo.Patients");
            DropForeignKey("dbo.HeightWeightTests", "PrescriptionId", "dbo.Prescriptions");
            DropForeignKey("dbo.HeartRateTests", "Patient_Id", "dbo.Patients");
            DropForeignKey("dbo.HeartRateTests", "PrescriptionId", "dbo.Prescriptions");
            DropForeignKey("dbo.CrossMatches", "Patient_Id", "dbo.Patients");
            DropForeignKey("dbo.Calciums", "Patient_Id", "dbo.Patients");
            DropForeignKey("dbo.BloodPressueTests", "Patient_Id", "dbo.Patients");
            DropForeignKey("dbo.BloodPressueTests", "PrescriptionId", "dbo.Prescriptions");
            DropForeignKey("dbo.BloodCPs", "Patient_Id", "dbo.Patients");
            DropForeignKey("dbo.MedicalTests", "PrescriptionId", "dbo.Prescriptions");
            DropForeignKey("dbo.MedicalTests", "MedicalTestTypeId", "dbo.MedicalTestTypes");
            DropForeignKey("dbo.Prescriptions", "DoctorId", "dbo.Doctors");
            DropForeignKey("dbo.CrossMatches", "Prescription_Id", "dbo.Prescriptions");
            DropForeignKey("dbo.Calciums", "PrescriptionId", "dbo.Prescriptions");
            DropForeignKey("dbo.BloodCPs", "PrescriptionId", "dbo.Prescriptions");
            DropForeignKey("dbo.Doctors", "DepartmentId", "dbo.Departments");
            DropIndex("dbo.Role", "RoleNameIndex");
            DropIndex("dbo.DoctorSchedules", new[] { "DoctorId" });
            DropIndex("dbo.PrescribedMedicines", new[] { "PrescriptionId" });
            DropIndex("dbo.PrescribedMedicines", new[] { "MedicineId" });
            DropIndex("dbo.TFTs", new[] { "Patient_Id" });
            DropIndex("dbo.TFTs", new[] { "PrescriptionId" });
            DropIndex("dbo.SugarTests", new[] { "PatientId" });
            DropIndex("dbo.UserRole", new[] { "RoleId" });
            DropIndex("dbo.UserRole", new[] { "UserId" });
            DropIndex("dbo.UserLogin", new[] { "UserId" });
            DropIndex("dbo.UserClaim", new[] { "UserId" });
            DropIndex("dbo.User", "UserNameIndex");
            DropIndex("dbo.MedicineBills", new[] { "PharmacistId" });
            DropIndex("dbo.MedicineBills", new[] { "PatientId" });
            DropIndex("dbo.HeightWeightTests", new[] { "Patient_Id" });
            DropIndex("dbo.HeightWeightTests", new[] { "PrescriptionId" });
            DropIndex("dbo.HeartRateTests", new[] { "Patient_Id" });
            DropIndex("dbo.HeartRateTests", new[] { "PrescriptionId" });
            DropIndex("dbo.BloodPressueTests", new[] { "Patient_Id" });
            DropIndex("dbo.BloodPressueTests", new[] { "PrescriptionId" });
            DropIndex("dbo.MedicalTests", new[] { "PrescriptionId" });
            DropIndex("dbo.MedicalTests", new[] { "MedicalTestTypeId" });
            DropIndex("dbo.CrossMatches", new[] { "Patient_Id" });
            DropIndex("dbo.CrossMatches", new[] { "Prescription_Id" });
            DropIndex("dbo.Calciums", new[] { "Patient_Id" });
            DropIndex("dbo.Calciums", new[] { "PrescriptionId" });
            DropIndex("dbo.BloodCPs", new[] { "Patient_Id" });
            DropIndex("dbo.BloodCPs", new[] { "PrescriptionId" });
            DropIndex("dbo.Prescriptions", new[] { "PatientId" });
            DropIndex("dbo.Prescriptions", new[] { "DoctorId" });
            DropIndex("dbo.Doctors", new[] { "UserId" });
            DropIndex("dbo.Doctors", new[] { "DepartmentId" });
            DropIndex("dbo.Appointments", new[] { "PatientId" });
            DropIndex("dbo.Appointments", new[] { "DoctorId" });
            DropTable("dbo.Role");
            DropTable("dbo.DoctorSchedules");
            DropTable("dbo.Medicines");
            DropTable("dbo.PrescribedMedicines");
            DropTable("dbo.TFTs");
            DropTable("dbo.SugarTests");
            DropTable("dbo.UserRole");
            DropTable("dbo.UserLogin");
            DropTable("dbo.UserClaim");
            DropTable("dbo.User");
            DropTable("dbo.MedicineBills");
            DropTable("dbo.HeightWeightTests");
            DropTable("dbo.HeartRateTests");
            DropTable("dbo.BloodPressueTests");
            DropTable("dbo.Patients");
            DropTable("dbo.MedicalTestTypes");
            DropTable("dbo.MedicalTests");
            DropTable("dbo.CrossMatches");
            DropTable("dbo.Calciums");
            DropTable("dbo.BloodCPs");
            DropTable("dbo.Prescriptions");
            DropTable("dbo.Departments");
            DropTable("dbo.Doctors");
            DropTable("dbo.Appointments");
        }
    }
}
