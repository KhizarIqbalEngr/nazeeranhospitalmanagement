namespace NazeeranHospitalManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedQueryMessageToTheDatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.QueryMessages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Email = c.String(),
                        PhoneNumber = c.String(),
                        Question = c.String(),
                        Answer = c.String(),
                        Date = c.DateTime(nullable: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.UserId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.QueryMessages", "UserId", "dbo.User");
            DropIndex("dbo.QueryMessages", new[] { "UserId" });
            DropTable("dbo.QueryMessages");
        }
    }
}
