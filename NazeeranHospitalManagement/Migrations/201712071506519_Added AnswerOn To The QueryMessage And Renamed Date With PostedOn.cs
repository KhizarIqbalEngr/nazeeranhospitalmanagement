namespace NazeeranHospitalManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedAnswerOnToTheQueryMessageAndRenamedDateWithPostedOn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.QueryMessages", "PostedOn", c => c.DateTime(nullable: false));
            AddColumn("dbo.QueryMessages", "AnswerOn", c => c.DateTime(nullable: false));
            DropColumn("dbo.QueryMessages", "Date");
        }
        
        public override void Down()
        {
            AddColumn("dbo.QueryMessages", "Date", c => c.DateTime(nullable: false));
            DropColumn("dbo.QueryMessages", "AnswerOn");
            DropColumn("dbo.QueryMessages", "PostedOn");
        }
    }
}
