namespace NazeeranHospitalManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReplacedPatientWithPrescriptionInAllMedicalTestsAndUsedEnumsInsteadOfString : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BloodPressueTests", "PrescriptionId", "dbo.Prescriptions");
            DropForeignKey("dbo.HeartRateTests", "PrescriptionId", "dbo.Prescriptions");
            DropForeignKey("dbo.HeightWeightTests", "PrescriptionId", "dbo.Prescriptions");
            DropForeignKey("dbo.CrossMatches", "Prescription_Id", "dbo.Prescriptions");
            DropForeignKey("dbo.BloodPressueTests", "Patient_Id", "dbo.Patients");
            DropForeignKey("dbo.HeartRateTests", "Patient_Id", "dbo.Patients");
            DropForeignKey("dbo.HeightWeightTests", "Patient_Id", "dbo.Patients");
            DropIndex("dbo.CrossMatches", new[] { "Prescription_Id" });
            DropIndex("dbo.BloodPressueTests", new[] { "PrescriptionId" });
            DropIndex("dbo.BloodPressueTests", new[] { "Patient_Id" });
            DropIndex("dbo.HeartRateTests", new[] { "PrescriptionId" });
            DropIndex("dbo.HeartRateTests", new[] { "Patient_Id" });
            DropIndex("dbo.HeightWeightTests", new[] { "PrescriptionId" });
            DropIndex("dbo.HeightWeightTests", new[] { "Patient_Id" });
            RenameColumn(table: "dbo.CrossMatches", name: "Prescription_Id", newName: "PrescriptionId");
            RenameColumn(table: "dbo.BloodPressueTests", name: "Patient_Id", newName: "PatientId");
            RenameColumn(table: "dbo.HeartRateTests", name: "Patient_Id", newName: "PatientId");
            RenameColumn(table: "dbo.HeightWeightTests", name: "Patient_Id", newName: "PatientId");
            AddColumn("dbo.Appointments", "Status", c => c.Int(nullable: false));
            AlterColumn("dbo.BloodCPs", "Status", c => c.Int(nullable: false));
            AlterColumn("dbo.CrossMatches", "PrescriptionId", c => c.Int(nullable: false));
            AlterColumn("dbo.MedicalTests", "Status", c => c.Int(nullable: false));
            AlterColumn("dbo.BloodPressueTests", "PatientId", c => c.Int(nullable: false));
            AlterColumn("dbo.HeartRateTests", "PatientId", c => c.Int(nullable: false));
            AlterColumn("dbo.HeightWeightTests", "PatientId", c => c.Int(nullable: false));
            CreateIndex("dbo.CrossMatches", "PrescriptionId");
            CreateIndex("dbo.BloodPressueTests", "PatientId");
            CreateIndex("dbo.HeartRateTests", "PatientId");
            CreateIndex("dbo.HeightWeightTests", "PatientId");
            AddForeignKey("dbo.CrossMatches", "PrescriptionId", "dbo.Prescriptions", "Id", cascadeDelete: true);
            AddForeignKey("dbo.BloodPressueTests", "PatientId", "dbo.Patients", "Id", cascadeDelete: true);
            AddForeignKey("dbo.HeartRateTests", "PatientId", "dbo.Patients", "Id", cascadeDelete: true);
            AddForeignKey("dbo.HeightWeightTests", "PatientId", "dbo.Patients", "Id", cascadeDelete: true);
            DropColumn("dbo.Appointments", "State");
            DropColumn("dbo.CrossMatches", "PatientName");
            DropColumn("dbo.BloodPressueTests", "PrescriptionId");
            DropColumn("dbo.HeartRateTests", "PrescriptionId");
            DropColumn("dbo.HeightWeightTests", "PrescriptionId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.HeightWeightTests", "PrescriptionId", c => c.Int(nullable: false));
            AddColumn("dbo.HeartRateTests", "PrescriptionId", c => c.Int(nullable: false));
            AddColumn("dbo.BloodPressueTests", "PrescriptionId", c => c.Int(nullable: false));
            AddColumn("dbo.CrossMatches", "PatientName", c => c.String());
            AddColumn("dbo.Appointments", "State", c => c.String());
            DropForeignKey("dbo.HeightWeightTests", "PatientId", "dbo.Patients");
            DropForeignKey("dbo.HeartRateTests", "PatientId", "dbo.Patients");
            DropForeignKey("dbo.BloodPressueTests", "PatientId", "dbo.Patients");
            DropForeignKey("dbo.CrossMatches", "PrescriptionId", "dbo.Prescriptions");
            DropIndex("dbo.HeightWeightTests", new[] { "PatientId" });
            DropIndex("dbo.HeartRateTests", new[] { "PatientId" });
            DropIndex("dbo.BloodPressueTests", new[] { "PatientId" });
            DropIndex("dbo.CrossMatches", new[] { "PrescriptionId" });
            AlterColumn("dbo.HeightWeightTests", "PatientId", c => c.Int());
            AlterColumn("dbo.HeartRateTests", "PatientId", c => c.Int());
            AlterColumn("dbo.BloodPressueTests", "PatientId", c => c.Int());
            AlterColumn("dbo.MedicalTests", "Status", c => c.String());
            AlterColumn("dbo.CrossMatches", "PrescriptionId", c => c.Int());
            AlterColumn("dbo.BloodCPs", "Status", c => c.String());
            DropColumn("dbo.Appointments", "Status");
            RenameColumn(table: "dbo.HeightWeightTests", name: "PatientId", newName: "Patient_Id");
            RenameColumn(table: "dbo.HeartRateTests", name: "PatientId", newName: "Patient_Id");
            RenameColumn(table: "dbo.BloodPressueTests", name: "PatientId", newName: "Patient_Id");
            RenameColumn(table: "dbo.CrossMatches", name: "PrescriptionId", newName: "Prescription_Id");
            CreateIndex("dbo.HeightWeightTests", "Patient_Id");
            CreateIndex("dbo.HeightWeightTests", "PrescriptionId");
            CreateIndex("dbo.HeartRateTests", "Patient_Id");
            CreateIndex("dbo.HeartRateTests", "PrescriptionId");
            CreateIndex("dbo.BloodPressueTests", "Patient_Id");
            CreateIndex("dbo.BloodPressueTests", "PrescriptionId");
            CreateIndex("dbo.CrossMatches", "Prescription_Id");
            AddForeignKey("dbo.HeightWeightTests", "Patient_Id", "dbo.Patients", "Id");
            AddForeignKey("dbo.HeartRateTests", "Patient_Id", "dbo.Patients", "Id");
            AddForeignKey("dbo.BloodPressueTests", "Patient_Id", "dbo.Patients", "Id");
            AddForeignKey("dbo.CrossMatches", "Prescription_Id", "dbo.Prescriptions", "Id");
            AddForeignKey("dbo.HeightWeightTests", "PrescriptionId", "dbo.Prescriptions", "Id", cascadeDelete: true);
            AddForeignKey("dbo.HeartRateTests", "PrescriptionId", "dbo.Prescriptions", "Id", cascadeDelete: true);
            AddForeignKey("dbo.BloodPressueTests", "PrescriptionId", "dbo.Prescriptions", "Id", cascadeDelete: true);
        }
    }
}
