namespace NazeeranHospitalManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MadeCnicUniqueColumnAndChangedDoctorAverageTimeTimeSpanToInt : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Doctors", "AverageTime", c => c.Int(nullable: false));
            CreateIndex("dbo.User", "Cnic", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.User", new[] { "Cnic" });
            AlterColumn("dbo.Doctors", "AverageTime", c => c.Time(nullable: false, precision: 7));
        }
    }
}
