using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using NazeeranHospitalManagement.Models;
using NazeeranHospitalManagement.ViewModels.Enums;
using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;

namespace NazeeranHospitalManagement.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<NazeeranHospitalDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<NazeeranHospitalDbContext>());
        }

        protected override void Seed(NazeeranHospitalDbContext context)
        {
            context.Roles.AddOrUpdate(role => role.Name,
                new IdentityRole { Id = ((int)UserRoles.Admin).ToString(), Name = UserRoles.Admin.ToString() },
                new IdentityRole { Id = ((int)UserRoles.Doctor).ToString(), Name = UserRoles.Doctor.ToString() },
                new IdentityRole { Id = ((int)UserRoles.HelpDesk).ToString(), Name = UserRoles.HelpDesk.ToString() },
                new IdentityRole { Id = ((int)UserRoles.LabAttendent).ToString(), Name = UserRoles.LabAttendent.ToString() },
                new IdentityRole { Id = ((int)UserRoles.Patient).ToString(), Name = UserRoles.Patient.ToString() },
                new IdentityRole { Id = ((int)UserRoles.Pharmacist).ToString(), Name = UserRoles.Pharmacist.ToString() });
            context.SaveChanges();

            var adminUser = new NazeeranHospitalUser
            {
                Name = "Admin",
                UserName = "Admin",
                Email = "admin@no.com",
                Cnic = "00000-0000000-0",
                PhoneNumber = "0333-0000000",
                Gender = Gender.MALE,
                PasswordHash = new PasswordHasher().HashPassword("Admin"),
                SecurityStamp = Guid.NewGuid().ToString()
            };
            context.Users.AddOrUpdate(user => user.UserName, adminUser);
            context.SaveChanges();

            var adminRole = new IdentityUserRole
            {
                RoleId = ((int)UserRoles.Admin).ToString(),
                UserId = adminUser.Id
            };
            context.Set<IdentityUserRole>().Add(adminRole);
            context.SaveChanges();
        }
    }
}
