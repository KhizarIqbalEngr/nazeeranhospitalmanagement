namespace NazeeranHospitalManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedInitialStructureToTheChatMessageTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ChatMessages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Message = c.String(),
                        IsSeen = c.Boolean(nullable: false),
                        Time = c.DateTime(nullable: false),
                        FromUserId = c.String(maxLength: 128),
                        ToUserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.FromUserId)
                .ForeignKey("dbo.User", t => t.ToUserId)
                .Index(t => t.FromUserId)
                .Index(t => t.ToUserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ChatMessages", "ToUserId", "dbo.User");
            DropForeignKey("dbo.ChatMessages", "FromUserId", "dbo.User");
            DropIndex("dbo.ChatMessages", new[] { "ToUserId" });
            DropIndex("dbo.ChatMessages", new[] { "FromUserId" });
            DropTable("dbo.ChatMessages");
        }
    }
}
