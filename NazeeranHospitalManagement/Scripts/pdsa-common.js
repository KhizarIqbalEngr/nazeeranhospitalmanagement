﻿$(function () {
    logOff();

    $('body').removeClass('notransition');
    //$(".datepic").datepicker();
    $(".datepic").each(function () {
        $(this).datepicker({ dateFormat: 'dd/mm/yy' });
    });
});

var $rapper = $(".pdsa-sn-wrapper,.body-content,header,.pdsa-sn-wrapper ul li a i,.ab a span");

$("#sidebarToggle").on("click", function () {
    $rapper.toggleClass("hide-sidebar");
    if ($rapper.hasClass("hide-sidebar")) {
        $(this).removeClass("glyphicon glyphicon-align-right");// css({ "class": "", "": "" });
        $(this).addClass("glyphicon glyphicon-align-left");
    }
    else {
        $(this).removeClass("glyphicon glyphicon-align-left");// css({ "class": "", "": "" });
        $(this).addClass("glyphicon glyphicon-align-right");
    }

    $(".sidebar-list-text").each(function (e) {
        $(this).css("display", "none");
    });
});

function logOff() {
    $("a#btnLogOff").on("click", function () {
        $(this).closest("form").submit();

        return false;
    });
}
