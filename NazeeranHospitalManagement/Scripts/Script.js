﻿/// <reference path="jquery-3.2.1.slim.js" />

window.onload = function () {
    logOff();
    maskInputs();
    setupDateTimes();
    showUserAlerts();

    if (window.location.href.match(/GetAppointment\/SearchSchedule/i) !== null) {
        Appointment_SearchSchedule();
    }
    else if (window.location.href.match(/GetAppointment\/GetSchedule/i) !== null) {
        Appointment_GetSchedule();
    }
    else if (window.location.href.match(/GetAppointment\/ViewSchedule/i) !== null) {
        Appointment_ViewSchedule();
    }
    else if (window.location.href.match(/GetAppointment\/SearchAppointment/i) !== null) {
        Appointment_SearchAppointment();
    }
    else if (window.location.href.match(/Account\/Login/i) !== null) {
        Signin_Sinup();
    }
    else if (window.location.href.match(/DoctorSchedule\/Create/i) !== null) {
        DoctorSchedule_Index();
    }
    else if (window.location.href.match(/Admin\/EditDoctor/i) !== null) {
        Admin_EditDoctor();
    }
    else if (window.location.href.match(/Admin\/ViewDoctor/i) !== null) {
        Admin_ViewDoctor();
    }
    else if (window.location.href.match(/Admin\/ViewAllDepartment/i) !== null) {
        Admin_ViewAllDepartment();
    }
    else if (window.location.href.match(/Admin\/ViewAll/i) !== null) {
        Admin_ViewAll();
    }
    else if (window.location.href.match(/Admin\/ManageDoctorSchedule/i) !== null) {
        Admin_ManageDoctorSchedule();
    }

    $('body').removeClass('notransition');
    if ($(".datepic").length > 0) $(".datepic").datetimepicker();
    $("input[type='datetime']").each(function () {
        $(this).datetimepicker({
            timepicker: false,
            format: 'd/m/Y',
            todayButton: true,
            // mask: true,
            startDate: new Date(),
            minDate: 0
        });
    });

    var $rapper = $(".pdsa-sn-wrapper,.body-content,header,.pdsa-sn-wrapper ul li a i,.ab a span");

    $("#sidebarToggle").on("click", function () {
        $rapper.toggleClass("hide-sidebar");
        if ($rapper.hasClass("hide-sidebar")) {
            $(this).removeClass("glyphicon glyphicon-align-right");// css({ "class": "", "": "" });
            $(this).addClass("glyphicon glyphicon-align-left");
        }
        else {
            $(this).removeClass("glyphicon glyphicon-align-left");// css({ "class": "", "": "" });
            $(this).addClass("glyphicon glyphicon-align-right");
        }

        $(".sidebar-list-text").each(function (e) {
            $(this).css("display", "none");
        });
    });
}

var Appointment_SearchSchedule = function () {
    OnChangeAppintmentSearchOption();

    $("div#CancelAppointment").on("click", "button[name$='btnProceed']", function () {
        swal({
            title: "Verifying!",
            text: "Please be patient while we getting information about the appointment!",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function (typedPassword) {
            console.log(typedPassword);
        });

        return false;
    });
}

var Appointment_GetSchedule = function () {
    OnChangeAppintmentSearchOption();
}

var Appointment_ViewSchedule = function () {
    $("[data-toggle='tooltip']").tooltip();

    $("table#tableDoctorSchedule").on("click", "button", function () {
        $("#patientDetailsModel").modal();

        var $tr = $(this).closest("tr");
        var $detailsTr = $tr.closest("table tbody").find("tr.hidden#doctorDetails");
        var $form = $("div#patientDetailsModel form#viewSlotsForm");

        var doctorId = $detailsTr.find("input[name$='DoctorId']").val();
        var dayName = $tr.find("input[name$='Day']").val();
        var slotNo = $(this).prev().val();
        var appointmentTime = $(this).prev().prev().val();

        $form.find("input[name$='DoctorId']").val(doctorId);
        $form.find("input[name$='Day']").val(dayName);
        $form.find("input[name$='Date']").val(appointmentTime);
        $form.find("input[name$='SlotNo']").val(slotNo);

        $("button[type='button']#btnProceed").on("click", function () {
            if ($form.valid()) {
                $form.submit();
            }
        });
    });

    // Show DateTimePicker when user click on the showDatePicker Button
    // $("form#findDoctorSchedule").off("click");
    $("form#findDoctorSchedule").on("click", "button.showDatePicker", function () {
        $(this).prev("input.date").focus();
        $(this).prev("input.date").datetimepicker("toggle");
    });

    function BookAppointment(tr) {
        var options = {
            url: $(tr).closest('table').attr("data-ppims-get-appointment-url"),
            method: "POST",
            dataType: "json",
            data: {
                DoctorName: tr.find("input[name$='DoctorName']").val(),
                DoctorId: tr.find("input[name$='DoctorId']").val(),
                ArrivalTime: tr.find("input[name$='ArrivalTime']").val(),
                LeaveTime: tr.find("input[name$='LeaveTime']").val(),
                Day: tr.find("input[name$='Day']").val(),
                __RequestVerificationToken: tr.closest("table").find("input[name$='__RequestVerificationToken']").val()
            },
            success: AppointmentSuccess,
            fail: AppointmentFail,
            error: AppointmentFail
        };

        // Gray out and disable the table before sending the request to the server for appointment...!!
        var table = $(tr).closest('table');
        table.attr("disabled", "disabled");

        $.ajax(options);

        function AppointmentSuccess(data) {
            var statusPanel = $("div#statusPanel");

            if (data == 1) {
                var statusPanel = $("div#statusPanel");
                statusPanel.removeClass("hidden");
                statusPanel.html("<p>Please Register Yourself first inorder to book appointment...!!<br/>" +
                    "<a href='http://localhost:63705/Account/LogIn'>Click Here</a> To Login...!!</p>");

                return;
            }

            var Id = data.Id;
            var PatientId = data.PatientId;
            var DoctorId = data.DoctorId;
            var Date = data.Date;
            var Time = data.Time;
            var AppointmentNo = data.AppointmentNo;

            statusPanel.html("<p>Your Appointment Is Booked. <a href='http://localhost:63705/GetAppointment/Appointment?appointmentId=" + data.Id + "'>Click Here</a> To View Details Of The Appointment...!!</p>");
            statusPanel.removeClass("hidden", 1000, "fadeIn");

            console.log(Id + PatientId + DoctorId + Date + Time + AppointmentNo);

            table.removeAttr("disabled");
        }

        function AppointmentFail(xhr, textStatus, exceptionThrown) {
            var statusPanel = $("div#statusPanel");

            // if status code is 401, then user is not registered...!!
            if (xhr.statusCode === 401 && xhr.responseText === "1") {
                statusPanel.removeClass("hidden");
                statusPanel.text("Please Register Yourself first inorder to book appointment...!!");
            }
            else {

            }

            table.removeAttr("disabled");
        }
    }

    function reposition() {
        var modal = $(this),
            dialog = modal.find('.modal-dialog');

        modal.css('display', 'block');

        // Dividing by two centers the modal exactly, but dividing by three
        // or four works better for larger screens.
        dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
    }

    // Reposition when a modal is shown
    $('.modal').on('show.bs.modal', reposition);

    // Reposition when the window is resized
    $(window).on('resize', function () {
        $('.modal:visible').each(reposition);
    });
}

var Appointment_SearchAppointment = function()
{
    $("button#btnCancelAppointment").on("click", function () {
        var $div = $(this).closest("div.min-padding-all");

        swal({
            title: "Are you sure?",
            text: "Are You Sure That You Want to Cancel this Appointment?",
            type: "question",
            showCancelButton: true,
            confirmButtonText: "Yes, Cancel It!",
            confirmButtonColor: "#ec6c62",
            allowOutsideClick: false,
            showLoaderOnConfirm: true,
            preConfirm: function (data) {
                return new Promise(function (resolve) {
                    var options = {
                        url: "CancelAppointment/",
                        type: "POST",
                        data: {
                            appointmentId: $div.find("input:hidden[name='AppointmentId']").val(),
                            __RequestVerificationToken: $div.find("input:hidden[name='__RequestVerificationToken']").val()
                        }
                    }

                    $.ajax(options)
                    .done(function (data) {
                        if (data["Success"]) {
                            swal("Canceled", "Your Appointment is successfully Cancelled!", "success");
                        } else if (data["AlreadyCancelled"]) {
                            swal("Already Cancelled", data["AlreadyCancelled"][0], "info");
                        } else if (data["Error"]) {
                            swal("Already Cancelled", data["Error"][0], "error");
                        } else if (data["NotFound"]) {
                            swal("Not Found.", data["NotFound"][0], "info");
                        }
                    })
                    .error(function (data) {
                        swal("Connection Erorr!", "Sorry! An Error Occured While Connecting To The Server.", "error");
                    });
                });
            }
        })
    });

    $("button#btnRescheduleAppointment").on("click", function () {
        var $form = $(this).closest("form");

        swal({
            title: "Are you sure?",
            text: "Are You Sure That You Want to Reschedule this Appointment?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, Go Ahead!",
            confirmButtonColor: "#ec6c62",
            allowOutsideClick: false,
        })
        .then(function (result) {
            if (result.value) $form.submit();
        });

        return false;
    });
}

var Signin_Sinup = function () {
    $('div#Login .form').find("input:not(hidden)").each(function () {
        if ($(this).val() !== '') {
            $(this).prev("label").addClass('active highlight');
        }
    });

    $('.form').find('input, textarea').on('keyup blur focus', function (e) {
        var $this = $(this),
            label = $this.prev('label');

        if (e.type === 'keyup') {
            if ($this.val() === '') {
                label.removeClass('active highlight');
            } else {
                label.addClass('active highlight');
            }
        }
        else if (e.type === 'blur') {
            if ($this.val() === '') {
                label.removeClass('active highlight');
            } else {
                label.removeClass('highlight');
            }
        }
        else if (e.type === 'focus') {
            if ($this.val() === '') {
                label.removeClass('highlight');
            }
            else if ($this.val() !== '') {
                label.addClass('highlight');
            }
        }

    });

    $('.tab a').on('click', function (e) {

        e.preventDefault();

        $(this).parent().addClass('active');
        $(this).parent().siblings().removeClass('active');

        target = $(this).attr('href');

        $('.tab-content > div').not(target).hide();

        $(target).fadeIn(600);

    });
}

var DoctorSchedule_Index = function () {
    $.each($(".time-picker"), function () {
        $(this).timepicker({
            timeFormat: 'hh:mm:ss p',
            interval: 15,
            defaultTime: "now",
            scrollbar: true
        });
    });
}

var Admin_EditDoctor = function () {
    showUserAlerts();
}

var Admin_ViewDoctor = function()
{
    $("table#doctorsList tbody").on("click", "a.btnDeleteDoctor", function () {
        var $tr = $(this).closest("tr");
        var doctorId = $tr.attr("data-doctorId");

        swal({
            title: "Are you sure?",
            text: "Do you really want to remove the doctor?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!"
        })
        .then(function (result) {
            if (!result.value) return;
            var options = {
                url: "DeleteDoctor",
                type: "POST",
                data: {
                    doctorId: doctorId,
                    __RequestVerificationToken: $tr.closest("table").find("input:hidden[name$='__RequestVerificationToken']").val()
                }
            };

            $.ajax(options)
                .done(function (data) {
                    if (data === "Deleted") {
                        swal({ title: "Deleted!", text: "Doctor Deleted Successfully...!!", type: "success", confirmButtonText: "O.K.," });
                        $tr.remove();
                    }
                    else if (data === "HasAppointments")
                        swal("Oops!", "This doctor has appointments, so can't delete the doctor...!!", "warning");
                    else if (data === "Exception")
                        swal("Oops!", "An Erorr Occred While Deleting The Doctor...!!", "warning");
                })
                .error(function () {
                    swal("Oops!", "We couldn't connect to the server!", "error");
                });
        });

        return false;
    });
}

var Admin_ViewAll = function ()
{
    $("table#tableDetails").on("click", "#deletePerson", function (e) {
        e.preventDefault();
        event.preventDefault();

        var $tr = $(this).closest("tr");
        var personId = $tr.attr("data-ppims-personId");

        swal({
            title: "Are you sure?",
            text: "Do You Really Want To Delete..!!!",
            type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!", cancelButtonText: "No, cancel plx!", closeOnConfirm: false, closeOnCancel: true
        })
        .then(function (result) {
            if (!result.value) return false;
            var options = {
                url: "DeletePerson",
                type: "POST",
                data: {
                    personId: personId,
                    __RequestVerificationToken: $tr.closest("table").find("input:hidden[name$='__RequestVerificationToken']").val()
                }
            };

            $.ajax(options)
                .done(function (data) {
                    if (data === "Deleted") {
                        swal({ title: "Deleted!", text: "Deleted Successfully...!!", type: "success", confirmButtonText: "O.K.," });
                        $tr.remove();
                    }
                    else if (data === "Exception")
                        swal("Oops!", "An Erorr Occred While Deleting The Record...!!", "warning");
                    else if (data === "NotFound")
                        swal("Oops!", "Sorry, Enable to find the record...!!", "warning");
                    else if (data === "InvalidId")
                        swal("Oops!", "Sorry, Id Is Not Valid...!!", "warning");
                })
                .error(function () {
                    swal("Oops!", "We couldn't connect to the server!", "error");
                });
        });

        return false;
    });
}

var Admin_ViewAllDepartment = function ()
{
    $("#departmentTable").on("click", "a.DeleteDepartmentbtn", function () {
        var $tr = $(this).closest("tr");
        var departmentId = $tr.attr("data-department-id");

        swal({
            title: "Are you sure?",
            text: "Do you really want to remove the department?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!"
        })
        .then(function (result) {
            if (!result.value) return;
            var options = {
                url: "DeleteDepartment",
                type: "POST",
                data: {
                    departmentId: departmentId,
                    __RequestVerificationToken: $tr.closest("table").find("input:hidden[name$='__RequestVerificationToken']").val()
                }
            };

            $.ajax(options)
                .done(function (data) {
                    if (data === "Deleted") {
                        swal({ title: "Deleted!", text: "Department Deleted Successfully...!!", type: "success", confirmButtonText: "O.K.," });
                        $tr.remove();
                    }
                    else if (data === "DoctorsExists")
                        swal("Oops!", "Can't delete the department as some doctors are accosiated with this department...!!", "warning");
                    else if (data === "Exception")
                        swal("Oops!", "An Erorr Occred While Deleting The Department...!!", "warning");
                    else if (data === "NotFound")
                        swal("Oops!", "Sorry, No Department Is Found...!!", "warning");
                    else if (data === "InvalidId")
                        swal("Oops!", "Sorry, Department Id Is Not Valid...!!", "warning");
                })
                .error(function () {
                    swal("Oops!", "We couldn't connect to the server!", "error");
                });
        });

        return false;
    });
};

var Admin_ManageDoctorSchedule = function ()
{
    var $table = $("#doctorScheduleList");
    $("#saveSchedule").on("click", function () {
        var $model = $(this).closest("div#AddModal");
        var doctorId = $model.attr("data-ppims-doctorId");

        swal({
            title: "Are you sure?",
            text: "Do you want to save schedule?",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-sucess",
            confirmButtonText: "Yes, save it!"
        })
        .then(function (result) {
            if (!result.value) return;

            var options = {
                url: "AddDoctorSchedule",
                type: "POST",
                data: {
                    doctorId: doctorId,
                    Day: $model.find("select[name$='Day']").val(),
                    ArrivalTime: $model.find("input[name$='ArrivalTime']").val(),
                    LeaveTime: $model.find("input[name$='LeaveTime']").val(),
                    AverageTime: $model.find("input[name$='AverageTime']").val(),
                    __RequestVerificationToken: $model.find("input:hidden[name$='__RequestVerificationToken']").val()
                }
            };

            $.ajax(options)
                .done(function (data) {
                    if (data["Success"]) {
                        swal({ title: "Added!", text: "Schedule Added Successfully...!!", type: "success", confirmButtonText: "O.K.," })
                        .then(function (result) { window.location.reload(); });
                    }
                    else if (data["InvalidModelState"]){
                        var errors = data["InvalidModelState"];
                        var html = "";
                        for (var i = 0; i < errors.length; i++) {
                            html += "<div class='text-danger'>" + errors[i] + "</div>";
                        }
                        $model.find("div#modelErrorList").html(html);
                    }
                    else if (data["Exception"])
                        swal("Oops!", "An Erorr Occred While Deleting The Department...!!", "warning");
                    else if (data["NotFound"])
                        swal("Oops!", "Sorry, No Department Is Found...!!", "warning");
                    else if (data["InvalidId"])
                        swal("Oops!", "Sorry, Department Id Is Not Valid...!!", "warning");
                })
                .error(function () {
                    swal("Oops!", "We couldn't connect to the server!", "error");
                });
        });

        return false;
    });

    $(".editDoctorSchedule").on("click", function () {
        var $tr = $(this).closest("tr");
        var $model = $("div#AddModal");
        $model.find("select[name$='Day']").val($tr.find("input[name$='Day']").val());
        $model.find("input[name$='ArrivalTime']").val($tr.find("input[name$='ArrivalTime']").val());
        $model.find("input[name$='LeaveTime']").val($tr.find("input[name$='LeaveTime']").val());
        $model.find("input[name$='AverageTime']").val($tr.find("input[name$='AverageTime']").val());

        $("div#AddModal").modal();
    });

    $(".deleteDoctorSchedule").on("click", function () {
        var $tr = $(this).closest("tr");
        
        swal({
            title: "Are you sure?",
            text: "Do you really want to remove the schedule?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!"
        })
        .then(function (result) {
            if (!result.value) return;
            var options = {
                url: "DeleteDoctorSchedule",
                type: "POST",
                data: {
                    DoctorId: $("div#AddModal").attr("data-ppims-doctorId"),
                    Day: $tr.find("input[name$='Day']").val(),
                    __RequestVerificationToken: $tr.closest("table").find("input:hidden[name$='__RequestVerificationToken']").val()
                }
            };

            $.ajax(options)
                .done(function (data) {
                    if (data["Success"]) {
                        swal({ title: "Deleted!", text: "Schedule Deleted Successfully...!!", type: "success", confirmButtonText: "O.K.," });
                        $tr.remove();
                    }
                    else if (data["InvalidModelState"]) {
                        var errors = data["InvalidModelState"];
                        var html = "<p>Follwing errros occured while deleting record.</p>";
                        for (var i = 0; i < errors.length; i++) {
                            html += "<div class='text-danger'>" + errors[i] + "</div>";
                        }
                        swal({ title: "Error!", html: html, type: "error" });
                    }
                    else if (data["Exception"])
                        swal("Oops!", "An Erorr Occred While Deleting The Schedule...!!", "warning");
                    else if (data["NotFound"])
                        swal("Oops!", "Sorry, No Schedule Is Found...!!", "warning");
                })
                .error(function () {
                    swal("Oops!", "We couldn't connect to the server!", "error");
                });
        });

        return false;
    });
};

function logOff()
{
    $("a#btnLogOff").on("click", function () {
        $(this).closest("form").submit();

        return false;
    });
}

function maskInputs()
{
    $(".mask-cnic").each(function () {
        $(this).mask("99999-9999999-9");
    });

    $(".mask-phone-no").each(function () {
        $(this).mask("9999-9999999");
    });
}

function showUserAlerts()
{
    var $isEditSuccessfull = $("div#alertUser");

    if ($isEditSuccessfull.length > 0) {
        var headerMessage = $isEditSuccessfull.find("p#header-message").text();
        var mainMessage = $isEditSuccessfull.find("p#main-message").text();
        var alertType = $isEditSuccessfull.find("p#alert-type").text();
        var confirmButtonClass = $isEditSuccessfull.find("p#confirm-button-class").text();

        swal({
            title: headerMessage,
            text: mainMessage,
            type: alertType,
            confirmButtonClass: confirmButtonClass
        });
    }
}

function setupDateTimes()
{
    var $dates = $(".date");
    $dates.each(function () {
        $(this).datetimepicker({
            timepicker: false,
            format: 'Y-m-d',
            todayButton: true,
            startDate: new Date(),
            minDate: 0
        });
    });

    var $times = $(".time");
    $times.each(function () {
        $(this).datetimepicker({
            datepicker: false,
            format: 'g:i A',
            formatTime: 'g:i A',
            step: 30
        });
    });
}

function OnChangeAppintmentSearchOption() {
    $("select#SearchOptions").on("change", function () {

        var value = $(this).val();
        var InAnimataion = "visible animated fadeInLeft";
        var OutAnimataion = "animated fadeInRight hidden";

        $.each($("div[id^='SearchOption_']"), function (div) {
            $(this).addClass(OutAnimataion);
        });

        if (value == 1) {
            $(this).closest("form").find("input[type='submit']").addClass('hidden');
        }
        else if (value == 2) {
            $("div[id='SearchOption_ByName']").removeClass(OutAnimataion);
            $("div[id='SearchOption_ByName']").addClass(InAnimataion);
            $(this).closest("form").find("input[type='submit']").removeClass('hidden');
        }
        else if (value == 3) {
            $("div[id='SearchOption_ByDate']").removeClass(OutAnimataion);
            $("div[id='SearchOption_ByDate']").addClass(InAnimataion);
            $(this).closest("form").find("input[type='submit']").removeClass('hidden');
        }
        else if (value == 4) {
            $("div[id='SearchOption_ByDepartment']").removeClass(OutAnimataion);
            $("div[id='SearchOption_ByDepartment']").addClass(InAnimataion);
            $(this).closest("form").find("input[type='submit']").removeClass('hidden');
        }
    });
}

function OnSearchScheduleForm_Submit() {
    var $form = $(this);

    console.log($form.serialize());

    return validateSearchScheduleForm($form);
}

function validateSearchScheduleForm($form) {
    var value = $form.find("select#SearchOptions").val();
    var $errorPanel = $("#error-panel ul");

    if (value == 1)
        return false;

    if (value == 2) {
        var $input = $form.find("div[id='SearchOption_ByName'] input[name='DoctorName']");
        var searchTerm = $input.val();

        if (searchTerm === '') {
            $input.closest(".form-group").addClass("has-error");
            $("<li>Please enter a doctor name to search</li>").appendTo($errorPanel);

            return false;
        }
    }

    if (value == 3) {
        var startDate = $form.find("div[id='SearchOption_ByDate'] input[name$='ArrivalTime']").val();
        var endDate = $form.find("div[id='SearchOption_ByDate'] input[name$='LeaveTime']").val();

        console.log(startDate);
        console.log(endDate);

        return false;
    }

    if (value == 4) {
        $form.find("div[id='SearchOption_ByDepartment']").removeClass(OutAnimataion);

        return false;
    }

    return true;
}
