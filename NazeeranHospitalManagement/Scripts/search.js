﻿var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

window.onload = function () {
    logOff();
    showUserAlerts();

    var Arr = location.href.split("/").reverse();
    var controllerName = Arr[1];
    var actionName = Arr[0];

    if (location.href.match(/Prescriptions/i) != null) {
        Prescriptions();
    }
    else if (location.href.match(/Medicine\/ViewPatient/i) != null) {
        Medicine_ViewPatient();
    }
    else if (location.href.match(/Lab/i) != null) {
        Lab_Index();
    }
}

var Prescriptions = function () {
    $("button[name$='btnFinish']").on("click", function (evt) {
        evt.preventDefault();
        var $form = $(this).closest("form");

        swal({
            title: "Save Prescription!",
            text: "Do you want to save the prescription!",
            cancelButtonText: "No",
            confirmButtonText: "Yes, Save",
            type: "warning",
            showCancelButton: true,
            allowOutsideClick: false
        })
            .then(function (result) {
                if (!result.value) return;
                console.log($form.valid());
                if ($form.valid())
                    $form.submit();
            });
    });

    $("[data-ppims-hide]").on("click", function () {
        var targets = $(this).attr("data-ppims-hide").split(",");

        for (var i = 0; i < targets.length; i++) {
            $(targets[i]).addClass("hidden");
        }
    });

    $("input[data-ppims-autocomplete]").each(createAutoComplete);

    $("input:checkbox[data-ppims-enable]").on("change", function () {
        var target = $(this).attr("data-ppims-enable");

        var isDisabled = $(target).attr("disabled");

        if (isDisabled) {
            $(target).prop("disabled", "");
        }
        else {
            $(target).prop("disabled", "disabled");
        }
    });

    $("[data-ppims-toogle]").on("click", function () {
        var targets = $(this).attr("data-ppims-toogle").split(",");

        for (var i = 0; i < targets.length; i++) {
            $(targets[i]).toggleClass("hidden");
        }

        return false;
    });

    $("div#btnNextStep").on("click", "button", function () {
        var target = $(this).attr("data-ppims-target");

        $(this).parent("div").addClass("hidden");
        $("div.hidden#" + target).removeClass("hidden");
    });

    $("a[data-ppims-target]").on("click", function () {
        var parent = $(this).closest("div.cd-tabs");

        console.log(parent);
        if (parent.length == 0) {
            parent = $(this).attr("data-ppims-close");
        }

        console.log(parent);

        if (parent)
            $(parent).addClass("hidden");

        var target = $(this).attr("data-ppims-target");
        $(target).removeClass("hidden");
    });

    $("div#LabTestsHistory").on("click", "button[name='addToPrescription']", function () {
        var $tr = $(this).closest("tr").clone(true);

        $tr.find("button").each(function () {
            $(this).remove();
        });

        var date = new Date();
        $tr.find("label.recommenedOn").each(function () {
            $(this).text(date.getDate() + "-" + monthNames[date.getMonth()] + "-" + date.getFullYear());
        });

        $tr.find("td.resultStatus").each(function () {
            $(this).remove();
        });

        $("div#prescribed-test-panel table tbody").append($tr);
        changeInputNames($("div#prescribed-test-panel table tbody"), "model.PrescribedTestsByDoctor");
    });

    $("div#prescribed-medicines-panel").on("change", "input:not(hidden), select", function () {
        var name = $(this).attr("name");
        //console.log("name: " + name);

        if (name.match("Time$") === null && name.match("NoOfDays$") === null && name.match("Dosage$") === null)
            return;

        var $tr = $(this).closest("tr");
        var noOfDays = $tr.find("input[name$='NoOfDays']").val();
        var time = $tr.find("select[name$='Time']").val();
        var freq = $tr.find("select[name$='Dosage']").val();

        if (!noOfDays || noOfDays === '')
            noOfDays = 0;

        var times = time.split("+");

        var oneDayDosage = 0.0;
        for (var i = times.length - 1; i >= 0 ; i--)
            oneDayDosage += parseFloat(times[i]);

        var totalDosage = oneDayDosage * noOfDays * freq;

        if (!totalDosage) {
            totalDosage = 0;
        }

        //console.log("noOfDays: " + noOfDays);
        //console.log("time: " + time);
        //console.log("freq: " + freq);
        //console.log("oneDayDosage: " + oneDayDosage);
        //console.log("totalDosage: " + totalDosage);

        $tr.find("input[name$='Quantity']").val(totalDosage);
    });

    $("div#previous-prescribed-medicines").on("click", "button.addToPrescription", addMedicineToPrescription);

    changeInputNames("#prescribed-medicines-panel table tbody");

    $("#prescribed-medicines-panel table tbody").on("change", "input:not(hidden)", function () {
        var inputValue = $(this).val();

        var $td = $(this).closest("tr").find("td:hidden");
        var quantity = $td.find("input[name='item.Quantity']").val();

        if (inputValue < 0)
            $(this).val(0);
        else if (inputValue > quantity)
            $(this).val(quantity);
    });

    function createAutoComplete() {
        var $input = $(this);

        var options = {
            source: $input.attr("data-ppims-autocomplete"),
            select: submitAutoCompleteForm
        }

        $input.autocomplete(options);
    }

    function submitAutoCompleteForm(event, ui) {
        var $input = $(this);

        var calBack = $input.attr("data-ppims-autocomplete-select");
        var target = $input.attr("data-ppims-target");
        var modelName = $input.attr("data-ppims-model");

        var options = {
            data: {
                itemId: ui.item.itemId
            },
            type: "POST",
            url: (calBack == null || calBack == '') ? $input.attr("data-ppims-autocomplete") : calBack
        };

        $.ajax(options).done(function (data) {
            // check if target is contains the same data. compare with id's..!!
            var oldId = $(target).find("input:hidden[name$='Id']").val();
            var newId = $(data).find("input:hidden[name$='Id']").val();

            // if both olld and new id's are same, then don't append data to the target, istead display an erorr message...!!
            if (oldId === newId) {
                alert("Item is already in the table...!!");
                return;
            }

            $(target).append(data);

            changeInputNames(target, modelName);
        })
        .error(ajaxAutomcompleteFail);
    }

    function ajaxAutomcompleteFail(xhr, textStatus, errorThrown) {
        if (xhr.status === 404) {
            alert("Medicine Not Found..!!");
        }
        else if (xhr.status === 400) {
            alert("Sorry, Invalid Id");
        }
        else {
            alert("Sorry! something wrong happend...!! Please try again!");
        }
    }

    function changeInputNames(target, modelName) {
        var noOfChild = 0;

        $(target).find("tr").each(function () {
            $(this).find("input, select").each(function () {
                var name = $(this).attr("name");
                var updatedName = name;

                if (name.match(/Quantity$/)) {
                    updatedName = modelName + "[" + noOfChild + "].Quantity";
                }
                else if (name.match(/Time$/)) {
                    updatedName = modelName + "[" + noOfChild + "].Time";
                }
                else if (name.match(/NoOfDays$/)) {
                    updatedName = modelName + "[" + noOfChild + "].NoOfDays";
                }
                else if (name.match(/prescription_id$/)) {
                    updatedName = modelName + "[" + noOfChild + "].PrescriptionId";
                }
                else if (name.match(/MedicineId$/)) {
                    updatedName = modelName + "[" + noOfChild + "].MedicineId";
                }
                else if (name.match(/MedicineName$/)) {
                    updatedName = modelName + "[" + noOfChild + "].MedicineName";
                }
                else if (name.match(/MedicineFormula$/)) {
                    updatedName = modelName + "[" + noOfChild + "].MedicineFormula";
                }
                else if (name.match(/ItemId$/)) {
                    updatedName = modelName + "[" + noOfChild + "].ItemId";
                }
                else if (name.match(/MedicineId$/)) {
                    updatedName = modelName + "[" + noOfChild + "].ItemId";
                }
                else if (name.match(/Time$/)) {
                    updatedName = modelName + "[" + noOfChild + "].Time";
                }
                else if (name.match(/Frequency$/)) {
                    updatedName = modelName + "[" + noOfChild + "].Frequency";
                }
                else if (name.match(/Id/)) {
                    updatedName = modelName + "[" + noOfChild + "].ItemId";
                }

                //console.log("Old Name: " + name);
                //console.log("New Name: " + updatedName);
                $(this).attr("name", updatedName);
                $(this).attr("id", updatedName);
            });
            noOfChild++;
        });
    }

    function addMedicineToPrescription() {
        var $tr = $(this).closest("tr").clone(true);
        var $table = $tr.closest("table tbody");

        $table.each("tr", function () {
            var medicineId = $(this).find("input:hidden[namne$='MedicineId']").val();
            console.log(medicineId);
        })

        $tr.find("input[type!='hidden']").each(function () {
            $(this).val('')
                .removeAttr("disabled")
                .removeAttr("readonly");

            if ($(this).attr("name").match("Quantity$") !== null) {
                $(this).attr("disabled", "disabled")
            }
        });

        $tr.find("select").each(function () {
            $(this).removeAttr("disabled");
        });

        var date = new Date();
        $tr.find("label.recommenedOn").each(function () {
            $(this).text(date.getDate() + "-" + monthNames[date.getMonth()] + "-" + date.getFullYear());
        });

        $tr.find("button.addToPrescription").remove();
        $("div#prescribed-medicines-panel table tbody").append($tr);

        $(".nav-tabs a[href='#Addprescription']").tab('show');
        $(".nav-pills a[href='#Addprescription']").tab('show');

        changeInputNames($tr.closest("table tbody"), "model.PrescribedMedicinesByDoctor");
    }
}

var Medicine_ViewPatient = function () {
    $("input[name$='medicine_quantity']").on("keydown", onQuantityEnter);
    $("input[name$='medicine_quantity']").on("keyup", OnChangeQuantity);

    function OnChangeQuantity() {
        var $tr = $(this).closest("tr");
        var subTotal = getSubTotal($tr);

        if (isNaN(subTotal))
            subTotal = 0;

        $tr.find("label#subTotal").text(subTotal);

        calculateTotalBill();
    }

    function calculateTotalBill() {
        var totalBill = 0.0;

        $("table#prescribedMedicine tbody").find("tr").each(function () {
            totalBill += getSubTotal(this);
        });

        $("table#prescribedMedicine tfoot label#totalBill").text(totalBill);
        console.log(totalBill);
    }

    function getSubTotal($tr) {
        var $input = $tr.find("input[name$='medicine_quantity']");
        var quantity = parseFloat($input.val());
        var unitPrice = parseFloat($tr.find("input:hidden[name$='medicine_price']").val());
        var subTotal = (quantity * unitPrice);

        if (isNaN(subTotal))
            subTotal = 0;

        return subTotal;
    }

    function onQuantityEnter() {
        // Ensure that if it is bakcspace, then return true
        if (event.keyCode === 8) {
            return true;
        }

        // Ensure that if it is not a number then stop the keypress
        if (((event.shiftKey || (event.keyCode < 48 || event.keyCode > 57)) && (event.keyCode < 96 || event.keyCode > 105))) {
            return false;
        }
    }

    $("table#prescribedMedicine tbody").on("change", "input", function () {
        var quantity = parseInt($(this).val());
        var $tr = $(this).closest("tr");
        var inStockQuantity = $tr.find("input:hidden[name$='inStockQuantity']").val();

        if (quantity < 0)
            $(this).val(0);
        else if (quantity > inStockQuantity) {
            $(this).val(inStockQuantity);
            alert("sorry, only " + inStockQuantity + " medicines are available...!!");
        }

        calculateTotalBill();
    });
}

var Lab_Index = function () {
    $("#prescribedTest input").each(function () {
        var name = $(this).attr("name");
        name = name.replace("item.", "");

        $(this).attr("name", name);
    });
}

function showUserAlerts() {
    var $isEditSuccessfull = $("div#alertUser");

    if ($isEditSuccessfull.length > 0) {
        var headerMessage = $isEditSuccessfull.find("p#header-message").text();
        var mainMessage = $isEditSuccessfull.find("p#main-message").text();
        var alertType = $isEditSuccessfull.find("p#alert-type").text();
        var confirmButtonClass = $isEditSuccessfull.find("p#confirm-button-class").text();

        swal({
            title: headerMessage,
            text: mainMessage,
            type: alertType,
            confirmButtonClass: confirmButtonClass
        });

        $("div#alertUser").remove();
    }
}

function logOff() {
    $("a#btnLogOff").on("click", function () {
        $(this).closest("form").submit();

        return false;
    });
}

