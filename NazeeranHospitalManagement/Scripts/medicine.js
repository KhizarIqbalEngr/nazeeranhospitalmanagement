﻿window.onload = function () {
    logOff();
    var Arr = location.href.split("/").reverse();
    var controllerName = Arr[1];
    var actionName = Arr[0];

    if (location.href.match(/Prescriptions\/Prescriptions/i) != null) {
        Prescriptions();
    }
    else if (location.href.match(/Medicine\/ViewPatient/i) != null) {
        Medicine_ViewPatient();
    }
    else if (location.href.match(/Medicine\/Display/i) != null) {
        display();
    }
}

var display = function () {
   // $('#example').DataTable();
    var height = $('.body-content').height();
    console.log(height);
    console.log($('.pdsa-sn-wrapper').height());
    $('.pdsa-sn-wrapper').parent("div.container").height();
    console.log($('.pdsa-sn-wrapper').parent("div.container").height());

    $("input[data-ppims-autocomplete]").each(createAutoComplete)

    function createAutoComplete() {
        var $input = $(this);
        var options = {
            source: $input.attr("data-ppims-autocomplete"),
            select: submitAutoCompleteForm
        }

        $input.autocomplete(options);
    }
}

function submitAutoCompleteForm(event, ui) {
    var $input = $(this);
    var medicineId = $input.val(ui.item.medicineId);

    var $form = $input.parent("div:first").parent("div:first").parent("form:first");

    var options = {
        data: {
            medicineId: medicineId,
            __RequestVerificationToken: $form.find("input[name$='__RequestVerificationToken']").val()
        },
        type: "POST",
        url: $input.attr("data-ppims-autocomplete")
    };
    console.log(options);
    console.log($form);
    //$.ajax(options)
}

var Prescriptions = function () {
    var form = $("form[data-ppims-ajax='true']");
    if (form !== null)
        $("form[data-otf-ajax='true']").on("submit", ajaxFormSubmit);

    function ajaxFormSubmit() {
        var modelName = "model";
        var $form = $(this);
        var options = {
            url: $form.attr("action"),
            type: $form.attr("method"),
            data: $form.serialize()
        };

        console.log(options);

        $.ajax(options).done(function (data) {
            //console.log(data);

            var target = $($form.attr("data-otf-target"));

            var noOfChild = 0;//target.children().length;

            $(target).append(data);
            console.log(target);
            $(target).find("tr").each(function () {
                $(this).find("input").each(function () {
                    var name = $(this).attr("name");
                    var updatedName = name;

                    if (name.match(/Quantity$/)) {
                        updatedName = modelName + "[" + noOfChild + "].Quantity";
                    }
                    else if (name.match(/Time$/)) {
                        updatedName = modelName + "[" + noOfChild + "].Time";
                    }
                    else if (name.match(/NoOfDays$/)) {
                        updatedName = modelName + "[" + noOfChild + "].NoOfDays";
                    }
                    else if (name.match(/prescription_id$/)) {
                        updatedName = modelName + "[" + noOfChild + "].PrescriptionId";
                    }
                    else if (name.match(/MedicineId$/)) {
                        updatedName = modelName + "[" + noOfChild + "].MedicineId";
                    }

                    $(this).attr("name", updatedName);
                    $(this).attr("id", updatedName);
                });
                noOfChild++;
            });
        });

        return false;
    };

    $("#getId tbody").on("change", "input:not(hidden)", function () {
        var inputValue = $(this).val();

        var $td = $(this).closest("tr").find("td:hidden");
        var quantity = $td.find("input[name='item.Quantity']").val();

        if (inputValue < 0)
            $(this).val(0);
        else if (inputValue > quantity)
            $(this).val(quantity);
    });
}

var Medicine_ViewPatient = function () {
    
    $("input[name$='medicine_quantity']").on("keydown", onQuantityEnter);
    $("input[name$='medicine_quantity']").on("keyup", OnChangeQuantity);
    calculateTotalBill();
    changeHiddenNames();

    function OnChangeQuantity() {
        var tr = $(this).closest("tr");
        var subTotal = getSubTotal(tr);

        if (isNaN(subTotal))
            subTotal = 0;

        $(tr).find("label#subTotal").text(subTotal);

        calculateTotalBill();
    }

    function calculateTotalBill() {
        var totalBill = 0.0;

        $("table#prescribedMedicine tbody").find("tr").each(function () {
            var subTotal = getSubTotal(this);

            $(this).find("label#subTotal").text(subTotal);

            totalBill += subTotal;
        });

        $("table#prescribedMedicine tfoot label#totalBill").text(totalBill);
    }

    function getSubTotal(tr) {
        var input = $(tr).find("input[name$='medicine_quantity']");
        var quantity = parseFloat($(input).val());
        var unitPrice = parseFloat($(tr).find("input:hidden[name$='medicine_price']").val());
        var subTotal = (quantity * unitPrice);

        if (isNaN(subTotal))
            subTotal = 0;

        return subTotal;
    }

    function onQuantityEnter() {
        // Ensure that if it is bakcspace, then return true
        if (event.keyCode === 8) {
            return true;
        }

        // Ensure that if it is not a number then stop the keypress
        if (((event.shiftKey || (event.keyCode < 48 || event.keyCode > 57)) && (event.keyCode < 96 || event.keyCode > 105))) {
            return false;
        }
    }

    $("table#prescribedMedicine tbody").on("change", "input", function () {
        var quantity = parseInt($(this).val());
        var $tr = $(this).closest("tr");
        var inStockQuantity = $tr.find("input:hidden[name$='inStockQuantity']").val();

        if (!quantity) {
            $(this).val(0);
            alert("Sorry, not enter less than one Or empty");
        }
        else if (quantity > inStockQuantity) {
            $(this).val(inStockQuantity);
            alert("Sorry, only " + inStockQuantity + " medicines are enter");
        }

        calculateTotalBill();
    });

    $("input#btnSaveBill").on("click", function () {
        var $from = $(this).closest("form");

        $from.attr("action", "SaveBill");
        $from.prop("action", "SaveBill");

        $from.submit();
    });

    $("input#btnPrintBill").on("click", function () {
        var $from = $(this).closest("form");

        $from.attr("action", "GeneratePDF");
        $from.prop("action", "GeneratePDF");

        $from.submit();
    });

    function changeHiddenNames()
    {
        var target = $("table#prescribedMedicine tbody");
        var modelName = "model.m1";
        var noOfChild = 0;

        $(target).find("tr").each(function () {
            $(this).find("input").each(function () {
                var name = $(this).attr("name");
                var updatedName = name;

                if (name.match(/medicine_name$/)) {
                    updatedName = modelName + "[" + noOfChild + "].medicine_name";
                }
                else if (name.match(/medicine_quantity$/)) {
                    updatedName = modelName + "[" + noOfChild + "].medicine_quantity";
                }
                else if (name.match(/medicine_price$/)) {
                    updatedName = modelName + "[" + noOfChild + "].medicine_price";
                }
                else if (name.match(/medicine_time$/)) {
                    updatedName = modelName + "[" + noOfChild + "].medicine_time";
                }
                else if (name.match(/medicine_noOfday$/)) {
                    updatedName = modelName + "[" + noOfChild + "].medicine_noOfday";
                }
                else if (name.match(/price$/)) {
                    updatedName = modelName + "[" + noOfChild + "].price";
                }
                else if (name.match(/totalBill$/)) {
                    updatedName = modelName + "[" + noOfChild + "].totalBill";
                }

                $(this).attr("name", updatedName);
            });
            noOfChild++;
        });
    }
}

function logOff() {
    $("a#btnLogOff").on("click", function () {
        $(this).closest("form").submit();

        return false;
    });
}

