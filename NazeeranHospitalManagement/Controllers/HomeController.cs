﻿using NazeeranHospitalManagement.Models;
using System;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace NazeeranHospitalManagement.Controllers
{
    public class HomeController : Controller
    {
        NazeeranHospitalDbContext db = new NazeeranHospitalDbContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View(new AddQueryMessageViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Contact(AddQueryMessageViewModel model)
        {
            if(ModelState.IsValid)
            {
                try
                {
                    var queryMessage = new QueryMessage
                    {
                        Name = model.Name,
                        Email = model.Email,
                        PhoneNumber = model.PhoneNumber,
                        Question = model.Question,
                        PostedOn = DateTime.Now,
                        Answer = string.Empty,
                        AnswerOn = DateTime.MinValue,
                        UserId = string.Empty
                    };

                    db.Entry(queryMessage).State = EntityState.Added;
                    await db.SaveChangesAsync();

                    ViewData["Success"] = true;
                }
                catch(Exception ex)
                {
                    ViewData["Exception"] = true;
                }
            }

            return View(model);
        }
    }
}