﻿using Microsoft.AspNet.Identity;
using NazeeranHospitalManagement.Models;
using System.Web.Mvc;

namespace NazeeranHospitalManagement.Controllers
{
    [Authorize]
    public class ChatRoomController : Controller
    {
        NazeeranHospitalDbContext db = new NazeeranHospitalDbContext();
        string CurrentUserId
        {
            get
            {
                return User.Identity.GetUserId();
            }
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}