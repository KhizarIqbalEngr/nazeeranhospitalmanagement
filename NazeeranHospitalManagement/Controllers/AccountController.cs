﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using NazeeranHospitalManagement.Models;
using NazeeranHospitalManagement.ViewModels.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace NazeeranHospitalManagement.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private NazeeranHospitalSignInManager _signInManager;
        private NazeeranHospitalUserManager _userManager;
        private NazeeranHospitalRoleManager _roleManager;
        private const string LOGIN_BIND_PARAMS = "Email,Password,RemeberMe";

        public AccountController()
        {
        }

        public AccountController(NazeeranHospitalUserManager userManager, NazeeranHospitalSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public NazeeranHospitalSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<NazeeranHospitalSignInManager>();
            }
            private set 
            { 
                _signInManager = value; 
            }
        }

        public NazeeranHospitalUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<NazeeranHospitalUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public NazeeranHospitalRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().GetUserManager<NazeeranHospitalRoleManager>();
            }

            private set
            {
                _roleManager = value;
            }
        }

        [AllowAnonymous]
        public async Task<ActionResult> LogIn(string returnUrl)
        {
            if(User.Identity.IsAuthenticated)
            {
                return RedirectToLocal(await this.GetReturnUrl(User.Identity.GetUserId(), returnUrl));
            }

            ViewBag.ReturnUrl = returnUrl;

            var RegisterModel = ViewData["RegisterModel"] as RegisterViewModel;

            var Model = new LoginRegisterViewModel
            {
                LoginModel = new LoginViewModel(),
                RegisterModel = RegisterModel ?? new RegisterViewModel()
            };

            return View(Model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login([Bind(Include = LOGIN_BIND_PARAMS)] LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(new LoginRegisterViewModel { LoginModel = model, RegisterModel = new RegisterViewModel() });
            }

            var user = await UserManager.FindByEmailAsync(model.Email);

            SignInStatus result = (user == null) ? 
                SignInStatus.Failure :
                await SignInManager.PasswordSignInAsync(user.UserName, model.Password, model.RememberMe, shouldLockout: false);
            
            switch (result)
            {
                case SignInStatus.Success:
                    var roles = await UserManager.GetRolesAsync(user.Id);

                    return RedirectToLocal(await this.GetReturnUrl(user.Id, returnUrl));

                case SignInStatus.LockedOut:
                    return View("Lockout");

                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });

                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    model.Errors = new List<string>();
                    model.Errors.Add("Invalid Login Attempt");
                    model.Errors.Add("Please, Enter A Valid Username or Password!");

                    return View(new LoginRegisterViewModel { LoginModel = model, RegisterModel = new RegisterViewModel() });
            }
        }

        [NonAction]
        private async Task<string> GetReturnUrl(string id, string returnUrl)
        {
            if (await UserManager.IsInRoleAsync(id, UserRoles.Admin.ToString()))
                returnUrl = Url.Action("Index", "Admin");
            else if (await UserManager.IsInRoleAsync(id, UserRoles.Doctor.ToString()))
                returnUrl = Url.Action("Index", "DoctorSchedule");
            else if (await UserManager.IsInRoleAsync(id, UserRoles.Pharmacist.ToString()))
                returnUrl = Url.Action("Index", "Medicine");
            else if (await UserManager.IsInRoleAsync(id, UserRoles.LabAttendent.ToString()))
                returnUrl = Url.Action("Index", "Lab");
            else if (await UserManager.IsInRoleAsync(id, UserRoles.HelpDesk.ToString()))
                returnUrl = Url.Action("Index", "HelpDesk");
            else if (await UserManager.IsInRoleAsync(id, UserRoles.Patient.ToString()))
                returnUrl = Url.Action("Index", "Patient");
            else
                returnUrl = Url.Action("Index", "Home");

            return returnUrl;
        }

        [NonAction]
        public async Task<IdentityResult> RegisterWithOutPassword(NazeeranHospitalUser user)
        {
            return await UserManager.CreateAsync(user);
        }

        [NonAction]
        public async Task<IdentityResult> RegisterWithPassword(NazeeranHospitalUser user, string password)
        {
            IdentityResult res = IdentityResult.Success;
            try
            {
                res = await UserManager.CreateAsync(user, password);
            }
            catch(Exception ex)
            {
                ex.ToString();
                res = new IdentityResult(new string[] { "Sorry, An Error Occured While Creating Your Account...!!" });
            }

            return res;
        }

        [ChildActionOnly]
        public async Task<IdentityResult> AddRoleToUser(string userId, string role)
        {
            IdentityResult res = new IdentityResult();
            try
            {
                res = await UserManager.AddToRoleAsync(userId, role);
            }
            catch(Exception ex)
            {
                ex.ToString();
            }

            return res;
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new NazeeranHospitalUser
                {
                    Name = model.FullName,
                    Email = model.Email,
                    UserName = model.UserName,
                    Gender = model.PatientGender,
                    Cnic = model.Cnic
                };

                var result = await UserManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    await UserManager.AddToRoleAsync(user.Id, UserRoles.Patient.ToString());

                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                    //return RedirectToActionPermanent("Index", "Patient");
                    return RedirectToAction("Index", "Home");
                }

                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            ViewData["RegisterModel"] = model;

            return RedirectToActionPermanent("LogIn");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}