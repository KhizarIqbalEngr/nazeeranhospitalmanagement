﻿using NazeeranHospitalManagement.Models;
using NazeeranHospitalManagement.ViewModels;
using NazeeranHospitalManagement.ViewModels.Enums;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace NazeeranHospitalManagement.Controllers
{
    [Authorize(Roles = "LabAttendent")]
    public class LabController : Controller
    {
        private NazeeranHospitalDbContext db = new NazeeranHospitalDbContext();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Index(int? patientId)
        {
            if (ModelState.IsValid)
            {
                if (patientId.HasValue)
                {
                    var patient = await db.Patients.FirstOrDefaultAsync(pat => pat.Id == patientId.Value);
                    var medicalTests = await db.MedicalTests.Where(test => test.Prescription.PatientId == patientId.Value)
                        .OrderByDescending(o => o.Id)
                        .Include(o => o.Prescription.Doctor.User)
                        .ToListAsync();

                        var model = new ViewLabTestsViewModel
                        {
                            PatientId = patientId.Value,
                            PatientName = patient.User.Name,
                            DoctorName = medicalTests.ElementAt(0).Prescription.Doctor.User.Name,
                            DateOfBirth = new DateTime(1992, 09, 07),
                            Tests = new List<PrescribedTestsViewmodel>()
                        };

                        model.Tests.AddRange(medicalTests.Select(test => new PrescribedTestsViewmodel
                        {
                            MedicalTestTypeId = test.MedicalTestTypeId,
                            PatientId = test.Prescription.PatientId,
                            TestType = (TestTypes)Enum.Parse(typeof(TestTypes), test.MedicalTestType.Name),
                            MedicalTestId = test.Id,
                            PrescriptionId = test.PrescriptionId,
                            Status = test.Status
                        }));

                        bool isAdded = false;
                        if (TempData.Keys.Contains("IsAdded"))
                            isAdded = (bool)TempData["IsAdded"];

                        return View("LabPrescribe", model);
                }
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddTestResult(GetTestViewModel testModel)
        {
            ViewBag.MedicalTestId = testModel.MedicalTestId;
            try
            {
                var medicalTest = await db.MedicalTests.FindAsync(testModel.MedicalTestId);

                if (medicalTest == null)
                {
                    ViewData["Errors"] = new List<string> { "Sorry, Something Wen Wrong While Getting Date...!!" };
                }
                else if (medicalTest.Status == MedicalTestStatus.Completed)
                {
                    ViewData["Errors"] = new List<string> { "Sorry, Test Result Is Already Entered...!!" };
                }
            }
            catch(Exception ex)
            {
                ex.ToString();
                ViewData["Errors"] = new List<string> { "Sorry, There Was An Errror While Getting Test Details...!!" };
            }

            switch (testModel.TestType)
            {
                case TestTypes.BloodCP:
                    var BloodCPModel = new BloodCP { PrescriptionId = testModel.PrescriptionId };
                    return View(testModel.TestType.ToString(), BloodCPModel);

                case TestTypes.Calcuim:
                    var CalcuimModel = new Calcium { PrescriptionId = testModel.PrescriptionId };
                    return View(testModel.TestType.ToString(), CalcuimModel);

                case TestTypes.CrossMatch:
                    var CrossMatchModel = new CrossMatch {  };
                    return View(testModel.TestType.ToString(), CrossMatchModel);

                case TestTypes.TFT:
                    var TFTModel = new TFT { PrescriptionId = testModel.PrescriptionId };
                    return View(testModel.TestType.ToString(), TFTModel);
            }

            return RedirectToActionPermanent("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> BloodCP(BloodCP model, int? MedicalTestId)
        {
            ViewData["IsAdded"] = false;

            if (ModelState.IsValid && MedicalTestId.HasValue)
            {
                try
                {
                    var test = await db.MedicalTests.FirstOrDefaultAsync(t => t.Id == MedicalTestId.Value);

                    if(test == null)
                    {
                        ViewData["Errors"] = new List<string> { "Sorry, Something Wen Wrong While Getting Date...!!" };
                    }
                    else if (test.Status == MedicalTestStatus.Completed)
                    {
                        ViewData["Errors"] = new List<string> { "Sorry, Test Result Is Already Entered...!!" };
                    }
                    else if (test != null)
                    {
                        test.Status = MedicalTestStatus.Completed;
                        model.Status = MedicalTestStatus.Completed;

                        db.Entry(model).State = EntityState.Added;
                        db.Entry(test).State = EntityState.Modified;

                        await db.SaveChangesAsync();
                        ViewData["IsAdded"] = true;
                    }
                }
                catch (Exception ex)
                {
                    ex.ToString();
                    ViewData["Errors"] = new List<string> { "Sorry! An Error Occured While Updating Database...!!"};
                }
            }

            ViewBag.MedicalTestId = MedicalTestId.Value;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CrossMatch(CrossMatch model, int? MedicalTestId)
        {
            ViewData["IsAdded"] = false;

            if (ModelState.IsValid && MedicalTestId.HasValue)
            {
                try
                {
                    var test = await db.MedicalTests.FirstOrDefaultAsync(t => t.Id == MedicalTestId.Value);

                    if (test == null)
                    {
                        ViewData["Errors"] = new List<string> { "Sorry, Something Wen Wrong While Getting Data...!!" };
                    }
                    else if (test.Status == MedicalTestStatus.Completed)
                    {
                        ViewData["Errors"] = new List<string> { "Sorry, Test Result Is Already Entered...!!" };
                    }
                    else if (test != null)
                    {
                        test.Status = MedicalTestStatus.Completed;

                        db.Entry(model).State = EntityState.Added;
                        db.Entry(test).State = EntityState.Modified;

                        await db.SaveChangesAsync();
                        ViewData["IsAdded"] = true;
                    }
                }
                catch (Exception ex)
                {
                    ex.ToString();
                    ViewData["Errors"] = new List<string> { "Sorry! An Error Occured While Updating Database...!!" };

                }
            }

            ViewBag.MedicalTestId = MedicalTestId.Value;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> TFT(TFT model, int? MedicalTestId)
        {
            ViewData["IsAdded"] = false;

            if (ModelState.IsValid && MedicalTestId.HasValue)
            {
                try
                {
                    var test = await db.MedicalTests.FirstOrDefaultAsync(t => t.Id == MedicalTestId.Value);

                    if (test == null)
                    {
                        ViewData["Errors"] = new List<string> { "Sorry, Something Wen Wrong While Getting Date...!!" };
                    }
                    else if (test.Status == MedicalTestStatus.Completed)
                    {
                        ViewData["Errors"] = new List<string> { "Sorry, Test Result Is Already Entered...!!" };
                    }
                    else if (test != null)
                    {
                        test.Status = MedicalTestStatus.Completed;

                        db.Entry(model).State = EntityState.Added;
                        db.Entry(test).State = EntityState.Modified;

                        await db.SaveChangesAsync();
                        ViewData["IsAdded"] = true;
                    }
                }
                catch (Exception ex)
                {
                    ex.ToString();
                    ViewData["Errors"] = new List<string> { "Sorry! An Error Occured While Updating Database...!!" };
                }
            }

            ViewBag.MedicalTestId = MedicalTestId.Value;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Calcuim(Calcium model, int? MedicalTestId)
        {
            ViewData["IsAdded"] = false;

            if (ModelState.IsValid && MedicalTestId.HasValue)
            {
                try
                {
                    var test = await db.MedicalTests.FirstOrDefaultAsync(t => t.Id == MedicalTestId.Value);

                    if (test == null)
                    {
                        ViewData["Errors"] = new List<string> { "Sorry, Something Wen Wrong While Getting Date...!!" };
                    }
                    else if (test.Status == MedicalTestStatus.Completed)
                    {
                        ViewData["Errors"] = new List<string> { "Sorry, Test Result Is Already Entered...!!" };
                    }
                    else if (test != null)
                    {
                        test.Status = MedicalTestStatus.Completed;

                        db.Entry(model).State = EntityState.Added;
                        db.Entry(test).State = EntityState.Modified;

                        await db.SaveChangesAsync();
                        ViewData["IsAdded"] = true;
                    }
                }
                catch (Exception ex)
                {
                    ex.ToString();
                    ViewData["Errors"] = new List<string> { "Sorry! An Error Occured While Updating Database...!!" };
                }
            }

            ViewBag.MedicalTestId = MedicalTestId.Value;

            return View(model);
        }
    }
}