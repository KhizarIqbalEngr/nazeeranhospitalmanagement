﻿using Microsoft.AspNet.Identity;
using NazeeranHospitalManagement.Models;
using NazeeranHospitalManagement.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using System.Web.UI.WebControls;

namespace NazeeranHospitalManagement.Controllers
{
    [HandleError]
    [Authorize(Roles = "Pharmacist")]
    public class MedicineController : Controller
    {
        private NazeeranHospitalDbContext db = new NazeeranHospitalDbContext();

        public ActionResult Index()
        {
            return View();
        }

        [WebMethod]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> Display()
        {
            return View(await db.Medicines.ToListAsync());
        }

        public async Task<ActionResult> GetMed(string Name, string ManufacturerName, string ChemicalComposition)
        {
            var Medicines = await db.Medicines.ToListAsync();

            if (!string.IsNullOrEmpty(Name))
            {
                Medicines = Medicines.Where(med => med.Name.ToLower().Contains(Name.ToLower())).ToList();
            }

            if (!string.IsNullOrEmpty(ManufacturerName))
            {
                Medicines = Medicines.Where(med => med.ManufacturerName.ToLower().Contains(ManufacturerName.ToLower())).ToList();
            }

            if (!string.IsNullOrEmpty(ChemicalComposition))
            {
                Medicines = Medicines.Where(med => med.ChemicalComposition.ToLower().Contains(ChemicalComposition.ToLower())).ToList();
            }

            return View("Display", Medicines);
        }

        public async Task<ActionResult> GetMedUpdate(string Name, string ManufacturerName, string ChemicalComposition)
        {
            var Medicines = await db.Medicines.ToListAsync();

            if (!string.IsNullOrEmpty(Name))
            {
                Medicines = Medicines.Where(med => med.Name.ToLower().Contains(Name.ToLower())).ToList();
            }

            if (!string.IsNullOrEmpty(ManufacturerName))
            {
                Medicines = Medicines.Where(med => med.ManufacturerName.ToLower().Contains(ManufacturerName.ToLower())).ToList();
            }

            if (!string.IsNullOrEmpty(ChemicalComposition))
            {
                Medicines = Medicines.Where(med => med.ChemicalComposition.ToLower().Contains(ChemicalComposition.ToLower())).ToList();
            }

            return View("Update", Medicines);
        }

        public async Task<JsonResult> SearchMedidicne(string term = "")
        {
            var MedicinesList = await db.Medicines
                .Where(med => med.Name.ToLower().Contains(term.ToLower()))
                .ToListAsync();

            var Lists = MedicinesList.Select(med => new
            {
                label = $"{med.Name} {med.Type} {med.Potency}",
                value = med.Name,
                medicineId = med.Id
            });

            return Json(Lists, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> SearchMedidicneByManu(string term = "")
        {
            var MedicinesList = db.Medicines
                .Where(med => med.ManufacturerName.ToLower().Contains(term.ToLower()))
                .Select(med => new
                {
                    label = med.ManufacturerName,
                    value = med.ManufacturerName,
                    medicineId = med.Id
                });

            return Json(await MedicinesList.ToListAsync(), JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> SearchMedidicneByChemi(string term = "")
        {
            var MedicinesList = db.Medicines
                .Where(med => med.ChemicalComposition.ToLower().Contains(term.ToLower()))
                .Select(med => new
                {
                    label = med.ChemicalComposition,
                    value = med.ChemicalComposition,
                    medicineId = med.Id
                });

            return Json(await MedicinesList.ToListAsync(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewPatient()
        {
            if (TempData.ContainsKey("ErrorWhilePrescriptionAdded"))
                ViewData["ErrorWhilePrescriptionAdded"] = true;
            else if (TempData.ContainsKey("PrescriptionAdded"))
                ViewData["PrescriptionAdded"] = true;

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ViewPatient(int? idd)
        {
            if (ModelState.IsValid)
            {
                if (idd.HasValue)
                {
                    var prescription = db.Prescriptions.Where(p => p.PatientId == idd.Value).OrderByDescending(o => o.Id).ToList();
                    if (prescription.Count > 0)
                    {
                        var m = new A();
                        //m1.prescription_id = prescription[0].Id;
                        int Id = prescription[0].Id;
                        var prescribeMedicine = db.PrescribedMedicines.Where(k => k.PrescriptionId == Id).ToList();

                        m.idd = idd.Value;
                        m.m1 = new List<PrescribeMedicinesViewmodel>();
                        for (int i = 0; i < prescribeMedicine.Count; i++)
                        {
                            PrescribeMedicinesViewmodel m1 = new PrescribeMedicinesViewmodel();
                            m1.medicine_id = prescribeMedicine[i].MedicineId;
                            m1.medicine_quantity = prescribeMedicine[i].Quantity;
                            m1.medicine_time = prescribeMedicine[i].Time;
                            m1.medicine_noOfday = prescribeMedicine[i].NoOfDays;


                            //for (int j = 0; j < prescribeMedicine.Count; j++)
                            if (prescribeMedicine.Count > 0)
                            {
                                var prescribeMedicineName = db.Medicines.Where(n => n.Id == m1.medicine_id).ToList();
                                m1.medicine_name = prescribeMedicineName[0].Name;
                                m1.TypeM = prescribeMedicineName[0].Type;
                                m1.PotencyM = prescribeMedicineName[0].Potency;
                                m1.medicine_price = prescribeMedicineName[0].PricePerUnit;
                                m1.price = (m1.medicine_quantity * m1.medicine_price);
                                m.m1.Add(m1);
                            }
                        }

                        return View("GivePrescribe", m);

                    }
                    else
                    {
                        return View();
                    }
                }
            }
            return View();
        }

        public ActionResult bill()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> bill(A model)
        {
            if (ModelState.IsValid)
            {
                foreach (var m in model.m1)
                {
                    //var MedicineName = await db.Medicines.FirstOrDefaultAsync(n => n.Name == m.medicine_name);
                    //MedicineName.Quantity = MedicineName.Quantity - m.medicine_quantity;

                    //db.Entry(MedicineName).Status = EntityState.Modified;
                }

                try
                {
                    var bill = new MedicineBill { PatientId = model.idd, PharmacistId = User.Identity.GetUserId(), Amount = 1, BillTime = DateTime.Now };
                    db.Entry(bill).State = EntityState.Added;

                    await db.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    ex.ToString();
                }

                return View("bill", model);
            }
            return RedirectToAction("GivePrescribe", model);
        }

        public ActionResult SaveBill(A model)
        {
            TryUpdateModel(model, new[] { "idd" });
            TryValidateModel(model);

            if (ModelState.IsValid)
            {
                double result = 0.0;
                double result1 = 0.0;
                for (int i = 0; i < model.m1.Count; i++)
                {
                    if (model.m1[i].medicine_quantity > 0)
                    {
                        string s = model.m1[i].medicine_name;
                        var MedicineName = db.Medicines.FirstOrDefault(n => n.Name == s);
                        MedicineName.Quantity = MedicineName.Quantity - model.m1[i].medicine_quantity;
                        db.Entry(MedicineName).State = EntityState.Modified;
                        result1 = model.m1[i].medicine_price * model.m1[i].medicine_quantity;
                        result += result1;
                    }

                }
                var bill = new MedicineBill { PatientId = model.idd, PharmacistId = User.Identity.GetUserId(), Amount = result, BillTime = DateTime.Now };
                db.Entry(bill).State = EntityState.Added;
                try
                {
                    db.SaveChanges();
                    TempData["PrescriptionAdded"] = true;
                    //return new ViewAsPdf("bill", model)
                    //{
                    //    FileName = "BillNumber.pdf"
                    //};
                }
                catch (Exception ex)
                {
                    TempData["ErrorWhilePrescriptionAdded"] = true;
                    ex.ToString();
                }
            }

            return RedirectToAction("ViewPatient");
        }

        //public ActionResult GeneratePDF(A model, FormCollection fc)
        //{
        //    TryUpdateModel(model, new[] {"idd"});
        //    TryValidateModel(model);

        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            double result = 0.0;
        //            double result1 = 0.0;
        //            for (int i = 0; i < model.m1.Count; i++)
        //            {
        //                if (model.m1[i].medicine_quantity > 0)
        //                {
        //                    string s = model.m1[i].medicine_name;
        //                    var MedicineName = db.Medicines.FirstOrDefault(n => n.Name == s);
        //                    result1 = model.m1[i].medicine_price * model.m1[i].medicine_quantity;
        //                    result += result1;
        //                }

        //            }

        //            return new ViewAsPdf("bill", model)
        //            {
        //                FileName = "BillNumber.pdf"
        //            };
        //        }
        //        catch (Exception ex)
        //        {
        //            ex.ToString();
        //        }
        //    }

        //    return RedirectToAction("ViewPatient", model);
        //}

        public ActionResult Update()
        {
            return View(db.Medicines.ToList());
        }

        public ActionResult bills()
        {
            return View(db.MedicineBills);
        }

        // GET: /Medicine/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Medicine medicine = db.Medicines.Find(id);
            if (medicine == null)
            {
                return HttpNotFound();
            }
            return View(medicine);
        }

        // GET: /Medicine/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,ManufacturerName,ManufactuerDate,ExpiryDate,PricePerUnit,ChemicalComposition,Quantity,BatchNo,QuantityThreshold,ExpiryThreshold")] Medicine medicine)
        {
            if (ModelState.IsValid)
            {
                db.Medicines.Add(medicine);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(medicine);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Medicine medicine = db.Medicines.Find(id);
            if (medicine == null)
            {
                return HttpNotFound();
            }
            return View(medicine);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Medicine medicine, FormCollection fc)
        {
            DateTime Date = DateTime.Now;

            if (DateTime.TryParse(fc["ExpiryThreshold"], out Date))
            {
                medicine.ExpiryThreshold = Date;
                TryValidateModel(medicine);
            }

            if (ModelState.IsValid)
            {
                db.Entry(medicine).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Display");
            }
            return View(medicine);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Medicine medicine = db.Medicines.Find(id);
            if (medicine == null)
            {
                return HttpNotFound();
            }
            return View(medicine);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Medicine medicine = db.Medicines.Find(id);
            db.Medicines.Remove(medicine);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        [ChildActionOnly]
        public ActionResult GetExpiredMedicines()
        {
            ViewBag.NoOfMedicines = db.Medicines.Where(d => d.ExpiryThreshold <= DateTime.Now && DateTime.Now <= d.ExpiryDate).Count();
            //var med = db.Medicines.Where(d => d.ExpiryThreshold <= DateTime.Now).Count();

            var abb = db.Medicines.RemoveRange(db.Medicines.Where(c => c.ExpiryDate <= DateTime.Now)).ToList();

            return PartialView("_GetExpiredMedicines");
        }

        public async Task<ActionResult> ShowExpiredMedicines()
        {
            var result = await db.Medicines.Where(d => d.ExpiryThreshold <= DateTime.Now && DateTime.Now <= d.ExpiryDate).ToListAsync();

            return View(result);
        }

        [ChildActionOnly]
        public ActionResult GetQuantityMedicines()
        {
            ViewBag.NoOfMedicines = db.Medicines.Where(m => m.Quantity <= m.QuantityThreshold && m.Quantity != 0).Count();
            return PartialView("_GetQuantityMedicine");
        }


        public ActionResult ShowQuantityMedicines()
        {
            var mod = db.Medicines.Where(m => m.Quantity <= m.QuantityThreshold && m.Quantity != 0).ToList();
            return View(mod);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        ///// ---------------------------------- for  calendar  ------- Install-Package jQuery.Fullcalendar  --------------  Install-Package moment.js ---------------
        public ActionResult Calendar()
        {
            return View();
        }

        public ActionResult Meddd()
        {
            var Med = db.Medicines.ToList();

            var Exp = Med.Select(m => new
            {
                id = m.Id,
                title = m.Name,
                start = string.Format("{0}/{1}/{2}", m.ExpiryDate.Year, m.ExpiryDate.Month, m.ExpiryDate.Day),
                end = string.Format("{0}/{1}/{2}", m.ExpiryDate.Year, m.ExpiryDate.Month, m.ExpiryDate.Day),
                allDay = true,
                color = "red"
            });

            return Json(Exp, JsonRequestBehavior.AllowGet);
        }


        ///// ---------------------------------- for sending mail----------------------------------------------------------------------------- ---------------
        public ActionResult SendMail()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SendMail(NazeeranHospitalManagement.Models.MailModel objModelMail, HttpPostedFileBase fileUploader)
        {
            if (ModelState.IsValid)
            {
                string from = "umairdaudraja@gmail.com";
                using (MailMessage mail = new MailMessage(from, objModelMail.To))
                {
                    mail.Subject = objModelMail.Subject;
                    mail.Body = objModelMail.Body;
                    if (fileUploader != null)
                    {
                        string fileName = Path.GetFileName(fileUploader.FileName);
                        mail.Attachments.Add(new Attachment(fileUploader.InputStream, fileName));
                    }
                    mail.IsBodyHtml = false;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = true;
                    NetworkCredential networkCredential = new NetworkCredential(from, "rjumair777@php");
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = networkCredential;
                    smtp.Port = 587;
                    smtp.Send(mail);
                    ViewBag.Message = "Sent";
                    return View("ViewPatient", objModelMail);
                }
            }
            else
            {
                return View();
            }
        }

        public ActionResult Laboratorists()
        {
            return View();
        }

        public object dayss { get; set; }
    }
}
