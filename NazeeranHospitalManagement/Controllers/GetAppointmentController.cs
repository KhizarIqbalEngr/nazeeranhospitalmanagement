﻿
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using NazeeranHospitalManagement.Models;
using NazeeranHospitalManagement.Models.Helpers;
using NazeeranHospitalManagement.ViewModels;
using NazeeranHospitalManagement.ViewModels.Enums;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace NazeeranHospitalManagement.Controllers
{
    public class GetAppointmentController : Controller
    {
        private NazeeranHospitalDbContext db = new NazeeranHospitalDbContext();
        private const string SearchScheduleBindParams = "DepartmentId,DoctorName,Date,SearchOption,FilterOption";

        [HttpGet]
        public async Task<ActionResult> SearchSchedule(SearchScheduleViewModel model)
        {
            // Set the default option to be SelectAnOption for the Search Option!
            model.SearchOption = SearchBy.SelectAnOption;

            // Get List of all departments...!!
            model.DepartmentList = await this.GetDepartments();

            // Set the SearchAppointmentViewModel an empty object!
            model.SearchAppointmentViewModel = new SearchAppointmentViewModel();

            if (model.WantCancelling)
            {
                model.OldAppointment = await GetAppointmentDetails(model.OldAppointmentId);
            }

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> GetSchedule(SearchScheduleViewModel model)
        {
            var Date = new DateTime();
            if (DateTime.TryParse(this.Request.QueryString["Date"], out Date))
            {
                model.Date = Date;
            }

            if (model.WantCancelling)
            {
                model.OldAppointment = await GetAppointmentDetails(model.OldAppointmentId);
            }

            // Get the list of DoctorSchedule that meets the selected criteria by the user!
            model.DoctorSchedule = await this.GetDoctorSchedule(model);

            // Get the list of all departments!
            model.DepartmentList = await this.GetDepartments();

            if (model.FilterOption == SearchBy.Dates || model.FilterOption == SearchBy.Department || model.FilterOption == SearchBy.Name)
            {
                // Filter doctor schedules according to the user criteria!
                await FilterDoctorSchedule(model);

                // If filter option is not 0, then set the IsFiltered to true to indicate that the search has been performed!
                model.IsFiltered = true;
            }

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> ViewSchedule(GetScheduleViewModel model)
        {
            var doctor = await db.Doctors.FirstOrDefaultAsync(doc => doc.UserId.Equals(model.DoctorId));
            var scheduleModel = new DoctorScheduleViewModel();
            scheduleModel.GetScheduleModel = model ?? new GetScheduleViewModel();
            scheduleModel.Appointments = new List<DoctorAppointmentsViewModel>();
            scheduleModel.WantCancelling = model.WantCancelling;

            if (doctor != null)
            {
                this.CreateDates(model);
                var doctorAppointments = doctor.Appointments.Where(
                    app => app.Date.Date >= model.StartDate.Date &&
                    app.Date.Date <= model.EndDate.Date &&
                    (app.Status == AppointmentStatus.Pending || app.Status == AppointmentStatus.Scheduled));

                int totalDays = (model.EndDate - model.StartDate).Days;
                var appointmentSlots = new List<DoctorAppointmentsViewModel>();
                for (var dayNo = 0; dayNo < totalDays; dayNo++)
                {
                    var day = (WeekDay)((int)model.StartDate.DayOfWeek);
                    var date = model.StartDate;
                    var schedule = doctor.DoctorSchedule.FirstOrDefault(sch => sch.Day == day);
                    var slots = GetAppointmentSlot(doctorAppointments, schedule, doctor.AverageTime, model.StartDate);
                    appointmentSlots.Add(new DoctorAppointmentsViewModel
                    {
                        Day = day,
                        Date = date,
                        AppointmentSlots = slots
                    });
                    model.StartDate = model.StartDate.AddDays(1);
                }

                scheduleModel.Appointments = appointmentSlots;
                scheduleModel.DoctorName = doctor.User.Name;
                scheduleModel.OldAppointmentId = -1;

                if (scheduleModel.WantCancelling)
                {
                    scheduleModel.OldAppointmentId = model.OldAppointmentId;
                    scheduleModel.OldAppointment = await this.GetAppointmentDetails(model.OldAppointmentId);
                    scheduleModel.BookAppointmentViewModel = new BookAppointmentViewModel
                    {
                        Status = scheduleModel.OldAppointment.Status,
                        PatientName = scheduleModel.OldAppointment.PatientName,
                        Cnic = scheduleModel.OldAppointment.PatientCnic,
                        PatientId = scheduleModel.OldAppointment.PatientId,
                        Email = scheduleModel.OldAppointment.PatientEmail,
                        ContactNo = scheduleModel.OldAppointment.PatientContactNo,
                        Gender = scheduleModel.OldAppointment.PatientGender
                    };
                }
            }

            // If doctor Is null Create A New List For Errors...!! Add Any Error To The Errors List To Inform The User...!!
            scheduleModel.Errors = scheduleModel.Errors ?? new List<string>();
            AddModelErrors(scheduleModel, ModelState);

            return View(scheduleModel);
        }

        [ValidateAntiForgeryToken]
        public async Task<ActionResult> BookAppointment(BookAppointmentViewModel model)
        {
            model.Errors = model.Errors ?? new List<string>();
            model.IsAppointmentBook = false;

            var patient = await SavePatient(model);

            if (patient == null)
                return View(model);

            model.PatientId = patient.Id;
            var doctor = await db.Doctors.SingleOrDefaultAsync(doc => doc.UserId.Equals(model.DoctorId));

            if (await IsAlreadyAppointmentBooked(patient, doctor, model.Date))
            {
                model.Errors.Add("Sorry, You Already Have An Appointment With This Doctor!");
                model.Errors.Add("You Can Not Have Multiple DoctorSchedule With The Same Doctor On The Same Day!");
            }
            else if (doctor != null)
            {
                var scheduledAppointment = await GetScheduledAppointment(model, doctor);
                scheduledAppointment.PatientId = patient.Id;

                try
                {
                    db.Entry(scheduledAppointment).State = model.OldAppointmentId > 0 ? EntityState.Modified : EntityState.Added;
                    await db.SaveChangesAsync();

                    model.IsAppointmentBook = true;
                    model.AppointmentId = scheduledAppointment.Id;
                    model.AppointmentNo = scheduledAppointment.AppointmentNo;
                    model.DoctorName = doctor.User.Name;
                    model.Status = AppointmentStatus.Pending;

                    // Send Message To The User About Appointment...!!
                    await SendMessage(model);
                }
                catch (Exception ex)
                {
                    ex.ToString();
                    model.Errors.Add("Sorry, There Was An Error While Booking Appointment...!!!");
                    model.Errors.Add("Please Try Again Or Contact To The Admin If Problem Persists..!!!");
                }
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SearchAppointment(SearchAppointmentViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToActionPermanent("SearchSchedule");
            }

            Patient patient = null;

            if (!string.IsNullOrEmpty(model.Cnic))
                patient = await db.Patients.FirstOrDefaultAsync(pat => pat.User.Cnic == model.Cnic);

            if (patient == null)
            {
                model.IsFound = false;
            }
            else
            {
                var appointment = await db.Appointments.FirstOrDefaultAsync(app => app.AppointmentNo == model.AppointmentId && app.PatientId == patient.Id);

                model.IsFound = (appointment != null);

                if (model.IsFound)
                {
                    var isOverDate = (appointment.Date.Date < DateTime.Now.Date) && (appointment.Time.Hours < DateTime.Now.Hour && appointment.Time.Minutes < DateTime.Now.Minute);
                    var doctor = await db.Doctors.FirstOrDefaultAsync(doc => doc.Id == appointment.DoctorId);

                    model.AppointmentId = appointment.Id;
                    model.AppointmentNo = appointment.AppointmentNo;
                    model.Date = appointment.Date;
                    model.DoctorName = doctor.User.Name;
                    model.State = isOverDate ? AppointmentStatus.OverDate : appointment.Status;
                    model.Time = appointment.Time;
                    model.Cnic = patient.User.Cnic;
                }
            }

            return View(model);
        }

        [NonAction]
        public async Task<ViewAppointmentViewModel> GetAppointmentDetails(int AppointmentId)
        {
            var appointment = await db.Appointments.FirstOrDefaultAsync(app => app.AppointmentNo == AppointmentId);
            ViewAppointmentViewModel model = null;

            if (appointment != null)
            {
                var doctor = await db.Doctors.FirstOrDefaultAsync(doc => doc.Id == appointment.DoctorId);
                var patient = await db.Patients.FirstOrDefaultAsync(pat => pat.Id == appointment.PatientId);

                model = new ViewAppointmentViewModel
                {
                    AppointmentId = appointment.Id,
                    Day = (WeekDay)((int)appointment.Date.DayOfWeek),
                    AppointmentTime = appointment.Date.Date + appointment.Time,
                    DepartmentName = doctor.Department.Name,
                    DoctorName = doctor.User.Name,
                    PatientName = patient.User.Name,
                    Status = appointment.Status,
                    PatientId = patient.Id,
                    PatientCnic = patient.User.Cnic,
                    PatientContactNo = patient.User.PhoneNumber,
                    PatientEmail = patient.User.Email,
                    PatientGender = patient.User.Gender
                };
            }

            return model;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> CancelAppointment(int appointmentId)
        {
            var response = new Dictionary<string, object>();

            var appointment = await db.Appointments.FirstOrDefaultAsync(app => app.AppointmentNo == appointmentId);

            if (appointment != null)
            {
                if (appointment.Status == AppointmentStatus.Cancelled)
                {
                    response["AlreadyCancelled"] = new string[] { "Appointment Is Already Cancelled..." };
                }
                else
                {
                    appointment.Status = AppointmentStatus.Cancelled;
                    db.Entry(appointment).State = EntityState.Modified;

                    if (await db.SaveChangesAsync() > 0)
                        response["Success"] = new string[] { "Success" };
                    else
                        response["Error"] = new string[] { "An Error Occured While Cancelling Your Appointment. Try Again." };

                }
            }
            else
                response["NotFound"] = new string[] { "No Appointment Is Found..." };

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult FindAppointment(bool WantCancelling)
        {
            return View(new SearchAppointmentViewModel { WantCancelling = WantCancelling });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Helper Methods...!!!

        [NonAction]
        public async Task<IEnumerable<ViewDoctorsViewModel>> GetDoctorSchedule(SearchScheduleViewModel model)
        {
            var query = db.Doctors.AsQueryable();

            if (model.SearchOption == SearchBy.Name)
            {
                query = query.Where(doc => doc.User.Name.ToLower().Contains(model.DoctorName.ToLower()));
            }
            else if (model.SearchOption == SearchBy.Dates)
            {
                var weekDay = (WeekDay)((int)model.Date.Value.DayOfWeek);
                query = query.Where(doc => doc.DoctorSchedule.Any(sch => sch.Day == weekDay) == true);
            }
            else if (model.SearchOption == SearchBy.Department)
            {
                query = query.Where(doc => doc.DepartmentId == model.DepartmentId);
            }

            var doctorList = await query.Select(doc => new ViewDoctorsViewModel
            {
                DoctorId = doc.UserId,
                DepartmentId = doc.DepartmentId,
                DoctorName = doc.User.Name,
                DepartmentName = doc.Department.Name
            })
            .ToListAsync();

            return doctorList;
        }

        [NonAction]
        private async Task FilterDoctorSchedule(SearchScheduleViewModel model)
        {
            switch (model.FilterOption)
            {
                case SearchBy.Name:
                    // Get list of all doctors conatins the name in the Model.DoctorName
                    model.DoctorSchedule = model.DoctorSchedule.Where(ds => ds.DoctorName.ToLower().Contains(model.DoctorName.ToLower())).ToList();
                    break;

                case SearchBy.Dates:
                    // Get list of all doctors who is available in the time duration of Model.ArrivalTime and Model.LeaveTime
                    var doctorsIds = model.DoctorSchedule.Select(sch => sch.DoctorId).ToList();
                    var doctorsSchedule = await db.DoctorSchedules.Where(doc => doctorsIds.Contains(doc.UserId)).ToListAsync();
                    var day = (WeekDay)((int)model.Date.Value.DayOfWeek);
                    model.DoctorSchedule = model.DoctorSchedule
                        .Where(ds => (doctorsSchedule.Any(sch => sch.UserId == ds.DoctorId && sch.Day == day) == true))
                        .ToList();
                    break;

                case SearchBy.Department:
                    // Get list of all doctors of the department equals Model.DepartmentId.
                    model.DoctorSchedule = model.DoctorSchedule.Where(ds => ds.DepartmentId == model.DepartmentId).ToList();
                    break;
            }
        }

        [NonAction]
        private async Task<IEnumerable<SelectListItem>> GetDepartments()
        {
            List<SelectListItem> Departments = (await db.Departments.ToListAsync())
                    .Select(dep => new SelectListItem
                    {
                        Value = dep.Id.ToString(),
                        Text = dep.Name
                    })
                    .ToList();

            Departments.Insert(0, new SelectListItem { Selected = true, Value = "-1", Text = "Select Department" });

            return Departments;
        }

        [NonAction]
        private DoctorScheduleController GetDoctorScheduleController()
        {
            var doctorScheduleController = ControllerBuilder.Current.GetControllerFactory().CreateController(HttpContext.Request.RequestContext, "DoctorSchedule") as DoctorScheduleController;

            doctorScheduleController.ControllerContext = this.ControllerContext;

            return doctorScheduleController;
        }

        [NonAction]
        private AccountController GetAccountController()
        {
            var cont = ControllerBuilder.Current.GetControllerFactory().CreateController(HttpContext.Request.RequestContext, "AccountController") as AccountController;

            cont.ControllerContext = this.ControllerContext;

            return cont;
        }

        [NonAction]
        public async Task<bool> IsAlreadyAppointmentBooked(Patient patient, Doctor doctor, DateTime Date)
        {
            var Appointment = await db.Appointments.FirstOrDefaultAsync(app =>
                app.PatientId == patient.Id &&
                app.DoctorId == doctor.Id &&
                (app.Status == AppointmentStatus.Scheduled || app.Status == AppointmentStatus.Pending) &&
                (app.Date.Year == Date.Year && app.Date.Month == Date.Month && app.Date.Day == Date.Day));

            return (Appointment != null);
        }

        [NonAction]
        public async Task<Appointment> GetScheduledAppointment(BookAppointmentViewModel model, Doctor doctor)
        {
            // check if there is any appointment with the old appointment id (in case of rescheduling).
            var appointment = await db.Appointments.FirstOrDefaultAsync(app => app.AppointmentNo == model.OldAppointmentId);

            appointment = appointment ?? new Appointment();

            appointment.Date = model.Date;
            appointment.Time = model.Date.TimeOfDay;
            appointment.DoctorId = doctor.Id;
            appointment.Status = AppointmentStatus.Pending;
            appointment.AppointmentNo = model.SlotNo;

            return appointment;
        }

        [NonAction]
        private async Task<Patient> SavePatient(BookAppointmentViewModel model)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    if (model.Errors.Count == 0)
                    {
                        var patient = await db.Patients.FirstOrDefaultAsync(pat => pat.User.Cnic == model.Cnic);
                        var isRegistered = patient != null;

                        patient = patient ?? new Patient { };
                        db.Entry(patient).State = isRegistered ? EntityState.Modified : EntityState.Added;
                        await db.SaveChangesAsync();

                        patient.User = patient.User ?? new NazeeranHospitalUser();

                        patient.User.Name = model.PatientName;
                        patient.User.UserName = model.PatientName;
                        patient.User.PasswordHash = new PasswordHasher().HashPassword(model.PatientName);
                        patient.User.SecurityStamp = Guid.NewGuid().ToString();
                        patient.User.Cnic = model.Cnic;
                        patient.User.PhoneNumber = model.ContactNo;
                        patient.User.Email = model.Email;
                        patient.User.Gender = model.Gender;
                        patient.UserId = patient.User.Id;
                        db.Entry(patient.User).State = isRegistered ? EntityState.Modified : EntityState.Added;
                        await db.SaveChangesAsync();

                        if(!await db.Set<IdentityUserRole>().AnyAsync(r => r.RoleId == ((int)UserRoles.Patient).ToString() && r.UserId == patient.UserId))
                        {
                            var role = new IdentityUserRole
                            {
                                RoleId = ((int)UserRoles.Patient).ToString(),
                                UserId = patient.UserId
                            };

                            db.Entry(role).State = EntityState.Added;
                            await db.SaveChangesAsync();
                        }

                        trans.Commit();
                        return patient;
                    }
                    else
                        return null;
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    ex.ToString();

                    model.Errors.Add("Sorry, There Was An Error While Booking Appointment!Please Try Again");

                    return null;
                }
            }
        }

        [NonAction]
        public async Task SendMessage(BookAppointmentViewModel model)
        {
            if (!string.IsNullOrEmpty(model.ContactNo))
            {
                SmsService smsService = new SmsService();

                IdentityMessage Message = new IdentityMessage();
                Message.Destination = string.Format("+92{0}", model.ContactNo.Substring(1, 11).Replace("-", ""));
                Message.Subject = "Appointment Booked";
                Message.Body = $"Your Appointment Has Beed Booked!Details Are:\nId:{model.AppointmentId}\nNo: {model.AppointmentNo}\nDoctor Name: {model.DoctorName}\nDate: {model.Date.ToLongDateString()}\nTime: {model.Date.TimeOfDay.ToString()}";

                await smsService.SendAsync(Message);
            }
        }

        /// <summary>
        /// Genrates and returns all of the AppointmentSlots for the doctor! This method returns all of the slots whether they are booked or not!
        /// This Method assumes that neither of the parameter will be null.
        /// </summary>
        /// <param name="appointments">
        /// Appointments of the doctor
        /// </param>
        /// <param name="schedule">
        /// Schedule of the doctor!
        /// </param>
        /// <param name="averageTime">
        /// Average time for an dcotor it takes to check each patient!
        /// </param>
        /// <returns>
        /// List Of Appointment Slots of the doctor!
        /// </returns>
        [NonAction]
        private IList<AppointmentSlotViewModel> GetAppointmentSlot(IEnumerable<Appointment> appointments, DoctorSchedule schedule, int averageTime, DateTime Date)
        {
            var Slots = new List<AppointmentSlotViewModel>();
            
            //if schedule is null, then doctor is not available for the day so return the empty slots.
            if (schedule == null) return Slots;

            //DateTime Time = DateTime.Parse(schedule.ArrivalTime.ToString());
            var Time = Date.Date + schedule.ArrivalTime;
            var SlotTime = TimeSpan.FromMinutes(averageTime);

            averageTime = averageTime <= 0 ? int.Parse(ConfigurationManager.AppSettings["DefaultDoctorAverageTime"].ToString()) : averageTime;

            int TotalMins = (int)(schedule.LeaveTime - schedule.ArrivalTime).TotalMinutes,
                TotalSlots = TotalMins / averageTime,
                AppointmentNo = 1;

            for (int i = 0; i <= TotalSlots; i++)
            {
                Slots.Add(new AppointmentSlotViewModel
                {
                    Day = schedule.Day,
                    Time = Time,
                    IsBooked = appointments.Any(app => app.Date.Year == Date.Year && app.Date.Month == Date.Month && app.Date.Day == Date.Day && app.AppointmentNo == AppointmentNo),
                    SlotNo = AppointmentNo
                });

                Time += SlotTime;
                AppointmentNo++;
            }
            return Slots;
        }

        /// <summary>
        /// Check the Dates Validity in the model and if they are invalid creates defaults values for dates!
        /// This Method Is Not An Action and Must be called by the as an regular method call!
        /// </summary>
        /// <param name="model">
        /// model that contains the dates which need to be validated!
        /// </param>
        [NonAction]
        private void CreateDates(GetScheduleViewModel model)
        {
            // If Start Date Is Not Valid, Then Use Current Date!
            if (model.StartDate == DateTime.MinValue || model.StartDate == DateTime.MaxValue)
                model.StartDate = DateTime.Now;

            // If End Date Is Not Valid, Then Add Seven Days To the Current Date!
            if (model.EndDate == DateTime.MinValue || model.EndDate == DateTime.MaxValue)
                model.EndDate = DateTime.Now.AddDays(7);
        }

        [NonAction]
        private void AddModelErrors(IViewErrors model, ModelStateDictionary state)
        {
            foreach (var error in state.Values)
                model.Errors.AddRange(error.Errors.Select(e => e.ErrorMessage));
        }

        #endregion

    }
}