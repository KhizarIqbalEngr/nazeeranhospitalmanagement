﻿using Microsoft.AspNet.Identity;
using NazeeranHospitalManagement.Models;
using NazeeranHospitalManagement.ViewModels;
using NazeeranHospitalManagement.ViewModels.Enums;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Services;

namespace NazeeranHospitalManagement.Controllers
{
    [Authorize(Roles = "Doctor")]
    public class PrescriptionsController : Controller
    {
        private NazeeranHospitalDbContext db = new NazeeranHospitalDbContext();

        [HttpGet]
        public ActionResult Index()
        {
            var isAdded = false;

            if (TempData.Keys.Contains("Added"))
                isAdded = (bool)TempData["Added"];

            ViewData["Added"] = isAdded;

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Index(int? patientId)
        {
            if (ModelState.IsValid)
            {
                if (patientId.HasValue)
                {
                    await this.GetDoctorDetails();
                    var doctor = this.Session["Doctor"] as Doctor;

                    var patient = await db.Patients.FirstOrDefaultAsync(p => p.Id == patientId.Value);
                    var hasAppointment = patient.Appointments.Any(app => app.PatientId == patient.Id && app.DoctorId == doctor.Id && app.Status == AppointmentStatus.Scheduled && app.Date.Date == DateTime.Today);

                    ViewData["HasAppointment"] = hasAppointment;

                    if (patient != null && hasAppointment)
                    {
                        var model = new PrescribeSendModel
                        {
                            DoctorId = doctor.Id,
                            DoctorName = doctor.User.Name,
                            DoctorDepartment = doctor.Department.Name,
                            PatientName = patient.User.Name,
                            PatientId = patient.Id,
                            Gender = patient.User.Gender,
                            DateOfBirth = new DateTime(1992, 7, 9),
                            PrescribedMedicinesByDoctor = await this.GetPrescribedMedicines(patientId.Value),
                            AllTestsWithResults = await this.GetPrescribedTests(patientId.Value),
                            PrescribedTestsByDoctor = new List<PrescribedMeicalTestViewModel>()
                        };

                        return View("ReceivedModel", model);
                    }
                    else if (patient == null)
                    {
                        ViewData["NoPatientFound"] = true;
                    }
                }
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddPrescription(PrescribeSendModel model, FormCollection fc)
        {
            TryUpdateModel(model, new[] { "DoctorId", "PatientId", "Time" });
            TryValidateModel(model);

            model.PrescribedMedicinesByDoctor = model.PrescribedMedicinesByDoctor ?? new List<PrescribedMedicinesViewmodel>();
            model.PrescribedTestsByDoctor = model.PrescribedTestsByDoctor ?? new List<PrescribedMeicalTestViewModel>();

            if (ModelState.IsValid && (model.PrescribedMedicinesByDoctor.Count() > 0 || model.PrescribedTestsByDoctor.Count() > 0))
            {
                try
                {
                    Prescription Prescription = new Prescription { PatientId = model.PatientId, DoctorId = model.DoctorId, Date = DateTime.Now };
                    db.Entry(Prescription).State = EntityState.Added;
                    await db.SaveChangesAsync();

                    model.PrescriptionId = Prescription.Id;
                    AddGeneralTests(model);
                    AddPrescribedMedicines(model);
                    AddPrescribedTests(model);

                    await db.SaveChangesAsync();

                    TempData["Added"] = true;
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ex.ToString();
                }
            }

            model.PrescribedMedicinesByDoctor = await this.GetPrescribedMedicines(model.PatientId);
            model.AllTestsWithResults = await this.GetPrescribedTests(model.PatientId);

            return View("ReceivedModel", model);
        }

        [WebMethod]
        //[ValidateAntiForgeryToken]
        public async Task<JsonResult> SearchMedicines(string term = "")
        {
            var medicinesList = await db.Medicines
                .Where(med => med.Name.ToLower().Contains(term.ToLower()) && med.ExpiryDate > DateTime.Now)
                .Select(med => new
                {
                    itemId = med.Id,
                    label = med.Name,
                    value = med.Name
                })
                .ToListAsync();

            return Json(medicinesList, JsonRequestBehavior.AllowGet);
        }

        [WebMethod]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> GetMedicineById(int? itemId)
        {
            if (itemId.HasValue && itemId.Value > 0)
            {
                var SearchedMedicine = await db.Medicines.FirstOrDefaultAsync(med => med.Id == itemId.Value);

                if (SearchedMedicine != null)
                {
                    var Model = new PrescribedMedicinesViewmodel
                    {
                        MedicineId = SearchedMedicine.Id,
                        MedicineFormula = SearchedMedicine.ChemicalComposition,
                        MedicineName = SearchedMedicine.Name
                    };

                    return View("_PrescriptionSearch", Model);
                }

                Response.StatusCode = (int)HttpStatusCode.NotFound;
                return Json("Medicine Not Found");
            }

            Response.StatusCode = (int)HttpStatusCode.BadRequest;

            return Json("Invalid Id");
        }

        [WebMethod]
        //[HttpPost]
        public async Task<JsonResult> SearchMedicalTest(string term = "")
        {
            var MedicinesList = db.MedicalTestTypes
                .Where(test => test.Name.ToLower().Contains(term.ToLower()))
                .Select(test => new
                {
                    itemId = test.Id,
                    label = test.Name,
                    value = test.Name
                });

            return Json(await MedicinesList.ToListAsync(), JsonRequestBehavior.AllowGet);

        }

        [WebMethod]
        [HttpPost]
        public async Task<ActionResult> GetMedicalTestById(int? itemId)
        {
            if (itemId.HasValue && itemId.Value > 0)
            {
                var SearchedTest = await db.MedicalTestTypes.FirstOrDefaultAsync(test => test.Id == itemId);

                if (SearchedTest != null)
                {
                    var Model = new PrescribedMeicalTestViewModel
                    {
                        ItemId = SearchedTest.Id,
                        TestName = SearchedTest.Name,
                        Date = DateTime.Now                        
                    };

                    return View("_MedicalTests", Model);
                }

                Response.StatusCode = (int)HttpStatusCode.NotFound;
                return Json("Lab Test Not Found");
            }

            Response.StatusCode = (int)HttpStatusCode.BadRequest;

            return Json("Invalid Id");
        }

        [HttpPost]
        [WebMethod]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddMedicalTest(AddMedicalTestsiewModel model)
        {
            if (ModelState.IsValid)
            {
                var Test = new MedicalTest
                {
                    MedicalTestTypeId = model.TestId,
                    Status = MedicalTestStatus.Active,
                    PrescriptionId = model.PrescriptionId,
                    PrescribedOn = DateTime.Now
                };

                db.Entry(Test).State = EntityState.Added;

                try
                {
                    int EffectedRows = await db.SaveChangesAsync();

                    if (EffectedRows > 0)
                        return Json(Test, JsonRequestBehavior.AllowGet);
                    else
                        return Json("Failed to add", JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    ex.ToString();
                    return Json("Exception occured while saving data...!!", JsonRequestBehavior.AllowGet);
                }
            }

            Response.StatusCode = (int)HttpStatusCode.BadGateway;
            return Json("Validation Failed", JsonRequestBehavior.AllowGet);
        }

        public ActionResult GeneralTest()
        {
            return View();
        }

        #region Helper Methods...!!

        [NonAction]
        public async Task<IEnumerable<PrescribedMedicinesViewmodel>> GetPrescribedMedicines(int patientId)
        {
            // Get all previoues precribed medicines of the Patient
            var Medicines = await db.PrescribedMedicines.Where(pre => pre.Prescription.PatientId == patientId).ToListAsync();

            var MdedicineList = Medicines.Select(med => new PrescribedMedicinesViewmodel
            {
                MedicineId = med.MedicineId,
                NoOfDays = med.NoOfDays,
                Quantity = med.Quantity,
                Time = med.Time,
                MedicineName = med.Medicine.Name,
                MedicineFormula = med.Medicine.ChemicalComposition,
                When = med.PrescribedOn.Date
            })
            .OrderByDescending(med => med.When);

            return MdedicineList;
        }

        [NonAction]
        public async Task<IEnumerable<SelectListItem>> GetAllMedicalTests()
        {
            var Tests = await db.MedicalTestTypes.ToListAsync();

            return Tests.Select(med => new SelectListItem
            {
                Text = med.Name,
                Value = med.Id.ToString()
            });
        }

        [NonAction]
        public async Task<PrescribiedTestResultViewModel> GetPrescribedTests(int patientId)
        {
            var Model = new PrescribiedTestResultViewModel();

            var Tests = await db.MedicalTests
                .Where(test => test.Prescription.PatientId == patientId && test.Status != MedicalTestStatus.Completed)
                .Include(test => test.Prescription.Doctor.User)
                .ToListAsync();

            Model.AllTests = Tests.OrderByDescending(test => test.PrescribedOn.Date);
            Model.BloodCpTests = await db.BloodCPs.Where(t => t.Prescription.PatientId == patientId).Include(t => t.Prescription.Doctor.User).ToListAsync();
            Model.CalciumTests = await db.Calciums.Where(t => t.Prescription.PatientId == patientId).Include(t => t.Prescription.Doctor.User).ToListAsync();
            Model.CrossMatchTests = await db.CrossMatches.Where(t => t.Prescription.PatientId == patientId).Include(t => t.Prescription.Doctor.User).ToListAsync();
            Model.TftTests = await db.TFTs.Where(t => t.Prescription.PatientId == patientId).Include(t => t.Prescription.Doctor.User).ToListAsync();

            return Model;
        }

        [NonAction]
        private void AddPrescribedMedicines(PrescribeSendModel model)
        {
            if (model.PrescribedMedicinesByDoctor != null)
            {
                foreach (var m in model.PrescribedMedicinesByDoctor)
                {
                    PrescribedMedicine PrescribeMedicine = new PrescribedMedicine
                    {
                        MedicineId = m.MedicineId,
                        NoOfDays = m.NoOfDays,
                        PrescriptionId = model.PrescriptionId,
                        Quantity = GetQuantiy(m.NoOfDays, m.Time),
                        Time = m.Time,
                        PrescribedOn = DateTime.Now.Date
                    };

                    db.Entry(PrescribeMedicine).State = EntityState.Added;
                }
            }
        }

        private int GetQuantiy(int NoOfDays, string Time)
        {
            var Times = Time.Split('+');
            int Qunatity = 0;

            foreach (var t in Times)
                Qunatity += int.Parse(t);

            return Qunatity * NoOfDays;
        }

        [NonAction]
        private void AddPrescribedTests(PrescribeSendModel model)
        {
            if (model.PrescribedTestsByDoctor != null)
            {
                foreach (var t in model.PrescribedTestsByDoctor)
                {
                    var test = new MedicalTest
                    {
                        MedicalTestTypeId = t.ItemId,
                        Status = MedicalTestStatus.Active,
                        PrescriptionId = model.PrescriptionId,
                        PrescribedOn = DateTime.Now
                    };

                    db.Entry(test).State = EntityState.Added;
                }
            }
        }

        [NonAction]
        private void AddGeneralTests(PrescribeSendModel model)
        {
            model.GenralTests = model.GenralTests ?? new GeneralTestsViewModel();
            model.GenralTests.BloodPressue = model.GenralTests.BloodPressue ?? new BloodPressueViewModel();
            model.GenralTests.HeartRate = model.GenralTests.HeartRate ?? new HeartRateViewModel();
            model.GenralTests.SugarTest = model.GenralTests.SugarTest ?? new SugarTestViewModel();

            var BloodTest = new BloodPressueTest
            {
                PatientId = model.PatientId,
                HighValue = model.GenralTests.BloodPressue.BloodHighValue,
                LowValue = model.GenralTests.BloodPressue.BloodLowValue
            };

            var HeartTest = new HeartRateTest
            {
                PatientId = model.PatientId,
                HeartRateValue = model.GenralTests.HeartRate.HeartRate
            };

            var SugarTest = new SugarTest
            {
                PatientId = model.PatientId,
                BeforeMealValue = model.GenralTests.SugarTest.BeforeMealValue,
                FastingValue = model.GenralTests.SugarTest.FastingValue
            };

            var HeightTest = new HeightWeightTest
            {
                PatientId = model.PatientId,
                HeightValue = model.GenralTests.HeightWeight.HeightValue,
                WeightValue = model.GenralTests.HeightWeight.WeightValue
            };

            db.Entry(BloodTest).State = EntityState.Added;
            db.Entry(HeartTest).State = EntityState.Added;
            db.Entry(SugarTest).State = EntityState.Added;
            db.Entry(HeightTest).State = EntityState.Added;
        }

        [NonAction]
        private async Task GetDoctorDetails()
        {
            Doctor Doctor = this.Session["Doctor"] as Doctor;

            if (Doctor == null)
            {
                var DoctorId = User.Identity.GetUserId();
                //DoctorId = "308076cb-3e44-492e-b53d-2cc88d75dbe9";

                Doctor = await db.Doctors.Where(doc => doc.UserId.Equals(DoctorId))
                    .Include(doc => doc.Department)
                    .Include(doc => doc.User)
                    .FirstOrDefaultAsync();
            }

            this.Session["Doctor"] = Doctor;
        }

        #endregion Helper Methods...!!
    }
}

