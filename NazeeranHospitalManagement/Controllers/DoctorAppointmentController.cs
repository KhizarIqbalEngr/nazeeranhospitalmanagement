﻿using Microsoft.AspNet.Identity;
using NazeeranHospitalManagement.Models;
using NazeeranHospitalManagement.ViewModels.Enums;
using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace NazeeranHospitalManagement.Controllers
{
    [Authorize(Roles= "Doctor")]
    public class DoctorAppointmentController : Controller
    {
        private NazeeranHospitalDbContext db = new NazeeranHospitalDbContext();

        [HttpGet]
        public async Task<ActionResult> ViewAll()
        {
            var DoctorUserId = User.Identity.GetUserId();

            var DoctorId = (await db.Doctors.Where(doc => doc.UserId.Equals(DoctorUserId)).Take(1).ToListAsync()).ElementAt(0).Id;

            var Model = await db.Appointments.Where(app => app.DoctorId == DoctorId).ToListAsync();

            return View(Model);
        }

        public async Task<ActionResult> Cancel(int id)
        {
            var app = db.Appointments.Find(id);

            if(app.Date >= DateTime.Now)
            {
                app.Status = AppointmentStatus.Pending;

                db.Entry(app).State = EntityState.Modified;

                await db.SaveChangesAsync();
            }

            return RedirectToActionPermanent("ViewAll");
        }

        public ActionResult Index()
        {
            return RedirectToActionPermanent("ViewAll");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Helper Methods...!!!

        #endregion
    }
}
