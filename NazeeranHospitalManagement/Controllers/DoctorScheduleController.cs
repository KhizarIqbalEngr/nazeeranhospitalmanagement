﻿using Microsoft.AspNet.Identity;
using NazeeranHospitalManagement.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using NazeeranHospitalManagement.ViewModels.Enums;
using NazeeranHospitalManagement.ViewModels;
using PagedList;

namespace NazeeranHospitalManagement.Controllers
{
    [Authorize(Roles = "Doctor")]
    public class DoctorScheduleController : Controller
    {
        private NazeeranHospitalDbContext db = new NazeeranHospitalDbContext();

        public ActionResult Index()
        {
            return RedirectToActionPermanent("ViewAllAppointments");
        }

        [HttpGet]
        public async Task<ViewResult> ViewAllAppointments(int? page)
        {
            await this.GetDoctorDetails();
            var doctor = this.Session["Doctor"] as Doctor;

            var query = db.Appointments.Where(app => app.DoctorId == doctor.Id && (app.Status == AppointmentStatus.Scheduled || app.Status == AppointmentStatus.Pending) && app.Date.Year >= DateTime.Now.Year && app.Date.Month >= DateTime.Now.Month && app.Date.Day >= DateTime.Now.Day)
                .Include(app => app.Patient);

            var appointments = await query.ToListAsync();

            int pageNumber = page ?? 1;
            int pageSize = 10;

            return View(appointments.ToPagedList(pageNumber, pageSize));
        }

        [HttpGet]
        public async Task<ViewResult> AppointmentDetail(int? id)
        {
            var model = new AppointmentDetailViewModel();
            model.Errors = new List<string>();

            try
            {
                await this.GetDoctorDetails();
                var doctor = this.Session["Doctor"] as Doctor;

                if (!id.HasValue)
                {
                    model.Errors.Add("Sorry! Invalid Appointment Id!");
                }

                var appointment = await db.Appointments.Where(app => app.Id == id.Value && app.DoctorId == doctor.Id).Include(app => app.Patient).FirstOrDefaultAsync();

                if (appointment != null)
                {
                    model.Appointment = appointment;
                }
                else
                {
                    model.Errors.Add("No Appointment Is Found...!!");
                }
            }
            catch(Exception ex)
            {
                ex.ToString();
                model.Errors.Add("An Error Occured While Getting Recrod...!!");
            }

            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Helpers Methods...!!

        [NonAction]
        private async Task GetDoctorDetails()
        {
            var doctor = this.Session["Doctor"] as Doctor;

            if (doctor == null)
            {
                var doctorId = User.Identity.GetUserId();
                doctor = await db.Doctors.Where(doc => doc.UserId.Equals(doctorId))
                    .Include(doc => doc.Department)
                    .Include(doc => doc.User)
                    .FirstOrDefaultAsync();
            }

            this.Session["Doctor"] = doctor;
        }

        #endregion Helpers Methods...!!

    }
}

