﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using NazeeranHospitalManagement.Models;
using NazeeranHospitalManagement.Models.Helpers;
using NazeeranHospitalManagement.ViewModels;
using NazeeranHospitalManagement.ViewModels.Enums;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Services;

namespace NazeeranHospitalManagement.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        #region Fields..!!

        private NazeeranHospitalDbContext db = new NazeeranHospitalDbContext();
        private const string DoctorBindParams = "FullName,UserName,Gender,Email,Cnic,SelectedDepartment";
        private const string DepartmentBindParams = "Name,Details";

        #endregion Fields..!!

        #region General Actions

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AddPerson(UserRoles userRole)
        {
            if (userRole == UserRoles.Doctor)
            {
                return RedirectToActionPermanent("AddDoctor");
            }

            return View(new AddPersonViewModel { Role = userRole });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddPerson(AddPersonViewModel model)
        {
            model.Errors = new List<string>();

            if (ModelState.IsValid)
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var user = new NazeeranHospitalUser
                        {
                            Name = model.FullName,
                            Email = model.Email,
                            UserName = model.UserName,
                            Gender = model.Gender,
                            Cnic = model.Cnic,
                            PasswordHash = new PasswordHasher().HashPassword(model.UserName),
                            SecurityStamp = Guid.NewGuid().ToString(),
                        };
                        db.Entry(user).State = EntityState.Added;
                        await db.SaveChangesAsync();

                        var role = new IdentityUserRole { UserId = user.Id, RoleId = ((int)model.Role).ToString() };
                        db.Entry(role).State = EntityState.Added;
                        await db.SaveChangesAsync();

                        dbTransaction.Commit();
                        ViewBag.Status = "Success";
                    }
                    catch (DbEntityValidationException ex)
                    {
                        foreach (var e in ex.EntityValidationErrors)
                            foreach (var ie in e.ValidationErrors)
                                model.Errors.Add(ie.ErrorMessage);
                        dbTransaction.Rollback();
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback();
                        model.Errors.Add($"Sorry, There was an error while adding {model.Role.ToString()} to the database. please try again.");
                    }
                }
            }

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> ViewAll(UserRoles userRole)
        {
            if(userRole == UserRoles.Doctor)
            {
                return RedirectToActionPermanent("ViewDoctor");
            }

            var roleId = ((int)userRole).ToString();

            var role = await db.Roles.FirstOrDefaultAsync(r => r.Id == roleId);
            var res = await (from u in db.Set<NazeeranHospitalUser>()
                             join r in db.Set<IdentityUserRole>()
                             on u.Id equals r.UserId
                             where r.RoleId == roleId
                             select new { u = u, r = r })
                      .ToListAsync();

            var Model = new ViewAllViewModel
            {
                Role = userRole,
                Users = res.Select(u => u.u).ToList()
            };

            return View(Model);
        }

        [HttpGet]
        public async Task<ActionResult> EditPerson(string personId)
        {
            var Model = new EditPersonViewModel();

            try
            {
                var User = await db.Users.FirstOrDefaultAsync(user => user.Id.Equals(personId));
                
                if(User != null)
                {
                    Model.Id = User.Id;
                    Model.Name = User.Name;
                    Model.Gender = User.Gender;
                    Model.Email = User.Email;
                    Model.Cnic = User.Cnic;
                    Model.UserName = User.UserName;   
                }
                else
                {
                    Model.Errors = new List<string> { "Not A Valid Person Id...!!" };
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                Model.Errors = new List<string> { "An Error Occured While Getting Record From Database..!!" };
            }

            return View(Model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditPerson(EditPersonViewModel model)
        {
            try
            {
                var User = await db.Users.FirstOrDefaultAsync(u => u.Id == model.Id);

                User.Name = model.Name;
                User.Email = model.Email;
                User.Gender = model.Gender;
                User.Cnic = model.Cnic;
                User.UserName = model.UserName;

                db.Entry(User).State = EntityState.Modified;

                await db.SaveChangesAsync();

                model.IsEdited = true;
            }
            catch(Exception ex)
            {
                ex.ToString();
                model.IsEdited = true;
                model.Errors = new List<string> { "Sorry, There Was An Error While Updating Data...!!" };
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> DeletePerson(string personId)
        {
            var status = string.Empty;
            try
            {
                var User = await db.Users.FirstOrDefaultAsync(user => user.Id.Equals(personId));
                if (User != null)
                {
                    db.Entry(User).State = EntityState.Deleted;
                    int rows = await db.SaveChangesAsync();

                    if (rows > 0)
                    {
                        status = "Deleted";
                    }
                    else
                    {
                        status = "Error";
                    }
                }
                else
                {
                    status = "NotFound";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                status = "Exception";
            }

            return Json(status, JsonRequestBehavior.AllowGet); ;
        }

        #endregion General Actions

        #region Doctors Actions

        [HttpGet]
        public async Task<ActionResult> AddDoctor()
        {
            var lists = db.Departments.Select(dep => new SelectListItem
            {
                Text = dep.Name,
                Value = dep.Id.ToString()
            });

            return View(new DoctorBaseViewModel { Departments = await lists.ToListAsync()});
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddDoctor(DoctorBaseViewModel model)
        {
            model.Errors = new List<string>();

            if (ModelState.IsValid)
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var user = new NazeeranHospitalUser
                        {
                            Name = model.FullName,
                            Email = model.Email,
                            UserName = model.UserName,
                            Gender = model.Gender,
                            Cnic = model.Cnic,
                            PasswordHash = new PasswordHasher().HashPassword(model.UserName),
                            SecurityStamp = Guid.NewGuid().ToString(),
                        };
                        user.Roles.Add(new IdentityUserRole { UserId = user.Id, RoleId = ((int)UserRoles.Doctor).ToString() });
                        db.Entry(user).State = EntityState.Added;
                        await db.SaveChangesAsync();

                        var doctor = new Doctor
                        {
                            // Default average time (15 mins) for a doctor to patient. This can be change letter by the doctor.
                            AverageTime = int.Parse(ConfigurationManager.AppSettings["DefaultDoctorAverageTime"].ToString()),
                            DepartmentId = model.SelectedDepartment,
                            UserId = user.Id
                        };
                        db.Entry(doctor).State = EntityState.Added;
                        await db.SaveChangesAsync();

                        dbTransaction.Commit();
                        ViewBag.Status = "Success";
                    }
                    catch (DbEntityValidationException ex)
                    {
                        dbTransaction.Rollback();

                        foreach (var validError in ex.EntityValidationErrors)
                            model.Errors.AddRange(validError.ValidationErrors.Select(err => err.ErrorMessage).ToList());
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback();
                        model.Errors.Add("Sorry, There was an error while adding doctor to the database. please try again.");
                    }
                }
            }

            model.Departments = await GetDepartments();

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> ViewDoctor(int? page)
        {
            int pageNumber = page ?? 1;
            int pageSize = 10;

            // Select all Users whose id's are in the DoctorIds!
            var doctors = await db.Users.Join(
                db.Doctors,
                User => new { userId = User.Id },
                Doc => new { userId = Doc.UserId },
                (User, Doc) => new { User = User, Doc = Doc })
                .Where(Doc => Doc.User.Id.Equals(Doc.Doc.UserId))
                .OrderBy(Doc => Doc.User.Name)
                .Select(doctor => new ViewDoctorViewModel
                {
                    Key = doctor.Doc.Id.ToString(),
                    FullName = doctor.User.Name,
                    UserName = doctor.User.UserName,
                    Gender = doctor.User.Gender,
                    Cnic = doctor.User.Cnic,
                    Email = doctor.User.Email,
                    DepartmentName = doctor.Doc.Department.Name
                })
                .ToListAsync();

            return View(doctors.ToPagedList(pageNumber, pageSize));
        }

        [HttpGet]
        public async Task<ActionResult> EditDoctor(int? Id)
        {
            if (!Id.HasValue)
            {
                return HttpNotFound("Sorry, No Doctor Is Not Found...!!");
            }

            var Doc = await db.Doctors.FindAsync(Id);
            var User = await db.Users.FirstOrDefaultAsync(doc => doc.Id == Doc.UserId);

            if (Doc == null || User == null)
            {
                return HttpNotFound("Sorry, No Doctor Is Not Found...!!");
            }

            var Model = new EditDoctorViewModel
            {
                FullName = User.Name,
                UserName = User.UserName,
                Cnic = User.Cnic,
                Gender = User.Gender,
                Email = User.Email,
                Key = Doc.Id.ToString(),
                DepartmentName = Doc.Department.Name,
                Departments = await GetDepartments(),
                SelectedDepartment = Doc.DepartmentId,
                IsEditSuccessfully = false
            };

            return View(Model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditDoctor(EditDoctorViewModel model)
        {
            bool state = ModelState.IsValid;
            bool isModelUpdated = TryUpdateModel(model, new string[] { "FullName", "UserName", "Cnic", "Gender", "Email", "DepartmentId", "Key" });
            state = ModelState.IsValid;

            int doctorId = int.Parse(model.Key);
            var Doc = await db.Doctors.FindAsync(doctorId);
            var User = await db.Users.FirstOrDefaultAsync(doc => doc.Id == Doc.UserId);

            if (Doc == null || User == null)
            {
                return HttpNotFound("Sorry, No Doctor Is Not Found...!!");
            }

            User.Name = model.FullName;
            User.UserName = model.UserName;
            User.Cnic = model.Cnic;
            User.Gender = model.Gender;
            User.Email = model.Email;
            Doc.DepartmentId = model.SelectedDepartment;

            db.Entry(User).State = EntityState.Modified;
            db.Entry(Doc).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
                model.IsEditSuccessfully = true;
            }
            catch(DbEntityValidationException ex)
            {
                model.Errors = new List<string>();
                this.AddErrors(ex, model);
                model.IsEditSuccessfully = false;
            }
            catch (Exception ex)
            {
                ex.ToString();
                model.IsEditSuccessfully = false;
            }

            model.Departments = await this.GetDepartments();
            return View(model);
        }

        [HttpPost]
        [WebMethod]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> DeleteDoctor(int? doctorId)
        {
            if (!doctorId.HasValue)
            {
                return Json("Sorry, No Doctor Is Not Found...!!", JsonRequestBehavior.AllowGet);
            }
            string message = "";
            using (var dbTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    var doctor = await db.Doctors.FindAsync(doctorId.Value);
                    if(doctor == null)
                    {
                        return Json("Sorry, No Doctor Is Not Found...!!", JsonRequestBehavior.AllowGet);
                    }
                    else if(await db.Appointments.AnyAsync(app => app.DoctorId == doctorId))
                    {
                        return Json("HasAppointments", JsonRequestBehavior.AllowGet);
                    }

                    var roles = await db.Set<IdentityUserRole>().Where(r => r.UserId == doctor.UserId).ToListAsync();
                    db.Set<IdentityUserRole>().RemoveRange(roles);
                    await db.SaveChangesAsync();

                    db.DoctorSchedules.RemoveRange(doctor.DoctorSchedule);
                    await db.SaveChangesAsync();

                    var user = doctor.User;
                    db.Doctors.Remove(doctor);
                    await db.SaveChangesAsync();

                    db.Entry(user).State = EntityState.Deleted;
                    await db.SaveChangesAsync();

                    dbTransaction.Commit();
                    message = "Deleted";
                }
                catch (Exception ex)
                {
                    dbTransaction.Rollback();
                    message = "Exception";
                }
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> ManageDoctorSchedule(int doctorId, int? page)
        {
            var doctor = await db.Doctors.FirstOrDefaultAsync(u => u.Id == doctorId);

            var model = doctor.DoctorSchedule.OrderBy(o => o.Day).Select(s => new ViewDoctorScheduleAdminViewModel
            {
                ScheduleId = s.Id,
                DoctorId = doctor.UserId,
                Day = s.Day,
                ArrivalTime = s.ArrivalTime,
                LeaveTime = s.LeaveTime,
                DoctorName = s.User.Name,
                AverageTime = doctor.AverageTime
            })
            .ToList();

            ViewBag.UserId = doctor.UserId;
            ViewBag.DoctorId = doctor.Id;
            ViewBag.DoctorName = doctor.User.Name;

            int pageNumber = page ?? 1;
            int pageSize = 10;

            return View(model.ToPagedList(pageNumber, pageSize));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> AddDoctorSchedule(AddEditDoctorScheduleAdminViewModel model)
        {
            if(!model.Day.HasValue)
            {
                ModelState.AddModelError("Day", "Invalid Day. Please provide a day to add/edit the schedule.");
            }

            var result = new Dictionary<string, object>();
            if(ModelState.IsValid)
            {
                try
                {
                    var doctor = await db.Doctors.FirstOrDefaultAsync(doc => doc.UserId == model.DoctorId);
                    var schedule = await db.DoctorSchedules.FirstOrDefaultAsync(s => s.Day == model.Day && s.UserId == model.DoctorId);
                    var isExists = (schedule != null);

                    schedule = schedule ?? new DoctorSchedule();
                    schedule.UserId = model.DoctorId;
                    schedule.DoctorId = doctor.Id;
                    schedule.ArrivalTime = model.ArrivalTime;
                    schedule.LeaveTime = model.LeaveTime;
                    schedule.Day = model.Day.Value;
                    doctor.AverageTime = model.AverageTime;

                    db.Entry(schedule).State = (isExists) ? EntityState.Modified : EntityState.Added;
                    db.Entry(doctor).State = EntityState.Modified;

                    await db.SaveChangesAsync();
                    result["Success"] = new int[] { schedule.Id };
                }
                catch (DbEntityValidationException ex)
                {
                    var errors = new List<string>();
                    foreach (var validError in ex.EntityValidationErrors)
                        errors.AddRange(validError.ValidationErrors.Select(err => err.ErrorMessage).ToList());

                    result["ValidationException"] = errors;
                }
                catch (Exception ex)
                {
                    result["Exception"] = "Exception Occured!";
                }
            }
            else
            {
                var errorList = new List<string>();

                foreach (var key in ModelState.Keys)
                    foreach (var errors in ModelState[key].Errors)
                        if (!string.IsNullOrEmpty(errors.ErrorMessage))
                            errorList.Add(errors.ErrorMessage);

                result["InvalidModelState"] = errorList;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> DeleteDoctorSchedule(DeleteDoctorScheduleAdminViewModel model)
        {
            if (!model.Day.HasValue)
            {
                ModelState.AddModelError("Day", "Invalid Day. Please provide a day to add/edit the schedule.");
            }

            var result = new Dictionary<string, object>();
            if (ModelState.IsValid)
            {
                try
                {
                    var schedule = await db.DoctorSchedules.FirstOrDefaultAsync(s => s.Day == model.Day.Value && s.UserId == model.DoctorId);
                    if (schedule != null)
                    {
                        db.Entry(schedule).State = EntityState.Deleted;

                        await db.SaveChangesAsync();
                        result["Success"] = new int[] { schedule.Id };
                    }
                    else
                    {
                        result["NotFound"] = new string[] { $"No Schedule Is Found On {model.Day.ToString()}." };
                    }
                }
                catch (DbEntityValidationException ex)
                {
                    var errors = new List<string>();
                    foreach (var validError in ex.EntityValidationErrors)
                        errors.AddRange(validError.ValidationErrors.Select(err => err.ErrorMessage).ToList());

                    result["ValidationException"] = errors;
                }
                catch (Exception ex)
                {
                    result["Exception"] = "Exception Occured!";
                }
            }
            else
            {
                var errorList = new List<string>();

                foreach (var key in ModelState.Keys)
                    foreach (var errors in ModelState[key].Errors)
                        if (!string.IsNullOrEmpty(errors.ErrorMessage))
                            errorList.Add(errors.ErrorMessage);

                result["InvalidModelState"] = errorList;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion Doctors Actions

        #region Departments..!!

        public async Task<ActionResult> ViewAllDepartment(int? page)
        {
            int pageNumber = page ?? 1;
            int pageSize = 10;

            var model = await db.Departments.ToListAsync();

            return View(model.ToPagedList(pageNumber, pageSize));
        }

        [HttpGet]
        public ActionResult AddDepartment()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddDepartment([Bind(Include = DepartmentBindParams)] Department department)
        {
            if (ModelState.IsValid)
            {
                var alreadyExists = await db.Departments.AnyAsync(d => d.Name.ToLower() == department.Name.ToLower());
                if (alreadyExists)
                {
                    ViewBag.Status = "AlreadyExists";
                }
                else
                {
                    db.Entry(department).State = EntityState.Added;
                    await db.SaveChangesAsync();

                    ViewBag.Status = "Success";
                }
            }

            return View(department);
        }

        [HttpGet]
        public async Task<ActionResult> FindDepartment(int? Id)
        {
            if (!Id.HasValue)
                return HttpNotFound("Sorry! Bad Request!");

            var Department = await db.Departments.FirstOrDefaultAsync(dep => dep.Id == Id.Value);

            if(Department == null)
                return HttpNotFound("Sorry! No Department Is Find..!!");

            var model = new EditDepartmentViewModel { Id = Department.Id, Name = Department.Name, Details = Department.Details, IsEditSuccessfully = false };

            return View("EditDepartment", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditDepartment(EditDepartmentViewModel model)
        {
            model.IsEditSuccessfully = false;

            if(ModelState.IsValid)
            {
                try
                {
                    var Department = await db.Departments.FirstOrDefaultAsync(dep => dep.Id == model.Id);

                    Department.Name = model.Name;
                    Department.Details = model.Details;

                    db.Entry(Department).State = EntityState.Modified;

                    await db.SaveChangesAsync();

                    model.IsEditSuccessfully = true;
                }
                catch (Exception ex)
                {
                    this.AddErrors(ex as DbEntityValidationException, model);
                    ex.ToString();
                }
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> DeleteDepartment(int departmentId)
        {
            if (departmentId <= 0)
            {
                return Json("InvalidId", JsonRequestBehavior.AllowGet);
            }

            string message = "";
            using (var dbTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    var department = await db.Departments.FindAsync(departmentId);

                    if (department == null)
                    {
                        return Json("NotFound", JsonRequestBehavior.AllowGet);
                    }

                    if (await db.Doctors.AnyAsync(d => d.DepartmentId == departmentId))
                    {
                        return Json("DoctorsExists", JsonRequestBehavior.AllowGet);
                    }

                    db.Departments.Remove(department);
                    await db.SaveChangesAsync();

                    dbTransaction.Commit();
                    message = "Deleted";
                }
                catch (Exception ex)
                {
                    dbTransaction.Rollback();
                    message = "Exception";
                }
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Pharmacist...!!

        public ActionResult ViewAllPharmacist()
        {
            return View();
        }

        #endregion  Pharmacist...!!

        #region Admin..!!

        [HttpGet]
        public ActionResult AddAdmin()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddAdmin(FormCollection fc)
        {
            if(ModelState.IsValid)
            {

            }

            return View();
        }

        #endregion Admin..!!

        #region Helpers...!!

        private void AddErrors(DbEntityValidationException exception, IViewErrors model)
        {
            if (exception != null)
            {
                model.Errors = model.Errors ?? new List<string>();

                foreach (var errors in exception.EntityValidationErrors)
                    foreach (var err in errors.ValidationErrors)
                        model.Errors.Add(err.ErrorMessage);
            }
        }

        [NonAction]
        private AccountController GetAccountController()
        {
            var accountController = ControllerBuilder.Current.GetControllerFactory().CreateController(HttpContext.Request.RequestContext, "Account") as AccountController;
            accountController.ControllerContext = this.ControllerContext;

            return accountController;
        }

        [NonAction]
        private async Task<IList<SelectListItem>> GetDepartments()
        {
            var Departments = db.Departments.Select(dep => new SelectListItem { Text = dep.Name, Value = dep.Id.ToString() });

            return await Departments.ToListAsync();
        }

        #endregion Helpers...!!
    }
}