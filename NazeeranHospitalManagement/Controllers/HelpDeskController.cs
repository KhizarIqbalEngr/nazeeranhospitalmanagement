﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using NazeeranHospitalManagement.Models;
using NazeeranHospitalManagement.ViewModels;
using NazeeranHospitalManagement.ViewModels.Enums;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace NazeeranHospitalManagement.Controllers
{
    [Authorize(Roles = "HelpDesk")]
    public class HelpDeskController : Controller
    {
        private NazeeranHospitalDbContext db = new NazeeranHospitalDbContext();

        // GET: HelpDesk
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AddPatient()
        {
            return View(new AddPetientViewModel { Errors = new List<string>() });
        }

        [HttpPost]
        public async Task<ActionResult> AddPatient(AddPetientViewModel model)
        {
            model.Errors = model.Errors ?? new List<string>();
            model.IsAdded = false;
            if (ModelState.IsValid)
            {
                if (await db.Users.AnyAsync(u => u.Cnic == model.Cnic))
                    model.Errors.Add($"{model.Cnic} already registered with another patient.");
                if (await db.Users.AnyAsync(u => u.PhoneNumber == model.PhoneNumber))
                    model.Errors.Add($"{model.PhoneNumber} already registered with another patient.");
                else
                {
                    using (var dbTrans = db.Database.BeginTransaction())
                    {
                        try
                        {
                            var user = new NazeeranHospitalUser
                            {
                                Name = model.PatientName,
                                UserName = model.Email,
                                Cnic = model.Cnic,
                                Email = model.Email,
                                Gender = model.Gender,
                                PhoneNumber = model.PhoneNumber,
                                PasswordHash = new PasswordHasher().HashPassword(model.Email),
                                SecurityStamp = Guid.NewGuid().ToString()
                            };
                            db.Entry(user).State = EntityState.Added;
                            await db.SaveChangesAsync();

                            var patient = new Patient
                            {
                                UserId = user.Id
                            };
                            db.Entry(patient).State = EntityState.Added;
                            await db.SaveChangesAsync();

                            var patientRole = new IdentityUserRole
                            {
                                RoleId = ((int)UserRoles.Patient).ToString(),
                                UserId = user.Id
                            };
                            db.Entry(patientRole).State = EntityState.Added;
                            await db.SaveChangesAsync();

                            dbTrans.Commit();
                            model.IsAdded = true;
                        }
                        catch (DbEntityValidationException validationException)
                        {
                            foreach (var errors in validationException.EntityValidationErrors)
                                foreach (var err in errors.ValidationErrors)
                                    model.Errors.Add(err.ErrorMessage);
                            dbTrans.Rollback();
                        }
                        catch (Exception ex)
                        {
                            dbTrans.Rollback();
                            model.Errors.Add("An Error Occured While Saving Patient Record...");
                        }
                    }
                }
            }

            return View(model);
        }

        [HttpGet]
        public ActionResult FindPatient()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> FindPateints(int patientId)
        {
            var Pateint = await db.Patients.FirstOrDefaultAsync(pat => pat.Id == patientId);

            if(Pateint == null)
            {
                Pateint = new Patient { Id = -1 };
            }

            return View("EditPateint", Pateint);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SavePatient(Patient model)
        {
            var patient = await db.Patients.FirstOrDefaultAsync(pat => pat.Id == model.Id);
            var errors = new List<string>();

            if (patient == null)
            {
                patient = new Patient { Id = -1 };
            }
            else
            {
                if ((patient.User.Cnic != model.User.Cnic) && await db.Users.AnyAsync(user => user.Cnic == model.User.Cnic))
                    errors.Add($"{model.User.Cnic} taken already...");
                if ((patient.User.PhoneNumber != model.User.PhoneNumber) && await db.Users.AnyAsync(user => user.PhoneNumber == model.User.PhoneNumber))
                    errors.Add($"{model.User.PhoneNumber} taken already...");
                if ((patient.User.Email != model.User.Email) && await db.Users.AnyAsync(user => user.Email == model.User.Email))
                        errors.Add($"{model.User.Email} taken already...");
                if (errors.Count == 0)
                {
                    patient.User.Name = model.User.Name;
                    patient.User.Cnic = model.User.Cnic;
                    patient.User.Email = model.User.Email;
                    patient.User.Gender = model.User.Gender;
                    patient.User.PhoneNumber = model.User.PhoneNumber;

                    db.Entry(patient).State = EntityState.Modified;

                    try
                    {
                        await db.SaveChangesAsync();
                        ViewData["IsAdded"] = true;
                    }
                    catch (DbEntityValidationException validationException)
                    {
                        foreach (var error in validationException.EntityValidationErrors)
                            foreach (var err in error.ValidationErrors)
                                errors.Add(err.ErrorMessage);
                        ViewData["IsAdded"] = false;
                    }
                    catch (Exception ex)
                    {
                        ex.ToString();
                        ViewData["IsAdded"] = false;
                        errors.Add("An Error Occured While Saving Patient Data...");
                    }
                }
            }

            ViewData["Errors"] = errors;
            return View("EditPateint", patient);
        }

        public async Task<ActionResult> ManageAppointments(int? page, int? departmentId, string doctorName = "")
        {
            var query = db.Appointments.Where(app => (app.Status != AppointmentStatus.OverDate) && app.Date >= DateTime.Today);

            if (departmentId.HasValue && departmentId.Value > 0)
                query = query.Where(app => app.Doctor.DepartmentId == departmentId.Value);

            if (!string.IsNullOrEmpty(doctorName))
                query = query.Where(app => app.Doctor.User.Name.ToLower().Contains(doctorName.ToLower()));

            var appointments = await query.ToListAsync();
            ViewBag.Departments = await db.Departments.ToListAsync();

            return View(appointments.ToPagedList(page.HasValue ? page.Value : 1, 10));
        }

        [HttpPost]
        public async Task<JsonResult> ChangeAppointmentStatus(int appointmentId, AppointmentStatus appointmentStatus)
        {
            var response = new Dictionary<string, object>();
            var appointment = await db.Appointments.FirstOrDefaultAsync(app => app.Id == appointmentId);

            if (appointment != null)
            {
                if (appointment.Date < DateTime.Today)
                {
                    response["OverDateAppointment"] = new string[] { "Appointment in the past date can't be changed..." };
                }
                else
                {
                    switch (appointment.Status)
                    {
                        case AppointmentStatus.OverDate:
                            response["OverDateAppointment"] = new string[] { "Appointment in the past date can't be changed..." };
                            break;
                        default:
                            appointment.Status = appointmentStatus;
                            db.Entry(appointment).State = EntityState.Modified;
                            if (await db.SaveChangesAsync() > 0)
                                response["Success"] = new string[] { "Appointment Status Changed Successfully." };
                            else
                                response["Error"] = new string[] { "Enable to update the appointment." };
                            break;
                    }
                }
            }
            else
            {
                response["NotFound"] = new string[] { "Sorry, we couldn't find any appointment." };
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GetAllQueryMessage(int? page)
        {
            var pageNumber = page ?? 1;
            var pageSize = 10;
            var queryMessages = await db.QueryMessages.Where(q => string.IsNullOrEmpty(q.Answer)).ToListAsync();

            return View(queryMessages.ToPagedList(pageNumber, pageSize));
        }

        public async Task<ActionResult> QueryDetails(int? id)
        {
            if(!id.HasValue)
            {
                return RedirectToActionPermanent("GetAllQueryMessage");
            }

            var model = await GetViewQueryMessageViewModel(id.Value);
            if(model == null)
            {
                return HttpNotFound("Invalid Status Id...");
            }

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> QueryDetails(AnswerQueryMessageViewModel model)
        {
            if(ModelState.IsValid)
            {
                var queryMessage = await db.QueryMessages.FirstOrDefaultAsync(q => q.Id == model.Id);

                if(queryMessage == null)
                {
                    return HttpNotFound("Invalid query id");
                }

                if (string.IsNullOrEmpty(queryMessage.Answer))
                {
                    queryMessage.Answer = model.Answer;
                    queryMessage.AnswerOn = DateTime.Now;
                    queryMessage.UserId = User.Identity.GetUserId();
                    db.Entry(queryMessage).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    ViewData["Success"] = true;
                }
                else
                {
                    ViewData["AlreadyAnswered"] = true;
                }
            }

            var viewModel = await GetViewQueryMessageViewModel(model.Id);
            if (viewModel == null)
            {
                return HttpNotFound("Invalid Status Id...");
            }

            return View(viewModel);
        }

        [NonAction]
        public async Task<ViewQueryMessageViewModel> GetViewQueryMessageViewModel(int id)
        {
            var queryMessage = await db.QueryMessages.FirstOrDefaultAsync(q => q.Id == id);

            return (queryMessage == null) ? null : new ViewQueryMessageViewModel
            {
                Id = queryMessage.Id,
                Name = queryMessage.Name,
                Email = queryMessage.Email,
                PhoneNumber = queryMessage.PhoneNumber,
                Question = queryMessage.Question,
                PostedOn = queryMessage.PostedOn
            };
        }
    }
}