﻿using Microsoft.AspNet.Identity;
using NazeeranHospitalManagement.Models;
using PagedList;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace NazeeranHospitalManagement.Controllers
{
    [Authorize(Roles ="Patient")]
    public class PatientController : Controller
    {
        NazeeranHospitalDbContext db = new NazeeranHospitalDbContext();
        string PatientId
        {
            get
            {
                return User.Identity.GetUserId();
            }
        }


        public async Task<ActionResult> Index()
        {
            var patient = await db.Patients.FirstOrDefaultAsync(pat => pat.UserId == PatientId);
            ViewBag.PatientId = patient.Id;

            return View();
        }

        public async Task<ActionResult> ViewAllAppointments(int? pageNumber)
        {
            var patient = await db.Patients.FirstOrDefaultAsync(pat => pat.UserId == PatientId);
            ViewBag.PatientId = patient.Id;

            var appointments = patient.Appointments.Where(app => app.Patient.UserId == PatientId && app.Date.Year >= DateTime.Now.Year && app.Date.Month >= DateTime.Now.Month && app.Date.Day >= DateTime.Now.Day).ToList();

            return View(appointments.ToPagedList(pageNumber.HasValue ? pageNumber.Value : 1, 10));
        }

        public async Task<ActionResult> ViewAllPrescriptions(int? pageNumber)
        {
            var prescriptions = await db.Prescriptions.Where(pres => pres.Patient.UserId == PatientId).ToListAsync();

            var patient = await db.Patients.FirstOrDefaultAsync(pat => pat.UserId == PatientId);
            ViewBag.PatientId = patient.Id;

            return View(prescriptions.ToPagedList(pageNumber.HasValue ? pageNumber.Value : 1, 10));
        }

        public async Task<ActionResult> ViewPrescriptionDetails(int prescprionNo)
        {
            if (prescprionNo <= 0)
                return RedirectToActionPermanent("ViewAllPrescriptions");

            var prescription = await db.Prescriptions.FirstOrDefaultAsync(pres => pres.Id == prescprionNo && pres.Patient.UserId == PatientId);

            if(prescription == null)
                return RedirectToActionPermanent("ViewAllPrescriptions");
            
            ViewBag.PatientId = prescription.PatientId;

            return View(prescription);
        }
    }
}