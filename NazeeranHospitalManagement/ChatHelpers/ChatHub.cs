﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.SignalR;
using NazeeranHospitalManagement.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace NazeeranHospitalManagement.ChatHelpers
{
    public class ChatHub : Hub
    {
        static List<NazeeranHospitalUser> OnlineUsers = new List<NazeeranHospitalUser>();
        static Dictionary<string, string> UserConnections = new Dictionary<string, string>();
        NazeeranHospitalDbContext db = new NazeeranHospitalDbContext();

        string CurrentUserId
        {
            get
            {
                return Context.User.Identity.GetUserId();
            }
        }

        public async Task Connect()
        {
            if (!Context.User.Identity.IsAuthenticated)
                return;

            if (OnlineUsers.Any(u => u.Id == CurrentUserId))
                return;

            var user = await db.Users.FirstOrDefaultAsync(u => u.Id == CurrentUserId);
            OnlineUsers.Add(user);
            UserConnections.Add(user.Id, Context.ConnectionId);
        }

        public object GetOnlineUsers()
        {
            var onlineUsers = OnlineUsers.Where(u => u.Id != CurrentUserId)
                .Select(u => new
                {
                    UserId = u.Id,
                    Name = u.Name
                })
                .ToList();

            return onlineUsers;
        }

        public async Task<object> GetChatHistory(string toUserId)
        {
            var chatHistory = await db.ChatMessages.Where(c => (c.FromUserId == CurrentUserId && c.ToUserId == toUserId) || (c.FromUserId == toUserId && c.ToUserId == CurrentUserId))
                .OrderBy(o => o.Time)
                .ToListAsync();

            return chatHistory
                .Select(c => new
                {
                    Id = c.Id,
                    Time = c.Time.ToString("dd/M/yyyy hh:mm tt"),
                    IsSeen = c.IsSeen,
                    Message = c.Message
                });
        }

        public async Task<object> SendMessage(string toUserId, string message)
        {
            var response = new Dictionary<string, object>();
            try
            {
                var chatMessage = new ChatMessage
                {
                    FromUserId = CurrentUserId,
                    ToUserId = toUserId,
                    IsSeen = false,
                    Time = DateTime.Now,
                    Message = message,
                };

                db.Entry(chatMessage).State = EntityState.Added;
                await db.SaveChangesAsync();

                var messageTime = chatMessage.Time.ToString("dd/M/yyyy hh:mm tt");
                Clients.Client(UserConnections[toUserId]).addChatMessage(CurrentUserId, message, messageTime);
                response["Success"] = new
                {
                    Id = chatMessage.Id,
                    Time = messageTime,
                    IsSeen = chatMessage.IsSeen,
                    Message = chatMessage.Message,
                    FromUserId = CurrentUserId,
                    ToUserId = toUserId
                };
            }
            catch(Exception ex)
            {
                return response["Exception"] = "Exception";
            }

            return response;
        }

        public override Task OnConnected()
        {
            var user = OnlineUsers.FirstOrDefault(u => u.Id == CurrentUserId);
            if(user == null)
            {
                user = db.Users.FirstOrDefault(u => u.Id == CurrentUserId);
                OnlineUsers.Add(user);
                UserConnections.Add(user.Id, Context.ConnectionId);
            }

            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            var user = OnlineUsers.FirstOrDefault(u => u.Id == CurrentUserId);
            if (user != null)
            {
                OnlineUsers.Remove(user);
                UserConnections.Remove(user.Id);
            }

            return base.OnDisconnected(stopCalled);
        }
    }
}