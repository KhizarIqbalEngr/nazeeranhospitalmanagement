﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(NazeeranHospitalManagement.Startup))]
namespace NazeeranHospitalManagement
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
